"use strict";
angular
  .module("eyCalculationPdfExporter", [])
  .service("eyCalculationPdfExporter", [
    "state",
    function (state) {
      /* ------------------------- Export pdf  ------------------- */
      // This is the main function - calculates and uses pdfmake to spit out a pdf
      const exportPdf = function (
        result,
        creationdate,
        resolution,
        note,
        contentOnly = false
      ) {
        // The lets ....

        let rows = [];
        let metMast = [];
        let eyCalc = [];
        let eycalcMasts = [];
        let mastAssociation = "N/A";
        let modelCount = 1;
        let rank = 1;
        let neighboringLayoutNote = "";
        let neighboringLayoutTable = "";
        let wakeLossValueText = "";
        let surroundingWakeLoss = 0;
        let turbineType = [];

        // some headers as constants
        let turbineModel = [
          {
            text: ["Turbine Manufacturer/Model"],
            style: "toptableheading",
            width: "25%",
          },
        ];
        let turbineRatedPower = [
          {
            text: ["Turbine Rated Power (kW)"],
            style: "toptableheading",
            width: "25%",
          },
        ];
        let hubHeight = [
          { text: ["Hub Height (m)"], style: "toptableheading", width: "25%" },
        ];
        let numberOfTurbine = [
          {
            text: ["Number of Turbines"],
            style: "toptableheading",
            width: "25%",
          },
        ];

        // Calculation keys -- number of calculations taken into consideration
        const keys = Object.keys(result["calculation"]);
        if (keys.length === 2) {
          // met mast without wake loss compare
          eyCalc = result["calculation"][keys[0]];
          eycalcMasts = result["calculation"][keys[1]];
        } else if (keys.length === 3) {
          // 1. wake loss compare with met mast
          //    key => 0 : ey Calcultion information
          //    key => 1 : wake loss information
          //    key => 2 : met mast information
          // 2. wake loss compare without met mast
          //    key => 0 : ey Calcultion information
          //    key => 1 : wake loss information
          //    key => 2 : [] empty array
          eyCalc = result["calculation"][keys[0]];
          eycalcMasts = result["calculation"][keys[2]];
        } else {
          // When there is no mast or wake loss compare -- just grid and turbines on it.
          // This time is an object - not array of object.
          eyCalc = result["calculation"];
          eycalcMasts = [];
        }

        //preparing the losses data block -- here - because we will use the totals in the losses summary
        let lossesData = {
          turbinePerformance: [],
          availability: [],
          electrical: [],
          environmental: [],
          curtailments: [],
          total: [],
        };

        let lossesValues = {};
        let isReportTemplate = false;
        let lossAvbAccTableText = [
          "Loss Accounting",
          "Wake Loss",
          "Availability",
          "Electrical",
          "Turbine Performance",
          "Environmental",
          "Curtailments/Operational Strategies",
          "Average Total Loss",
        ];
        let percentageTxt = "Percentage";
        let percentage = "";
        if (
          state.user_info.extraOption.report_template !== undefined &&
          state.user_info.extraOption.report_template == 1
        ) {
          isReportTemplate = true;
          lossAvbAccTableText = [
            "Availability Accounting",
            "Wake Efficiency ",
            "Machine Availability",
            "Electrical Efficiency",
            "Turbine Performance",
            "Environmental",
            "Curtailments/Operational Strategies",
            "Total Gross to Net Losses",
          ];
          percentageTxt = "";
          percentage = "%";
        }
        if (eyCalc.losses_parameters == undefined) {
          lossesValues.turbinePerformance = [
            {
              "Turbine Performance Total": checkValueOrBlank(
                eyCalc.site[0].sub_optimal_performance_loss
              ),
            },
          ];

          lossesValues.availability = [
            {
              "Availability Total": checkValueOrBlank(
                eyCalc.site[0].availability_loss.combined
              ),
            },
          ];

          lossesValues.electrical = [
            {
              "Electrical Total": checkValueOrBlank(
                eyCalc.site[0].total_collector_system_loss
              ),
            },
          ];

          lossesValues.environmental = [
            {
              "Environmental Total": checkValueOrBlank(
                eyCalc.site[0].other_environmental_losses
              ),
            },
          ];

          lossesValues.curtailments = [
            {
              "Curtailments Total": checkValueOrBlank(
                eyCalc.site[0].environmental_curtailment_loss
              ),
            },
          ];

          lossesValues.total = calculateTotal([
            checkValueOrBlank(eyCalc.site[0].sub_optimal_performance_loss),
            checkValueOrBlank(eyCalc.site[0].availability_loss.combined),
            checkValueOrBlank(eyCalc.site[0].total_collector_system_loss),
            checkValueOrBlank(eyCalc.site[0].other_environmental_losses),
            checkValueOrBlank(eyCalc.site[0].environmental_curtailment_loss),
          ]);
        } else {
          const availabilityArr = [
            checkValueOrBlank(
              eyCalc.losses_parameters.contractual_availability
            ),
            checkValueOrBlank(
              eyCalc.losses_parameters.non_contractual_availability
            ),
            checkValueOrBlank(
              eyCalc.losses_parameters.fixed_availability_correlation_loss
            ),
            checkValueOrBlank(eyCalc.losses_parameters.substation_loss),
            checkValueOrBlank(eyCalc.losses_parameters.grid_loss),
            checkValueOrBlank(eyCalc.losses_parameters.restart),
          ];
          const turbinePerformanceArr = [
            checkValueOrBlank(eyCalc.losses_parameters.power_curve_adjustment),
            checkValueOrBlank(eyCalc.losses_parameters.fixed_hwh_loss),
            checkValueOrBlank(eyCalc.losses_parameters.wind_shear_loss),
            checkValueOrBlank(eyCalc.losses_parameters.sub_optimal_performance),
          ];
          const electricalArr = [
            checkValueOrBlank(eyCalc.losses_parameters.fixed_electrical_loss),
            checkValueOrBlank(eyCalc.losses_parameters.fixed_heating_loss),
          ];
          const environmentalArr = [
            checkValueOrBlank(eyCalc.losses_parameters.icing),
            checkValueOrBlank(eyCalc.losses_parameters.blade_degradation),
            checkValueOrBlank(eyCalc.losses_parameters.low_temperature),
            checkValueOrBlank(eyCalc.losses_parameters.high_temperature),
            checkValueOrBlank(eyCalc.losses_parameters.site_access),
            checkValueOrBlank(eyCalc.losses_parameters.lightning),
            checkValueOrBlank(
              eyCalc.losses_parameters.other_energy_loss_percent
            ),
          ];
          const curtailmentsArr = [
            checkValueOrBlank(
              eyCalc.losses_parameters.fixed_environmental_curtailment
            ),
            checkValueOrBlank(eyCalc.losses_parameters.fixed_grid_curtailment),
            checkValueOrBlank(
              eyCalc.losses_parameters.fixed_directional_curtailment
            ),
          ];
          const totalArr = availabilityArr.concat(
            turbinePerformanceArr,
            electricalArr,
            environmentalArr,
            curtailmentsArr
          );

          let defaultAvailability = {
            "Availability - contractual": isReportTemplate
              ? (100 - availabilityArr[0]).toFixed(2)
              : availabilityArr[0],
            "Availability - non-contractual": isReportTemplate
              ? (100 - availabilityArr[1]).toFixed(2)
              : availabilityArr[1],
            "Loss due to correction of downtime": isReportTemplate
              ? (100 - availabilityArr[2]).toFixed(2)
              : availabilityArr[2],
            "Availability of collection and substation": isReportTemplate
              ? (100 - availabilityArr[3]).toFixed(2)
              : availabilityArr[3],
            "Availability of utility grid": isReportTemplate
              ? (100 - availabilityArr[4]).toFixed(2)
              : availabilityArr[4],
            "Plant restart after grid outages": isReportTemplate
              ? (100 - availabilityArr[5]).toFixed(2)
              : availabilityArr[5],
            "Availability Total": isReportTemplate
              ? (100 - calculateTotal(availabilityArr)).toFixed(2)
              : calculateTotal(availabilityArr),
          };
          let defaultTurbinePerformance = {
            "Power Curve Adjustment": isReportTemplate
              ? (100 - turbinePerformanceArr[0]).toFixed(2)
              : turbinePerformanceArr[0],
            "High Wind Control Hysteresis": isReportTemplate
              ? (100 - turbinePerformanceArr[1]).toFixed(2)
              : turbinePerformanceArr[1],
            "Wind Shear": isReportTemplate
              ? (100 - turbinePerformanceArr[2]).toFixed(2)
              : turbinePerformanceArr[2],
            "Sub-Optimal Performance": isReportTemplate
              ? (100 - turbinePerformanceArr[3]).toFixed(2)
              : turbinePerformanceArr[3],
            "Turbine Performance Total": isReportTemplate
              ? (100 - calculateTotal(turbinePerformanceArr)).toFixed(2)
              : calculateTotal(turbinePerformanceArr),
          };
          let defaultElectrical = {
            "Electrical Efficiency": isReportTemplate
              ? (100 - electricalArr[0]).toFixed(2)
              : electricalArr[0],
            "Power Consumption of Weather Package": isReportTemplate
              ? (100 - electricalArr[1]).toFixed(2)
              : electricalArr[1],
            "Electrical Total": isReportTemplate
              ? (100 - calculateTotal(electricalArr)).toFixed(2)
              : calculateTotal(electricalArr),
          };
          let defaultEnvironmental = {
            "Icing and blade soiling losses": isReportTemplate
              ? (100 - environmentalArr[0]).toFixed(2)
              : environmentalArr[0],
            "Blade Degradation": isReportTemplate
              ? (100 - environmentalArr[1]).toFixed(2)
              : environmentalArr[1],
            "Low temperature shutdown loss": isReportTemplate
              ? (100 - environmentalArr[2]).toFixed(2)
              : environmentalArr[2],
            "High temperature shutdown loss": isReportTemplate
              ? (100 - environmentalArr[3]).toFixed(2)
              : environmentalArr[3],
            "Site Access": isReportTemplate
              ? (100 - environmentalArr[4]).toFixed(2)
              : environmentalArr[4],
            Lightning: isReportTemplate
              ? (100 - environmentalArr[5]).toFixed(2)
              : environmentalArr[5],
            Others: isReportTemplate
              ? (100 - environmentalArr[6]).toFixed(2)
              : environmentalArr[6],
            "Environmental Total": isReportTemplate
              ? (100 - calculateTotal(environmentalArr)).toFixed(2)
              : calculateTotal(environmentalArr),
          };
          let defaultCurtailments = {
            "Environmental Curtailment": isReportTemplate
              ? (100 - curtailmentsArr[0]).toFixed(2)
              : curtailmentsArr[0],
            "Loss Due To Grid Curtailment": isReportTemplate
              ? (100 - curtailmentsArr[1]).toFixed(2)
              : curtailmentsArr[1],
            "Directional Curtailment": isReportTemplate
              ? (100 - curtailmentsArr[2]).toFixed(2)
              : curtailmentsArr[2],
            "Curtailments Total": isReportTemplate
              ? (100 - calculateTotal(curtailmentsArr)).toFixed(2)
              : calculateTotal(curtailmentsArr),
          };
          lossesValues = {
            availability: [defaultAvailability],

            turbinePerformance: [defaultTurbinePerformance],

            electrical: [defaultElectrical],

            environmental: [defaultEnvironmental],

            curtailments: [defaultCurtailments],
          };
          lossesValues.total = [
            {
              TOTAL: isReportTemplate
                ? (100 - calculateTotal(totalArr)).toFixed(2)
                : calculateTotal(totalArr),
            },
          ];

          lossesData.turbinePerformance.push([
            {
              text: ["Turbine Performance"],
              style: "tableheaderrow",
              width: "60%",
            },
            {
              text: [percentageTxt],
              style: "tableheaderrow",
            },
          ]);

          lossesData.availability.push(
            [
              {
                text: ["Energy Losses"],
                style: "toptableheading",
              },
              {
                text: [],
                style: "toptableheading",
              },
            ],
            [
              {
                text: ["Availability"],
                style: "tableheaderrow",
                width: "60%",
              },
              {
                text: [percentageTxt],
                style: "tableheaderrow",
              },
            ]
          );

          lossesData.electrical.push([
            {
              text: ["Electrical"],
              style: "tableheaderrow",
              width: "60%",
            },
            {
              text: [percentageTxt],
              style: "tableheaderrow",
            },
          ]);
          lossesData.environmental.push([
            {
              text: ["Environmental"],
              style: "tableheaderrow",
              width: "60%",
            },
            {
              text: [percentageTxt],
              style: "tableheaderrow",
            },
          ]);
          lossesData.curtailments.push([
            {
              text: ["Curtailments"],
              style: "tableheaderrow",
              width: "60%",
            },
            {
              text: [percentageTxt],
              style: "tableheaderrow",
            },
          ]);
        }

        // get the primary layout turbines
        const turbines = eyCalc.site[0].site_turbines.turbine;

        // pushing value to wake loss text.
        const wakeValue = isReportTemplate
          ? 100 - checkValueOrBlank(eyCalc.site[0].array_loss, false)
          : checkValueOrBlank(eyCalc.site[0].array_loss, false);
        wakeLossValueText = wakeValue.toFixed(2) + "%";

        // if we are getting wake loss compare flag get the wake loss text
        if (
          result["source_info"] &&
          result["source_info"].wakeLossCompare === true &&
          typeof result["internalWakeLoss"] !== "undefined"
        ) {
          const internalWakeValue = isReportTemplate
            ? 100 -
              checkValueOrBlank(
                result["calculation"][keys[1]].site[0].array_loss,
                false
              )
            : checkValueOrBlank(
                result["calculation"][keys[1]].site[0].array_loss,
                false
              );
          surroundingWakeLoss = checkValueOrBlank(
            wakeValue - internalWakeValue,
            true
          );
          const text = isReportTemplate ? "Efficiency" : "Loss";
          lossAvbAccTableText[1] = `Plant Wake ${text} \n  --  Internal Wake ${text} \n  --  Surrounding Wake ${text}`;
          wakeLossValueText =
            wakeValue.toFixed(2) +
            "%\n" +
            internalWakeValue.toFixed(2) +
            "%\n" +
            surroundingWakeLoss +
            "%";
        }
        // total loss calculation
        let totalLoss = calculateTotal(
          [
            checkValueOrBlank(wakeValue),
            checkValueOrBlank(
              lossesValues.availability[0]["Availability Total"]
            ),
            checkValueOrBlank(lossesValues.electrical[0]["Electrical Total"]),
            checkValueOrBlank(
              lossesValues.turbinePerformance[0]["Turbine Performance Total"]
            ),
            checkValueOrBlank(
              lossesValues.environmental[0]["Environmental Total"]
            ),
            checkValueOrBlank(
              lossesValues.curtailments[0]["Curtailments Total"]
            ),
          ],
          isReportTemplate
        );
        // if there is mast - mast surrounding wake loss
        // TODO: Don't delete below code, we be back agian for this code.
        /* let mastSurroundingWakeLoss = 0;
        if (eycalcMasts.length > 0) {
          for (let k = 1; k < eycalcMasts.site.length; k++) {
            if (eycalcMasts.site[k] && "array_loss" in eycalcMasts.site[k]) {
              mastSurroundingWakeLoss += eycalcMasts.site[k].array_loss;
            }
          }
        } */

        // aggregating turbine data
        turbines.forEach(function (turbine) {
          const tempTurbineType = turbine.turbine_type
            .replace(/Unknown/gi, "")
            .trim();
          if (turbineType.indexOf(tempTurbineType.toLowerCase()) === -1) {
            // If turbine type is not found in array then insert for next time to look up.
            turbineType.push(tempTurbineType.toLowerCase());

            if (modelCount > 5) {
              return;
            }
            modelCount++;

            turbineModel.push({
              text: [tempTurbineType],
              style: ["toptablevalue", "leftAlign"],
            });
            turbineRatedPower.push({
              text: [checkValueOrBlank(turbine.capacity, true)],
              style: "toptablevalue",
            });
            numberOfTurbine.push({
              text: [
                calculateNumberOfTurbine(
                  turbines,
                  tempTurbineType.toLowerCase()
                ),
              ],
              style: "toptablevalue",
            });
          }
        });

        turbineModel.forEach(function (model, index) {
          // skip the first
          if (index > 0) {
            model.width = 75 / turbineType.length + "%";
          }
        });

        // pushing into hub height data to display
        turbineType.forEach(function (turbine_type) {
          hubHeight.push({
            text: [
              countNumberOfHubHeight(turbines, turbine_type.toLowerCase()),
            ],
            style: "toptablevalue",
          });
        });

        // sorting the turbines by yield
        turbines.sort(function (a, b) {
          return b.net_yield - a.net_yield;
        });

        // adding rank to turbines based on net yield
        for (let i = 0; i < turbines.length; i++) {
          // increase rank only if current score less than previous
          if (i > 0 && turbines[i].net_yield < turbines[i - 1].net_yield) {
            rank++;
          }
          turbines[i].rank = rank;
        }
        // now sorting by index.
        turbines.sort(function (a, b) {
          return a.index > b.index ? 1 : -1;
        });

        // turbines header - push
        rows.push([
          { text: ["ID"], style: "tableheaderrow", width: "2%" },
          {
            text: ["Mast Association"],
            style: "tableheaderrow",
            width: "7.5%",
          },
          {
            text: [
              "Coordinates \n" +
                eyCalc.mapping_datum +
                " UTM " +
                eyCalc.utm_zone +
                "\nEasting (m)",
            ],
            style: "tableheaderrow",
            width: "7.5%",
          },
          {
            text: ["Coordinates Northing (m)"],
            style: "tableheaderrow",
            width: "6%",
          },
          { text: ["Elevation (m)"], style: "tableheaderrow", width: "6%" },
          { text: ["Hub Height (m)"], style: "tableheaderrow", width: "6%" },
          { text: ["Turbine Model"], style: "tableheaderrow", width: "10%" },
          { text: ["Free Speed (m/s)"], style: "tableheaderrow", width: "6%" },
          { text: ["Gross MWh/yr"], style: "tableheaderrow", width: "6%" },
          { text: ["Array Eff. (%)"], style: "tableheaderrow", width: "6%" },
          { text: ["Array Loss (%)"], style: "tableheaderrow", width: "6%" },
          { text: ["Total Loss (%)"], style: "tableheaderrow", width: "6%" },
          { text: ["Net MWh/yr"], style: "tableheaderrow", width: "6%" },
          { text: ["Turbine Rank"], style: "tableheaderrow", width: "6%" },
          {
            text: ["Net Capacity Factor (%)"],
            style: "tableheaderrow",
            width: "7%",
          },
          {
            text: ["Total TI at 15m/s (%)"],
            style: "tableheaderrow",
            width: "6%",
          },
        ]);

        angular.forEach(turbines, function (value, key) {
          mastAssociation = value["tab"];
          rows.push([
            value["index"],
            value["tab"],
            checkValueOrBlank(value["x"], true),
            checkValueOrBlank(value["y"], true),
            checkValueOrBlank(value["terrain_elevation"], true),
            checkValueOrBlank(value["hub_height"], true),
            value["turbine_type"].replace(/Unknown/gi, "").trim(),
            checkValueOrBlank(value["mean_free"], true),
            checkValueOrBlank(value["gross_yield"], true),
            checkValueOrBlank(value["array_efficiency"], true),
            (100 - checkValueOrBlank(value["array_efficiency"])).toFixed(2),
            checkValueOrBlank(value["gross_yield"])
              ? (
                  ((checkValueOrBlank(value["gross_yield"]) -
                    checkValueOrBlank(value["net_yield"])) /
                    checkValueOrBlank(value["gross_yield"])) *
                  100
                ).toFixed(2)
              : "0.00",
            checkValueOrBlank(value["net_yield"], true),
            value["rank"],
            checkValueOrBlank(value["capacity_factor"], true),
            checkValueOrBlank(value["total_ti"], true),
          ]);
        });

        const estimatesComment = getEstimatesComment(
          mastAssociation,
          resolution
        );

        if (
          eyCalc.site[1] !== null &&
          eyCalc.site[1].site_name === "neighboringLayout"
        ) {
          neighboringLayoutNote =
            "\u200B\t\t  2. Neighboring WindFarms have been considered in wake calculation. Please check the details below.";

          let surroundingTurbine = [];
          let surroundingTurbineModel = [
            {
              text: ["Turbine Manufacturer/Model"],
              style: "tableheaderrow",
              width: "30%",
            },
          ];
          let surroundingTurbineHubHeight = [
            { text: ["Hub Height"], style: "tableheaderrow", width: "30%" },
          ];
          let surroundingNumberOfTurbines = [
            {
              text: ["Number Of Turbine(s)"],
              style: "tableheaderrow",
              width: "30%",
            },
          ];

          eyCalc.site[1].site_turbines.turbine.forEach(function (turbine) {
            if (
              surroundingTurbine.indexOf(turbine.turbine_type.toLowerCase()) ===
              -1
            ) {
              let numberOfTurbines = calculateNumberOfTurbine(
                eyCalc.site[1].site_turbines.turbine,
                turbine.turbine_type.toLowerCase()
              );
              //numberOfTurbines -- is a text field -- and my contain values like "5,3"
              if (
                !numberOfTurbines == 0 ||
                !numberOfTurbines === "0" ||
                !numberOfTurbines == ""
              ) {
                // If turbine type is not found in array then insert for next time to look up.
                surroundingTurbine.push(turbine.turbine_type.toLowerCase());
                surroundingTurbineModel.push({
                  text: [turbine.turbine_type.replace(/Unknown/gi, "").trim()],
                  style: "surroundingTablevalue",
                });
                surroundingTurbineHubHeight.push({
                  text: [
                    countNumberOfHubHeight(
                      eyCalc.site[1].site_turbines.turbine,
                      turbine.turbine_type.toLowerCase()
                    ),
                  ],
                  style: "surroundingTablevalue",
                });
                surroundingNumberOfTurbines.push({
                  text: [numberOfTurbines],
                  style: "surroundingTablevalue",
                });
              }
            }
          });

          neighboringLayoutTable = {
            style: "bottomtable",
            margin: [0, 15, 0, 0],
            width: "100%",
            table: {
              tableLayouts: "noBorders",
              body: [
                surroundingTurbineModel,
                surroundingTurbineHubHeight,
                surroundingNumberOfTurbines,
              ],
            },
            layout: {
              fillColor: function (rowIndex, node, columnIndex) {
                return "#f4f5f5";
              },
              paddingTop: function (rowIndex, node, columnIndex) {
                return 5;
              },
              paddingBottom: function (rowIndex, node, columnIndex) {
                return 5;
              },
              hLineWidth: function (i, node) {
                return 0.5;
              },
              vLineWidth: function (i, node) {
                return 0.5;
              },
              hLineColor: function (i, node) {
                return "#dee2e6";
              },
              vLineColor: function (i, node) {
                return "#dee2e6";
              },
            },
          };
        }

        // Mast layout table push
        metMast.push([
          { text: ["Tower ID"], style: "tableheaderrow", width: "15%" },
          {
            text: ["X Coordinates"],
            style: "tableheaderrow",
            width: "10%",
          },
          {
            text: ["Y Coordinates"],
            style: "tableheaderrow",
            width: "10%",
          },
          {
            text: ["Elevation (m)"],
            style: "tableheaderrow",
            width: "10%",
          },
          {
            text: ["Hub Height (m)"],
            style: "tableheaderrow",
            width: "10%",
          },
          { text: ["Turbine Type"], style: "tableheaderrow", width: "15%" },
          { text: ["VLT HH (m)"], style: "tableheaderrow", width: "10%" },
          {
            text: ["Gross AEPLT HH"],
            style: "tableheaderrow",
            width: "10%",
          },
          { text: ["Wind Shear"], style: "tableheaderrow", width: "10%" },
        ]);

        // MAST section related calculation .......
        if (
          Object.keys(eycalcMasts).length > 0 &&
          eycalcMasts.metmast !== null
        ) {
          let mastTurbines = eycalcMasts.site[0].site_turbines.turbine;
          let metMastHeights = [];
          let metMastMeanArr = [];
          let metMastHeightLists = [];
          if (eycalcMasts.metmast[0].hasOwnProperty("met_mast_heights")) {
            metMastHeightLists = eycalcMasts.metmast[0].met_mast_heights;
          } else if (
            eycalcMasts.metmast[0].hasOwnProperty("met_mast") &&
            eycalcMasts.metmast[0].met_mast.hasOwnProperty("met_mast_heights")
          ) {
            metMastHeightLists =
              eycalcMasts.metmast[0].met_mast.met_mast_heights;
          }
          angular.forEach(metMastHeightLists, function (mastHeight) {
            metMastHeights.push(mastHeight.height);
            metMastMeanArr.push(mastHeight.mean);
          });

          let mastTurbineModel = [
            {
              text: ["Turbine Manufacturer/Model"],
              style: "toptableheading",
              width: "25%",
            },
          ];
          let mastTurbineRatedPower = [
            {
              text: ["Turbine Rated Power (kW)"],
              style: "toptableheading",
              width: "25%",
            },
          ];
          let mastNumberOfTurbine = [
            {
              text: ["Number of Turbines"],
              style: "toptableheading",
              width: "25%",
            },
          ];

          let MastTurbineType = [];
          let mastModelCount = 1;
          mastTurbines.forEach(function (mastTurbine) {
            const tempMastTurbineType = mastTurbine.turbine_type.replace(
              /Unknown/gi,
              ""
            );
            if (
              MastTurbineType.indexOf(tempMastTurbineType.toLowerCase()) === -1
            ) {
              // If turbine type is not found in array then insert for next time to look up.
              MastTurbineType.push(tempMastTurbineType.toLowerCase());

              if (mastModelCount > 5) {
                return;
              }
              mastModelCount++;

              mastTurbineModel.push({
                text: [tempMastTurbineType],
                style: ["toptablevalue", "leftAlignment"],
              });
              mastTurbineRatedPower.push({
                text: [checkValueOrBlank(mastTurbine.capacity, true)],
                style: "toptablevalue",
              });
              mastNumberOfTurbine.push({
                text: [
                  calculateNumberOfTurbine(
                    mastTurbines,
                    tempMastTurbineType.toLowerCase()
                  ),
                ],
                style: "toptablevalue",
              });
            }
          });

          // sorting the mastTurbines by index
          mastTurbines.sort(function (a, b) {
            return a.index > b.index ? 1 : -1;
          });

          angular.forEach(mastTurbines, function (value, key) {
            angular.forEach(result.shearData, function (shaerValue) {
              if (shaerValue["height"] === value["hub_height"]) {
                mastTurbines[key]["shear"] = shaerValue["shearvalue"];
              }
            });
          });

          let chkList = [];
          angular.forEach(mastTurbines, function (value, key) {
            const temp =
              value["turbine_type"].replace(/Unknown/gi, "").trim() +
              "-" +
              value["hub_height"] +
              "-" +
              value["label"];
            if (!chkList.includes(temp)) {
              chkList.push(temp);
              metMast.push([
                value["label"],
                checkValueOrBlank(value["x"], true),
                checkValueOrBlank(value["y"], true),
                checkValueOrBlank(value["terrain_elevation"], true),
                checkValueOrBlank(value["hub_height"], true),
                value["turbine_type"].replace("Unknown", "").trim(),
                checkValueOrBlank(value["mean_free"], true),
                checkValueOrBlank(value["gross_yield"], true),
                checkValueOrBlank(value["shear"], true),
              ]);
            }
          });
        }

        // The main structure and blocks.....
        // Note -- for additional sections - please add by pushing to comment array section
        const dd = {
          pageSize: "A3",
          // pageOrientation: 'landscape',
          pageMargins: [40, 60, 40, 60],
          header: function (currentPage) {
            return [
              {
                style: "redtable",
                table: {
                  widths: ["100%"],
                  heights: 30,
                  body: [
                    [
                      {
                        //margin: [0, 0, 0, 0],
                        border: [false, false, false, false],
                        columns: [
                          {
                            width: "15%",
                            stack: [
                              {
                                image:
                                  "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABkAAAAJdCAYAAABqC7vxAAAgAElEQVR4XuydjZkctdJG/UUARMASgU0ELBFgImAdASYClgiACDyOADuCO0QAjoB1BOAI+KoWjT07nh+VVGqV1KefZ665tn5PVavV9bak/3vEBQEIQAACEIAABCAAAQhAAAIQgAAEIAABCEAAAhCAAAQmI/B/k/WH7kAAAhCAAAQgAAEIQAACEIAABCAAAQhAAAIQgAAEIACBRwggOAEEIDAMgX///fe6orH//N///d+fFfnJCgEIQAACEIAABCAAAQhAAAIQgAAEIAABCAxEAAFkIGPRVAiMSkCEiytpu/702v9v/f/XB/3Sf/984b6+lfruDurc7v3/f+S/d+IJQsrCxqE6CEAAAhCAAAQgAAEIQAACEIAABCAAAQiUEEAAKaFGHghA4D2BvVUZ1+kvn8ifn6bf48lR7Qsn29RXFUpUMLmTFSd3k/ef7kEAAhCAAAQgAAEIQAACEIAABCAAAQhAICwBBJCwpqFhEIhBYG/1xk7YuE4t+ypGC8O3YieSqBiiv3uBRMSRbfiW00AIQAACEIAABCAAAQhAAAIQgAAEIAABCAxMAAFkYOPRdAh4EkhCh4oc+rtKP0QOT8gfl/VO/koFkd2qka38NytH2jKndAhAAAIQgAAEIAABCEAAAhCAAAQgAIGVEEAAWYmh6SYE9gmkbav2xQ6Ejngu8nsSRu70T1aMxDMQLYIABCAAAQhAAAIQgAAEIAABCEAAAhCITQABJLZ9aB0EqgkciB0qesx+Lkc1s8AF6HZauxUj2ySM6HkjXBCAAAQgAAEIQAACEIAABCAAAQhAAAIQgMABAQQQXAICExFI21hdS5dU6NA/ETsmsu+JrqgospWfCiNbWSmif3JBAAIQgAAEIAABCEAAAhCAAAQgAAEIQGD1BBBAVu8CABiZgAgeO6HjWvqh//35yP2h7W4EdPusrfx2ogirRNzQUhAEIAABCEAAAhCAAAQgAAEIQAACEIDAKAQQQEaxFO2EgBBIKzyeyn9ep98ngIFABoE3SRBRUURXiSCIZEAjCQQgAAEIQAACEIAABCAAAQhAAAIQgMDYBBBAxrYfrZ+cgAgen0oX9wUPVnhMbvOFuvdeEBEx5NVCdVINBCAAAQhAAAIQgAAEIAABCEAAAhCAAAQWJYAAsihuKoPAZQJpWysVPfTHGR6XkZGinoBumaVCCGeI1LOkBAhAAAIQgAAEIAABCEAAAhCAAAQgAIEgBBBAghiCZqybgIgeO8FD/2Rbq3W7Q+/ev0tiyE4QYbus3hahfghAAAIQgAAEIAABCEAAAhCAAAQgAIEiAgggRdjIBIE6AntbW6ng8U1daeSGQFMCu9Uhr2S7rLumNVE4BCAAAQhAAAIQgAAEIAABCEAAAhCAAAQcCSCAOMKkKAicI4DogX9MQGB3dshGxJA/J+gPXYAABCAAAQhAAAIQgAAEIAABCEAAAhCYmAACyMTGpWv9CSB69LcBLWhG4K2UrNtkIYY0Q0zBEIAABCAAAQhAAAIQgAAEIAABCEAAAjUEEEBq6JEXAicIpDM9buSf2d4KL1kDAcSQNViZPkIAAhCAAAQgAAEIQAACEIAABCAAgcEIIIAMZjCaG5eAiB5PpHXP5cdB5nHNRMvaE0AMac+YGiAAAQhAAAIQgAAEIAABCEAAAhCAAAQyCCCAZEAiCQROERDR4yoJHip8fA4pCEDgAQE9M2QjPw5QxzEgAAEIQAACEIAABCAAAQhAAAIQgAAEFieAALI4ciqcgQBbXM1gRfqwMIHXSQhRQYQLAhCAAAQgAAEIQAACEIAABCAAAQhAAALNCSCANEdMBbMQSKs9bqQ/+mO1xyyGpR9LE3gnFerh6b/83//9359LV059EIAABCAAAQhAAAIQgAAEIAABCEAAAushgACyHlvT00ICInxcS1bd4ooDzQsZkg0CJwjstsjaiBjyD5QgAAEIQAACEIAABCAAAQhAAAIQgAAEIOBJAAHEkyZlTUNARI9PpTN6mPmt/FjtMY1l6UhgAi+lbawKCWwgmgYBCEAAAhCAAAQgAAEIQAACEIAABEYjgAAymsVob1MCe9tc6YqPT5pWRuEQgMAxAroq5Bf56cHprArBRyAAAQhAAAIQgAAEIAABCEAAAhCAAASKCSCAFKMj40wERPh4Iv1R0eO7mfpFXyAwMAE9K2SjYogIIXcD94OmQwACEIAABCAAAQhAAAIQgAAEIAABCHQigADSCTzVxiCQzve4ldZ8FaNFtAICEDhC4HUSQrbQgQAEIAABCEAAAhCAAAQgAAEIQAACEIBALgEEkFxSpJuKgAgfN9Ih/SF8TGVZOjM5gbfSv1tZEbKZvJ90DwIQgAAEIAABCEAAAhCAAAQgAAEIQMCBAAKIA0SKGIdAEj5upcUcbD6O2WgpBA4JqBCiIohuj8U5IfgHBCAAAQhAAAIQgAAEIAABCEAAAhCAwFECCCA4xioIIHyswsx0cn0EOCdkfTanxxCAAAQgAAEIQAACEIAABCAAAQhAIJsAAkg2KhKOSADhY0Sr0WYIFBF4Kbl0e6y7otxkggAEIAABCEAAAhCAAAQgAAEIQAACEJiOAALIdCalQ0oA4QM/gMBqCSCErNb0dBwCEIAABCAAAQhAAAIQgAAEIAABCDwkgACCR0xFQISPa+nQrfw43Hwqy9IZCJgJvJYcekbI1pyTDBCAAAQgAAEIQAACEIAABCAAAQhAAAJTEEAAmcKMdEKEjyca7ET4wBcgAIEDAr/L/9etsRBCcA0IQAACEIAABCAAAQhAAAIQgAAEILAyAgggKzP4bN0V4eNKg5vy+262vtEfCEDAlYCuCHnOGSGuTCkMAhCAAAQgAAEIQAACEIAABCAAAQiEJoAAEto8NO4UARE+PtVgpvx+hBIEIAABAwHOCDHAIikEIAABCEAAAhCAAAQgAAEIQAACEBiZAALIyNZbadvTAee63dUnK0VAtyEAgXoCCCH1DCkBAhCAAAQgAAEIQAACEIAABCAAAQiEJoAAEto8NG6fQDrgXIWPx5CBAAQg4ETgJylHD0v/x6k8ioEABCAAAQhAAAIQgAAEIAABCEAAAhAIQgABJIghaMZpAmm7KxU+OOcDR4EABFoQeJdEkNsWhVMmBCAAAQhAAAIQgAAEIAABCEAAAhCAQB8CCCB9uFNrJgERP/ScDw1Kst1VJjOSQQACxQTe6ngjq0E2xSWQEQIQgAAEIAABCEAAAhCAAAQgAAEIQCAMAQSQMKagIfsE2O4Kf4AABDoS+D0JIduObaBqCEAAAhCAAAQgAAEIQAACEIAABCAAgUoCCCCVAMnuSyBtd3UrpX7vWzKlQQACEDAT4KB0MzIyQAACEIAABCAAAQhAAAIQgAAEIACBOAQQQOLYYvUtEfHjqUDQsz4+Xz0MAEAAAlEIcD5IFEvQDghAAAIQgAAEIAABCEAAAhCAAAQgYCSAAGIERnJ/AmnVx0ZK/sa/dEqEAAQg4EJAzwd5LueDvHIpjUIgAAEIQAACEIAABCAAAQhAAAIQgAAEmhNAAGmOmArOEUirPlT84JBzXAUCEBiBgJ4PciNCyN0IjaWNEIAABCAAAQhAAAIQgAAEIAABCEBgzQQQQNZs/Y59Z9VHR/hUDQEIeBD4SUSQW4+CKAMCEIAABCAAAQhAAAIQgAAEIAABCECgDQEEkDZcKfUMAVZ94B4QgMAkBHRbLF0Nsp2kP3QDAhCAAAQgAAEIQAACEIAABCAAAQhMRQABZCpzxu4Mqz5i24fWQQACxQReJyHkn+ISyAgBCEAAAhCAAAQgAAEIQAACEIAABCDgTgABxB0pBR4jIOLHtfy9Hh7MWR+4CAQgMCOBd9IpPSR9M2Pn6BMEIAABCEAAAhCAAAQgAAEIQAACEBiRAALIiFYbrM0ifvwiTf5+sGbTXAhAAAIlBDgkvYQaeSAAAQhAAAIQgAAEIAABCEAAAhCAQAMCCCANoFLkfwRE+Hgif2zk9xgmEIAABFZEQFeD3MpqEBV/uSAAAQhAAAIQgAAEIAABCEAAAhCAAAQ6EUAA6QR+9mpF/HiuAUD5seXV7MamfxCAwCkCrAbBNyAAAQhAAAIQgAAEIAABCEAAAhCAQEcCCCAd4c9YNQedz2hV+gQBCFQQYDVIBTyyQgACEIAABCAAAQhAAAIQgAAEIACBGgIIIDX0yPuAQNrySg86/xw0EIAABCDwgACrQXAICEAAAhCAAAQgAAEIQAACEIAABCCwMAEEkIWBz1qdiB830rcXs/aPfkEAAhBwIMBqEAeIFAEBCEAAAhCAAAQgAAEIQAACEIAABHIJIIDkkiLdUQJpyys96Pc7EEEAAhCAQBYBXQ3yVA5J/ycrNYkgAAEIQAACEIAABCAAAQhAAAIQgAAEiggggBRhI5MSEPHjSv7QLa8eQwQCEIAABEwEdDXIjYggOoZyQQACEIAABCAAAQhAAAIQgAAEIAABCDQggADSAOoaihTx41r6qYG7T9bQX/oIAQhAoBGBl1Luc1aDNKJLsRCAAAQgAAEIQAACEIAABCAAAQismgACyKrNX9Z5ET+eS86fy3KTCwIQgAAEDgi8kf+vq0H+hAwEIAABCEAAAhCAAAQgAAEIQAACEICAHwEEED+W05fEeR/Tm5gOQgACfQn8ICKInqnEBQEIQAACEIAABCAAAQhAAAIQgAAEIOBAAAHEAeIaikjix1b6ynkfazA4fYQABHoReC0V62oQDkjvZQHqhQAEIAABCEAAAhCAAAQgAAEIQGAaAggg05iyXUdE/Hgipav4wXkf7TBTMgQgAIEdgbdJBNFxlwsCEIAABCAAAQhAAAIQgAAEIAABCECgkAACSCG4tWQT8eNG+qpbsiB+rMXo9BMCEIhCgC2xoliCdkAAAhCAAAQgAAEIQAACEIAABCAwJAEEkCHNtkyjOex8Gc7UAgEIQOAMAbbEwj0gAAEIQAACEIAABCAAAQhAAAIQgEAhAQSQQnCzZxPxYyN9/G72ftI/CEAAAgMQ0C2xnsq5IH8O0FaaCAEIQAACEIAABCAAAQhAAAIQgAAEwhBAAAljihgNSYedq/jxTYwW0QoIQAACEBAC7+T3XEQQHZ+5IAABCEAAAhCAAAQgAAEIQAACEIAABDIIIIBkQFpLkiR+bKW/j9fSZ/oJAQhAYDACL0UEuRmszTQXAhCAAAQgAAEIQAACEIAABCAAAQh0IYAA0gV7vEpF/LiSVr1C/IhnG1oEAQhA4IDAG/n/1yKE/AMZCEAAAhCAAAQgAAEIQAACEIAABCAAgdMEEEDwjkcifjwRDFv5fQIOCEAAAhAYgoBuiaUiCOeCDGEuGgkBCEAAAhCAAAQgAAEIQAACEIBADwIIID2oB6oT8SOQMWgKBCAAATuBZ5wLYodGDghAAAIQgAAEIAABCEAAAhCAAATWQQABZB12PtpLxI8VG5+uQwACMxH4VUSQ5zN1iL5AAAIQgAAEIAABCEAAAhCAAAQgAAEPAgggHhQHLEPEjxtp9osBm06TIQABCEDgYwKv5a9uOBcE14AABCAAAQhAAAIQgAAEIAABCEAAAh8IIICs0BsQP1ZodLoMAQisgYAejv5URJC7NXSWPkIAAhCAAAQgAAEIQAACEIAABCAAgUsEEEAuEZrs3xE/JjMo3YEABCDwkACHo+MREIAABCAAAQhAAAIQgAAEIAABCEAgEUAAWZErIH6syNh0FQIQWDMBFUGeczj6ml2AvkMAAhCAAAQgAAEIQAACEIAABCCgBBBAVuIHiB8rMTTdhAAEIPCBwA8igvwCEAhAAAIQgAAEIAABCEAAAhCAAAQgsFYCCCArsDzixwqMTBchAAEIHCfwUkSQG+BAAAIQgAAEIAABCEAAAhCAAAQgAIE1EkAAmdzqiB+TG3id3dODnv9JXb+TP/W3uw7///u/Lz0YWu6hJ1LIpydQXx/8/ZX8f/3trq/WaSJ6HYzAa2nPjdwDu/smWPNoDgQgAAEIQAACEIAABCAAAQhAAAIQaEMAAaQN1xClIn6EMAONsBH4PSXfpj//lD81aHtXKmDYqm+XWu7H61S6iikqqui1E1eu5L8/b1c7JUPgkQqH14ggeAIEIAABCEAAAhCAAAQgAAEIQAACayKAADKptRE/JjXs+N3Sw5lV1LhLv3uBQ4Ky2/G7Vt8DuW934sjhn1dSOgJJPeK1l6AiyNPRxcS1G5H+QwACEIAABCAAAQhAAAIQgAAEIJBPAAEkn9UwKdOWPX8M02AaOisBXc1xJz8VOe5/fH1eZ+p0b19JKbpyRP/UH9ts1WFdW24VIXUliN6TXBCAAAQgAAEIQAACEIAABCAAAQhAYGoCCCCTmTcFSLfSrU8m6xrdiU1AxY59oYPg6oL2kvv+SqpTUWT/x4qRBW0wWFWIIIMZjOZCAAIQgAAEIAABCEAAAhCAAAQgUEYAAaSMW8hciB8hzTJjo95Kp7bpp6s6EDsCWnlvO62dKHItzUQUCWirTk1SEUS3w9J7mQsCEIAABCAAAQhAAAIQgAAEIAABCExJAAFkErOmL8A1EM3Kj0lsGqgbem6ABknvf2xjFcgyxqbsiSLXklV/Ko4wZhg5Tpb8mdzTm8n6RHcgAAEIQAACEIAABCAAAQhAAAIQgMA9AQSQCRwhBTW30pXHE3SHLvQnoCs8XslPfQrBo789mrYgrRy7lkpUDNE/WSXSlHjIwhFBQpqFRkEAAhCAAAQgAAEIQAACEIAABCBQSwABpJZg5/yIH50NMEf1uhXOVn73ood8DX43R7foRQmBtJrsWvLq76n8WCFSAnK8PIgg49mMFkMAAhCAAAQgAAEIQAACEIAABCBwgQACyOAuIsFKDVp/M3g3aP7yBN6v8hDBQ32ICwJHCeytELlmrJneSRBBpjcxHYQABCAAAQhAAAIQgAAEIAABCKyLAALIwPaWwORGmv/dwF2g6csS0LM81Gd0lQcHly/LfpraZNzRVSEqhuifbJc1jWXfdwQRZD6b0iMIQAACEIAABCAAAQhAAAIQgMBqCSCADGp6CUI+l6b/PGjzafZyBHaix6u1b22Vtna6sqIXbltrnrWkT0xVCNHfV2vp9wr6iQiyAiPTRQhAAAIQgAAEIAABCEAAAhCAwBoIIIAMaGUJOt5Is18M2HSavAyBaUUP8f3rhPBT+VMP7dZr/793///xAqh1G7G7vXr+kf/eX1mj/61/p9efIqTs/nuBpi1fRTqPaCeGsC3f8ibwrvEn8dlb70IpDwIQgAAEIAABCEAAAhCAAAQgAAEILEkAAWRJ2g51pf34t1IUBxM78JyoiN2ZHptRt7faW6GhwsZO1NA/r+Q3y1ZLeuD8TiTZCSR38nf3v1lW6SCGTDOy/CA++cs0vaEjEIAABCAAAQhAAAIQgAAEIAABCKyOAALIQCZPQUUNlCJ+DGS3xk19KeXr9lbDHGSeRDwVOVTYuJafihxLrNhobAq34ncrS3YCyVZKHlYcQQxx84teBbEdVi/y1AsBCEAAAhCAAAQgAAEIQAACEIBANQEEkGqEyxSQgogaCCVQvAzyyLVogFy/ytbVHqG3VUpix7W0VQUP/Z3y39/3gO9vHaV/rX5/eJm2lErtUKHl8NK27a7DbbUi3mu6vdmd/JTR/W+kVSN7YoieYRSRb+T7vmfbEEF60qduCEAAAhCAAAQgAAEIQAACEIAABIoJIIAUo1s2owQON1Ljd8vWSm3BCOhqDxU9tsHa9b45e4KHngWxO6NjJ2jstn7atd8kYvTq8554ciVt0N9OKNH/jrI1lwpI+6LI/lkkvdCdrTdteaZCiPpKFI4hWQVpFCJIEEPQDAhAAAIQgAAEIAABCEAAAhCAAATyCSCA5LPqllIChRok/LlbA6i4JwFd7bGR3y9RV3ukg8l3KzzupK33gkdkocbToHtnl1xLuTtxRHn03qpuXxTZRl4pIgxVBNEfIq+nc/qXhQjiz5QSIQABCEAAAhCAAAQgAAEIQAACEGhIAAGkIVyPolNw+X8eZVHGUAQ0eK2rPTZDtZrGvieQtntSIeRaflfyO7cF2BLk9AD27e4nvhVulUhidiNtVNGXVSFLeIW9DkQQOzNyQAACEIAABCAAAQhAAAIQgAAEINCJAAJIJ/A51aYvyzVI2ftL8pzmksaHgG5zpas9wgWnfbpHKQcrZq47BvpDCyKJ043wYVVIvNsGESSeTWgRBCAAAQhAAAIQgAAEIAABCEAAAkcIIIAEdQsOPQ9qmDbN0kD0K/ndRt6mqE3XKTXd6yqE7FaLfNWJyr4g8iqKLyYhWIUQXRWCGNzJOY5U+yVCbRxj0BIIQAACEIAABCAAAQhAAAIQgAAEjhNAAAnqGRx6HtQwvs3SgPMv+ot6vodvdyktl0Ba/XAt6fXXSxDR82dUmNPzQ/TP7pdwuZFGqBDyuHtjaICOX9eIIDgCBCAAAQhAAAIQgAAEIAABCEAAApEJIIA0ss5u//+Sg6BTkO9Fo6ZRbH8CCB/9bTBMC/ZWiFxLo/Wg8F5nY7yWuneCyF1PgEkgUiHkm57toO5HiCA4AQQgAAEIQAACEIAABCAAAQhAAAKhCSCANDBPCljqV/031uIlr26D84c1H+mHIIDwMYSZYjcybQmlQsh1RwHgjdS9kV/XrbISi1tpB+eE9HNbHdeuWMXWzwDUDAEIQAACEIAABCAAAQhAAAIQgMBpAgggDbwjbV9lPsg6CSd6+HWvL7wb0KBIIYDwgRs0IZDGjJ0Yon/2OCOjuxjCOSFN3MtSqPqAbof1jyUTaSEAAQhAAAIQgAAEIAABCEAAAhCAQGsCCCDOhJP48acEgvRsB9MleXV7GbZ0MVELnRjhI7R55mucjCEqgux+qxNDkiCkW2NxYPry7o0IsjxzaoQABCAAAQhAAAIQgAAEIAABCEDgAgEEEEcXkeDbrRT3RMQPDUCaLs79MOEaIfFLDcLyRfQIppqzjQHEkN2ZIbpN1qIrAxBCuvn0y5KtH7u1loohAAEIQAACEIAABCAAAQhAAAIQmJ4AAoiTiZOAoas+zHuhp3M/tpK3xxfbTgQoJhHQoK8KH3cQgUAEAnvbZKkw22OFma6E0tVtG7kvdJxb7EIIWQz1fkWIIF2wUykEIAABCEAAAhCAAAQgAAEIQAACxwgggDj4hQTZrqWY/8nvWwnwaaDPdEl+PffjsSkTiaMR0O1fVPjYRmsY7YHAjsDeAeq6RVSPs4beSr0b/S0pEiKELH4PPBP7qp25IAABCEAAAhCAAAQgAAEIQAACEIBAVwIIIJX491ZvbAu3vtJVI99XNoPs/Qjo1+0qfBDs62cDai4gkMYuFUJ6HZ6uq6VUCDGLxgXdvc+ShBAdc78rLYN82QQQQbJRkRACEIAABCAAAQhAAAIQgAAEIACBVgQQQCrIpmCart74VH4lW19dSz5dOcI1JoGfpNm/LH2+wZioaHVUAntbZN1KG3utClFRQsWQRc4KSSthtL8IIe0cU8Xha7GpPiO5IAABCEAAAhCAAAQgAAEIQAACEIBAFwIIIIXYU9BwK9l16yrz1lcp/53k5dyPQht0zPa71H2z5BY+HftK1SsikLbzu5Eu9xIGXkrdt0vdW6m/t1LnVysy85JdVRHE/HHAkg2kLghAAAIQgAAEIAABCEAAAhCAAATmJoAAUmhfCZxtUpDwdeHWV7rtS48DiQt7TDYhoME8FT4W27IH6hDoQSCtkNDtsW7k10OkVZFRhZDtEv2X/uo2YLoKpccKmCW62LMOPR9JV4IssrqnZ0epGwIQgAAEIAABCEAAAhCAAAQgAIF4BBBACmwiwbJbyfaj/Iq+bk3Btt8KqiZLPwK/poAsQbx+NqDmhQkE2R5LhZDNEl1PY7sKPz1EnyW62KuOog8FejWWeiEAAQhAAAIQgAAEIAABCEAAAhCYhwACiNGWEiC7kSwvUja2vjLyGzC5fr2sh5xvB2w7TYaAG4E09qk4oNv+LX29lQo38mt+5g7ngzQz7U8yjt42K52CIQABCEAAAhCAAAQgAAEIQAACEIDAEQIIIAa3kMDYE0m+lZ9+HVz0RauUwdZXBuadkxKw62wAqo9HoPO5GbrqTreqWkIIuU519RB84hnep0XPllrN49NcSoEABCAAAQhAAAIQgAAEIAABCEBgdAIIIJkWTFvB/CnJdY94DcI9sR7Uy9ZXmbD7J9NVH3rWh9qbCwIQOEJgRUKIrnq5lR/bYtXfCfrs1PNAGFvrWVICBCAAAQhAAAIQgAAEIAABCEAAAhkEEEAyIGkSCfZt5Y+vUvIfJICjXyFnX0lAuSOIlo2sV0JWffQiT71DEliDEJK2xdIx/5shjRSr0bqdmX5AwHlKsexCayAAAQhAAAIQgAAEIAABCEAAAlMSQADJMKsEvzTw9X1K+kYCN7oVluli6ysTrh6JWfXRgzp1TkNgJULIUzGYPg90JSBXOYHf5Tl6XZ6dnBCAAAQgAAEIQAACEIAABCAAAQhAII8AAsgFTgeHnmvqL63bd6TA4P/yTEKqDgR+FZvqNjdcEIBAJYE03m2kmB4iwf0ZIS0P206r+W6lnp0oXklstdkZd1drejoOAQhAAAIQgAAEIAABCEAAAhBYjgACyBnWB4eea0pzwObg7JDlLEtNOQR0KxY962Obk5g0EIBAPoEkHt9Kjh5CiN7bty0P3O4s9OQbInZKDkWPbR9aBwEIQAACEIAABCAAAQhAAAIQGJ4AAsgJEybhQgPjj1MS/bL4yrpv+cH2WcM7zEQdeC19UfGDfegnMipdiUUgjaO6ukp/PQ4R163tnrcSOVkNUu1vHIpejZACIAABCEAAAhCAAAQgAAEIQAACEDhHAAHktADySv5p/8Bb85eqaQXJH7hgKAIacNMvw02H2IfqAY2BwGAEklCg99x3nZr+u9Srguddi/pZDVJFVUWqa8ToKoZkhgAEIAABCEAAAoTpgMwAACAASURBVBCAAAQgAAEIQOAEAQSQI2AkmKVfK/+8909FB7ZKOVsp4yu8LwwBDjoPYwoaskYCSRRWIaTXuPir1K0CqPvKL1aDVHn0S7HJTVUJZIYABCAAAQhAAAIQgAAEIAABCEAAAkcIIIAcQDmxaqPk4HMN5rzA68IQeCkt0a1w3AOfYXpIQyAwCIF0PogKIT22xdJVYDoWbFrgkr49lXK17B59a9Glpcr8gZV5S6GmHghAAAIQgAAEIAABCEAAAhCAwHoIIIDs2Tp9wXt3ELgyf5l6opz1eFW8npq3L4vXBVoEgbkIBFgxodtiqRDypzfZ1DcVQfa3UfSuZsbyzB8bzAiBPkEAAhCAAAQgAAEIQAACEIAABCDgRwAB5KEAspX/u781Cwef+/laj5LeSqVPWwQ4e3SGOiEwI4G06k7Fgsed+tdyW6zD7RQ7dXGYanXMfsJKvWHsRUMhAAEIQAACEIAABCAAAQhAAALhCSCAJBMdOfdD/+UnCcTcWqzIwecWWk3T6tfdKn6w5VVTzBQOAR8CaQzW8bbH1lEqdush6a98evOhlAACj3eXWpf3Wuyg24hxQQACEIAABCAAAQhAAAIQgAAEIACBagIIIILwhGjxVoIwV1bCHHxuJdYk/a9iO/3ymgsCEBiIgIyfOuZu5NfrkHQVTlUIufPElrbE0jNPvvMsd+Ky2LZwYuPSNQhAAAIQgAAEIAABCEAAAhCAwJIEVi+ApMCU7gH/+QH4b61fA6fDb39b0oDU9REBAmc4BQQGJ9D5IHFdDXLb4kDuzoe/j+QVaoNrti8cyWS0FQIQgAAEIAABCEAAAhCAAAQgEJMAAsi//+qWJ4cH1f4ugZdrq8kkuHV3REixFkP6MgIEzMq4kQsCIQkEOEi81WqQJwJ8I79eZ56EtPeRRr2R57Cy4oIABCAAAQhAAAIQgAAEIAABCEAAAsUEVi2ApK9xXxyh97UEXrYWqlLWraT/0ZKHtG4E3khJet7HnVuJFAQBCIQg0HnVRJPVIAHEnRC2zWgE2xlmQCIJBCAAAQhAAAIQgAAEIAABCEAAAqcJrFYASXvN69ZXhwfumg9gTcEsDb73OLx37f79WgDonv0cdr52T6D/0xKY+GyQWzEawvl5zzV/kDDtjUDHIAABCEAAAhCAAAQgAAEIQAACEDATWLMAshVaxw7a/cK6kkCCc3q47fdm+mSoJfBSbHVTWwj5IQCBMQjIWPtcWvpzp9a2Wg1yLf3RrRgR0I8bVrlfIXJ38nqqhQAEIAABCEAAAhCAAAQgAAEIDE5glQLIme2qzAH19GXyX4P7wYjN57DzEa1GmyFQSUDG3N5naLivOkvPERVBOBfkuH+YV2ZWuhnZIQABCEAAAhCAAAQgAAEIQAACEJiEwOoEkBQ8++OE/UpWf2ykrO8m8YdRuoH4MYqlaCcEGhBI2w7qyrteY6+uStBzh7Ze3eNckIskvxXeKhJxQQACEIAABCAAAQhAAAIQgAAEIACBbAJrFED03I9jX9my+iPbbbol1KDjtQTB1IZcEIDAygl0PiBd6bsf0s2Wiiedmq2wVn6/030IQAACEIAABCAAAQhAAAIQgEAJgVUJIBcCSyWrP7YC/dg5IiW2IM95AogfeAgEIPARgQBbYr2RRulqkDsv8yRh54VXeROV87twvp6oP3QFAhCAAAQgAAEIQAACEIAABCAAgcYEViOASEBJgyb/O8HzJwmq3FpYXyjPUhRpLxN4mwKMrPy4zIoUEFgdgSBbYt14btGUhJ2tGJPD0R969A/CWbc/44IABCAAAQhAAAIQgAAEIAABCEAAAhcJrEIAScExDZ5/foRI0bYaUqYGplj9cdHFqhPo19W67dU/1SVRAAQgMDWBACsnXLfECrC6JaK/6DP7ieeKm4idpE0QgAAEIAABCEAAAhCAAAQgAAEI+BBYiwCiX4t+fwJZydkf11LWqdUkPpahFCWA+IEfQAACJgIBVk64jltJwN8KhGNnV5nYTJSYrbAmMiZdgQAEIAABCEAAAhCAAAQgAAEItCQwvQCSsVUVZ3+09LDysl2DiOXNICcEIDAagQCiga5S0HNBVLiovlJ/NlLQN9WFzVPAM+GrTLggAAEIQAACEIAABCAAAQhAAAIQgMBJAmsQQO6k98e2vlIorP6IeXMgfsS0C62CwFAERDjQAPl3HRvtel5FgP50RPlR1UXbV0bqAG2BAAQgAAEIQAACEIAABCAAAQhAoD2BqQUQCRbdCsIfz2Bk9Ud7H7PWgPhhJUZ6CEDgJIEA54KYhfZz5pT+nNvScW2e8FpWgTxdW6fpLwQgAAEIQAACEIAABCAAAQhAAAL5BKYVQNI+8H+cQWEOSmVsp5VPnpTHCCB+4BcQgIA7gTR2v5KCP3EvPK9A17EtgKiT1+tlUn0rIojalgsCEIAABCAAAQhAAAIQgAAEIAABCHxEYGYB5E/p7blDY1n9EeuGcA0QxuoarYEABHoTSKK4BspPbYnYuom6ZdO1BOv12VR9IYK8R/hW/uuJcP2nGioFQAACEIAABCAAAQhAAAIQgAAEIDAdgSkFEAkMPRdL/XzGWr9LsOTaYk0p80rS/2XJQ9psAogf2ahICAEIlBIIcjj6c6/DuxFB3nvCr8JUn/tcEIAABCAAAQhAAAIQgAAEIAABCEDgAYHpBJAkVOgXtue2OvlagiVbiy9w+KyFlikt4ocJF4khAIEaAkkE0XM0pjgcXfqjZ2BsLjzzapCNkvdLr9U1o3SYdkIAAhCAAAQgAAEIQAACEIAABCBwmcCMAohucfLNma6/kSDJk8toPqRg9YeFlikt4ocJF4khAAEvAgFEbfM5VKf6nrb32sq/9zrjxMssNeWYn+01lZEXAhCAAAQgAAEIQAACEIAABCAAgTEITCWAZB5S/sy6/YiUq18Lfz+GSYdpJeLHMKaioRCYk4CM7bfSsx879u53qfupx/kViCD3VvxBWOrzmgsCEIAABCAAAQhAAAIQgAAEIAABCNwTmEYASdua6NZX5w64fSvBkSuL7VO5d5JnzV/WWpDlpHU9DDinQtJAAAIQOEYgwDkabmIwIsgjfbZceQhK3C0QgAAEIAABCEAAAhCAAAQgAAEIzEFgJgHkVkxy6UvenyQwoumyrwBfCGe3dZCEiB+DGIpmQmAtBAKIIG+Fta4EURG/6kIEeeS2tViVIcgMAQhAAAIQgAAEIAABCEAAAhCAQAgCUwgghjM6PrN+GSpl34mlzq0qCWHIQRqB+DGIoWgmBNZGIMBh4m7jIyLIo6/lWb9dmw/TXwhAAAIQgAAEIAABCEAAAhCAAAQ+JjCLAKKBjq8uGNj8VWiAr4Jn81nz+SuzAaA/EIBAXAIBhANEEB/34EB0H46UAgEIQAACEIAABCAAAQhAAAIQGJ7A8AJI5sHnaqgvrduLSNk5wsrwTrBQBxA/FgJNNRCAQDmBACKINt5lvDQ8H8uBxc3pwjBu92gZBCAAAQhAAAIQgAAEIAABCEAAAjkEZhBA7qSjl7ao+l3Ej+scILs0KQj2hyUPaU8SMK++gSUEIACBXgQmE0FuhOOLXiw71suB6B3hUzUEIAABCEAAAhCAAAQgAAEIQCAKgaEFEMMB5eYvQaXsjRjpuyiGGrgdiB8DG4+mQ2CtBBBBprD8r/Lxw/MpekInIAABCEAAAhCAAAQgAAEIQAACECgiMKwAIsGpT6XHd/L75ELP30kARNNmX6nsv7MzkPAUgTfyD9fWg+fBCQEIQCACAUSQCFaobsMX8gzSuQIXBCAAAQhAAAIQgAAEIAABCEAAAiskMLIAshF75azQMH8BKkEv/WL05xX6g2eX2X7EkyZlQQACXQhMJoLcCsQfu4DsV+lrEUCe9quemiEAAQhAAAIQgAAEIAABCEAAAhDoSWBIAUQCUlcC7a9McOavP6X8Oyn70rkimdWvMpmKH7ry489V9p5OQwACUxGYTATZiHFyPh6YyYZfy/NoO1OH6AsEIAABCEAAAhCAAAQgAAEIQAACeQRGFUA0kPFVRhffSNDjSUa690kk0HUt/+d/ljyk/YiA+cwVGEIAAhCITAARJLJ1LrbNPBe4WCIJIAABCEAAAhCAAAQgAAEIQAACEBiCwHACiFGgMAfiOfy82m9/EtHptroUCoAABCAQjIA8H26kSS86N8v8XDtsbzrnait//7hzX5asvprbko2lLghAAAIQgAAEIAABCEAAAhCAAAR8CIwogOi2SjlBG/MZFBx+Xu1UXfZaT19m/1Ld+r4FbEQ42vRswiQc/xSOeoYPFwSaEEAEaYJ1iULfythwtURF1AEBCEAAAhCAAAQgAAEIQAACEIBAHAJDCSDGwNNLCXbcWFBz+LmF1kdp38jf6Lkf/1SVUpDZuCqooIZFsnRfOTMJx9/FB68XsRiVrJaA8VnUilP1ioZ0npZ+VPBJq0YGK7f7OBuMB82BAAQgAAEIQAACEIAABCAAAQhMT2A0AeROLJJ7OPm3Egh9ZbGgBINyV5dYil1D2q6Hnk8SuO8emJuEIwLIGkacAH2U++VWmvFj56Z4iCB6TtZ2JSKIeWVoZ/tSPQQgAAEIQAACEIAABCAAAQhAAAKVBIYRQIyrM8xbXaTtf/6o5LnW7NVBuBpwkwTuEUBqnOBDXgQQH46UkkEgyJlRX4vYrwJG8RVkRUtx+40Zu4+1xvaSHAIQgAAEIAABCEAAAhCAAAQgAIEKAkMIIOlsjjvpZ+42Hb9azwGQOvQMie8rWK41q5m1NygEEB+ik3BEAPFxB0rJJCD3ja40/CYzeYtkLivwgqxoacHnsExWgSxBmTogAAEIQAACEIAABCAAAQhAAAJBCIwigNwKL8tWI1+KAKLbWWVfEvzRsytyBZbscidP+EY46/YpXa9JAvfdv0qehCMCSNe7cX2VJ4F+Kz1/3LH3XiLIRvrwXcd+LFV1d+F+qY5SDwQgAAEIQAACEIAABCAAAQhAYO0EwgsgBas/Sra/eiqO8NvancHYfw24PREB5M6Yzz35JIF7BBAfz0AA8eFIKQYCBc8pQ+nZSatXNgQRc7I7XJnwiwjPr8o+kB0CEIAABCAAAQhAAAIQgAAEIACBCwRGEEBupQ+W1R8/SFBDt7PKvoLs457d3iAJzYfMt2o3AogP2Uk4IoD4uAOlGAmkc6S2kq3nSsI3Uv+1PAN1RWPRFUTMKWq7MdNL4XRjzENyCEAAAhCAAAQgAAEIQAACEIAABAYjEFoAKQzEmL7qTHX8PZjdejc3VOBoksA9K0B8vBoBxIcjpRQQkLEowmrC6q0Jk5jzRwGC0bKY5gujdY72QgACEIAABCAAAQhAAAIQgAAEIPDoUXQB5FaMZFn9YQ78SKDnRup4gTNkE6j+wji7psyECCCZoC4km4QjAoiPO1BKIQG5j55L1p8Ls3tlqxapV/JsrObkZTDKgQAEIAABCEAAAhCAAAQgAAEIQKANgbACSOHqj5Ltr14J2m/a4J2yVPMB860pTBK4ZwWIj6MggPhwpJQKAkG2Vaw+6DtIPyoskZWVVSBZmEgEAQhAAAIQgAAEIAABCEAAAhAYk0BkAeRWkFpWf6gFTIEMtr8yO61ZYDLXUJABAaQA2pEsk3BEAPFxB0qpJCD3059SxOPKYmqzP5NzLjY1hQTpR00XLuVlFcglQvw7BCAAAQhAAAIQgAAEIAABCEBgYAIhBZDC1R9vJdBzZbHFSrb4sCA5lzZsYHmSwD0rQHw8Nayf+nSPUkYhUPgca9G9r+XZuC0tWPqhz1UVc3oe7l7a/Nx8po8ncgslHQQgAAEIQAACEIAABCAAAQhAAAL9CUQVQG4FjXX1h3m7DwnssP1Vng++k2RPJIh2l5d82VQIID68J+GIAOLjDpTiQCDIPaXj97WM3ypiFF1BDncvantmJvP8IbNckkEAAhCAAAQgAAEIQAACEIAABCDQmUA4AaTiq1nT2RRsf2XyvOptVEy1GRMHCTIaW/1RclaA1BL8Lz8CiA9HSnEiEORQ9DfSHRVB/intlvTjF8n7fWn+4PlUJLqq4RO8fzQPAhCAAAQgAAEIQAACEIAABCCwWgIRBZBbsYZ19QfbX7Vz4dcSFHrarvj6khFA6hlqCZNwRADxcQdKcSQQZLVh9b0x+Xkg3UVoR5ejKAhAAAIQgAAEIAABCEAAAhCAAAQSgYgCiH6hat1r3HyIaZCAVHRHHOKr2EkC992Db5NwrA7yRr8pad94BNKKw620vPeh6FVbPU1+HsgQz7vxvJ8WQwACEIAABCAAAQhAAAIQgAAE+hIIJYBUHEr+raxS0PM8si+pq0RoyS5/koSht77aMZ4kcI8A4nPTIID4cKQUZwIyTj2RIlUEsQr8zi15VDWuT34eyA8yl9CtvrggAAEIQAACEIAABCAAAQhAAAIQmIRANAHkTrh+bmUrAQtTPyYP4FjxnUo/TCAZAcTH5JNwHMZvfaxGKSMRCHIeiMeh6LOeB2LeTnMk/6OtEIAABCAAAQhAAAIQgAAEIACBNRIwCQctAVWs/jCfUSF1baQv37Xsz+Bla4DsiehKKkiFvyYJ3LMCxMfTEEB8OFJKIwJBtl98m8b4okPRA23p1cJKVStkWjSIMiEAAQhAAAIQgAAEIAABCEAAAhAoJxBJANlKN74q6Ip5ywoJ3mhg37zSpKBto2YxM+3ZUQQQH/qTcEQA8XEHSmlEIIkHfwZ4Bpk/HthHkrb0+qMRpp7FsgqkJ33qhgAEIAABCEAAAhCAAAQgAAEIOBMIIYBUBl6/sKxUmDho4+Uab4Sn7lU/zFXpP1H6yQoQH0sggPhwpJSGBAKNWVVid5AtvVpY6mt5Dm5bFEyZEIAABCAAAQhAAAIQgAAEIAABCCxLIIoAspFul2xJZf5SUwI2t1LXj8tiHqq2LyXwo18nD3MFCibWMEMAqaH3IS8CiA9HSmlMINCzqCrYL/3YCqqS1ZuNCVcVzzhShY/MEIAABCAAAQhAAAIQgAAEIACBOAS6CyASPLkSHH8VInkpwfobS16pT4P7jy15VpT2V+H5fLT+IoD4WGwSjgQufdyBUhYgEOR5pGc+XcnYX3oeiD7D9bn6yQLIlqzCtLp0yYZRFwQgAAEIQAACEIAABCAAAQhAAAL5BCIIIL9Ic7/Pb/KDlKbDStPe638X1jV7tqogWE84kwTuWQHi40QIID4cKWUBAmlLxm0A8aDqvpF+PJU+/LYAsiWrMH9gsWTjqAsCEIAABCAAAQhAAAIQgAAEIACBPAJdBZAkSNxVBH8+s3y1OmmQJs/Sl1OZxKTLxS2XAgHEh/UkHKsCuT4kKQUC+QQCnaNRex7IK+n1N/k9D59y2I8CwpOlgRCAAAQgAAEIQAACEIAABCAAgQUJ9BZAbqSvLwr7az6sWwJNG6mr5KyRwiYOk23ooPEkgXtWgPjcLkP7sg8CShmNQKBzNIrPgHL4oCGi2apEoYgdok0QgAAEIAABCEAAAhCAAAQgAIG1EegtgNwJ8M8LoZvPq5AATU19hc0cIltx0CtC7xBAfKwwCUcEEB93oJQFCaSzsCKco/FWuv3EsrJyH9OEqyzfCourBV2BqiAAAQhAAAIQgAAEIAABCEAAAhBwJtBNAHEItn4rgQndciPrqjxsPauOQRMNv8+5gy9FMB0rQHysgADiw5FSFiYQaCusqmeC9GO2rbBMc42F3YbqIAABCEAAAhCAAAQgAAEIQAACELhAoKcAspG21WxHZT3/40bqK91ua1ZHmmKPcwQQH/echCMCiI87UEoHAoG2wioO+gdazeJlwdfysYUe8s4FAQhAAAIQgAAEIAABCEAAAhCAwIAEugggDqsxOP/Dx9m6rzrw6MYkgfvutpiEIwKIx01FGV0IyD34RCr+o0vlDyutEscDrWbxQvmFiCB3XoVRDgQgAAEIQAACEIAABCAAAQhAAALLEeglgNxKF3+s6KZ5iw4JyPwj9X1SUedsWafZ23ySwD0CiM8dhgDiw5FSOhGQ8az2+ejV8qp7KdBqFg8e3cdnj05QBgQgAAEIQAACEIAABCAAAQhAYI0EegkgdwK79PBztdMz+Rpzk2swhxUnuVWNlM7EMHLHEEB8rDMJx6qgrQ9JSoFAHQG5F2ufkXUN+JD7B3nW/lJSWKDVLCXNP8wzzQcDHjAoAwIQgAAEIAABCEAAAhCAAAQgMBKBxQUQCYroXtq/VUIybUchdd5IfZz/8QH6VEHiSQL33b8wnoTjVL5dOU6SfVACge5F3QrrSen2T4FWs3h4QvG5KB6VUwYEIAABCEAAAhCAAAQgAAEIQAACZQR6CCCvpKnflDX3Ptc7CcZ8askvQZiNpK85cN1S3QhpvxaG2xEamtPGQMHCnOaeSoMAUkPvQ14EEB+OlNKZQKDnVvE9JX3QZ/Wf8qtZ8dnZEu+r5zD0KJagHRCAAAQgAAEIQAACEIAABCAAAQOBRQWQFAz529C+Y0nNQYhA24lUdt0le3Ewy6X2BoUggPhAnYTjdP7tY11KGY1Ael7eSbsjnF1VsxXWtfThf6PxP9Fe0+rTSfpMNyAAAQhAAAIQgAAEIAABCEAAAkMTWFoAeS60fq4kZvpSnvM/PqL9paz+0C9yp7kmCdyb/LqF8SbhiADSwjkoswsBuSc9npkeba/dCqt25adHHzzKKBaCPCqnDAhAAAIQgAAEIAABCEAAAhCAAATsBJYWQO6kibVbYZi2b3I6c8RONmaOlyJ+3MRsWnmrJgncI4CUu8B+TgQQH46UEoRAoBWMxfdW+hBBhfcIq1lqLMth6DX0yAsBCEAAAhCAAAQgAAEIQAACEOhAYDEBRAIgT6R/fzj08TMJ4v+TW47U+4uk/T43/eTppty+AwHEx2sn4VgcpPWhSCkQ8CUQ7L4sPgh8ogPRp1tF6euxlAYBCEAAAhCAAAQgAAEIQAACEIhFYEkBZCNdrz2I3Pz1pQRdtlLvV7Gwd2nNlKs/lGSwAGGpcVkBUkruYT4EEB+OlBKIQKDnmG6FdWX5CGGHcaID0ad9lgZyeZoCAQhAAAIQgAAEIAABCEAAAhBwI7CkAKKrNmq3vyg5AP1fN1pjFzTl6g8EED+nnERIQgDxcwlKCkIg2FlWxQLAJFtSvhMB6NMgrkEzIAABCEAAAhCAAAQgAAEIQAACELhAYBEBxDHoYfpKfpKArocTFwesPCpvXcYkdjb5dgumk3BEAGnhHJTZnUCw7RxNZ3Htwwu0mqXGpsVbgdVUSl4IQAACEIAABCAAgfME0tbrV5JKt2A/vO7kL+7kY5YtHCEAAQhAYF0ElhJAXgnWbxzQmoIu8vB7LnX+7FDv6EVMu/pDDTNJ4B4BxOcuQwDx4UgpwQikLaT0pa12JaVHz97Ii+Oxl8qLZTueB3axroYJzKtRG7aFoiEAAQhAAAIQgMDqCKS58dPU8d2flpjT65R3I/NajVdxQQACEIDAxASaCyDpwfS3E0PrAegbqbf23BGnpncrZurVH0oVAcTHtybhiADi4w6UEpBAsIPEi0Vb6ccMz2bTfCSgO9EkCEAAAhCAAAQgMCwBBJBhTUfDIQABCHQhsIQAciM9e+HQu5ID0P+Ueh871D1yEV/KFw3KYdprksB9cTDRy7CTcEQA8XIIyglHINhB4nog+hN5vtxZQaUzTfS5FGE1i7X5u/TPpO+b0szkgwAEIAABCEAAAhCwE0jzYd3pQ39ec8m3UtYtczu7PcgBAQhAYBQCSwggW4HxlQMQc2BTHo5rPwDdzMzBTosXMUngHgHEx3NW4fM+qChlRAIy3t1Iuz0+KvDofvFWUMFWs5SwKO57SWXkgQAEINCLQJpnX/eq/0i9ul3NXaD20BQIQGAhAmmL81upzkv4OGz57/IX99tpyTjzz0LdohoIQAACEFiAQFMBJH3l+ZdTP0wB4kmC4rXoTGem1FbWK/8ktjb5dwvWk3BEAGnhHJQZioDcqxr4+TxIo4qeM8HONClFOfX5WqVQyAcBCMxFAAFkLnvSGwiMTAABZGTr0XYIQAACfQm0FkA8DyE3bTcR7CvZHlZeTSB4ksA9AojPXbIav/fBRSkjEgj2fKs5EN1zjtDDlD/I14G/9KiYOiEAAQgsRSDgir0i4X0pXtQDAQj4EkgfzWihG/lZDjkvbYhu86rX9exbiZcCIh8EIACBEQm0FkA8z+AwTXblQalBie9HNIpTm02CkVOdXYpBAPHBPglHBBAfd6CU4ASCrQIpFgKC9cNq9WLxx1oR6SEAAQj0IoAA0os89UIAAkn82CYSS5/tqkLItdaNEIIvQgACEBifQDMBxHn7K33omNoq9euD0uPskRGt/FZwXY3Y8JI2TxK4ZwVIifE/zoMA4sORUoITCLYKRF8Qr0r2Sg7WjxKrsw1WCbUF86Q5wn6N98GMjOtO0uhvd91x7kAGNZJMRwABZDqT0iEIDEFgT/xYWvjY58NqkCG8hUZCAAIQuEzAJCpcLu5DirQ/48+WPGfSmgP6Uv+aD0Av/hrXyV6LFoMA4oN7Eo4IID7uQCkDEAi2eqJYxA3WD6vlV/W8tcJZIn0KkDyRuq5Tffrfn8qv1Ucwb6TsO/npKudtqvPPEgFwCT7UAYFaAgggtQTJDwEIlBCQsWcj+b4rydsgjz77dUssDkZvAJciIQABCCxBoKUA4rn9lSmo6b36ZAlDONZR/CWuYxsWLWqSwH1x8NAL9iQcTWOFFzvKgUAPAgFXTxSthgjYD4s5X8vL8FNLBtLWERB/2fG+1mCE/Hp+GbrfGQ2ObNNfbNguo87O5I5DAAEkji1oCQTWQiA9638L1t/u7+vBeNAcCEAAAkMRaCKApK/x/nYkYXrYTBLILcX3q7x068Gyq7kmsbfJx1sYdxKOCCAtnIMywxIItnripTx/bkpgBeuHtQuf8UWgFVl++hQEUb9a4uDT/IZdTvlWkrxKyRBELvMiRVACE5Y4bwAAIABJREFUCCBBDUOzIDAxgcDzwqKPfSY2FV2DAAQgMAyBVgKIvqi+cKRg2mIi4ETdEcXFolb3UJ4kcI8ActG1sxIggGRhItEsBAKunvhaxICtlW/Afli68Ez6vLFkIG0+AQSQfFakhEALAgHfq4qeMy3YUCYEINCGAAJIG66UCgEIQGDNBFoJIPrFm+eXeqaJbrD9Ipf0r1UGfxFAfFxsEo6rvAd8PIBSRiUQ7CWx+B4M1g+LO7ANloVWRtq0lamuZr2R3ycZWUZI8rs0UleCbEZoLG2EwI4AAgi+AAEILEkg+Ecxxaudl2RIXRCAAAQg8DEBdwGkwfZX2mrTqgZpw1bytDr8MrIffSsv1rvtFiK307VtkwTuWQHi4xXFwVef6ikFAssTkDFQA8U/L1/zyRpNHy3sBdlu5L89V48uhkSeve7zqcUaH6SiJHpoa27lF+XQ0xZ0dGusW4SQFmgpswUBBJAWVCkTAhA4RUDGHM+zZL1Bv5Pn96fehVIeBCAAAQi0J+D+wt7iwCprYEHa8I+gm+WLwVwveCucrnITz5QOAcTHmpNwRADxcQdKGYhA+vDgLtBz7408j56UIBx4FcgqP0AosfFhniR83Mrfzyx6HEOlK0K0349Kto3zYE8ZEMghgACSQ4k0EICAB4E0J/jLo6yGZTDnawiXoiEAAQi0ItBCANk4v8SaAimNVqC04u9ZbvcVBJ6dsZQ1SeC+u/0m4YgAYrl5SDsNgYABqqJzMYJve3DOX9gSoeBuCui3Bb1wyfKrlKKrQvQDHi4IhCIQ8D4tWmUYCiqNgQAEjhIYZB74qzyvdfU1FwQgAAEIDESghQDivfrCFNCcJIhb4kKmbcJKKoiaZxKbI4D4OJhpvPCpklIg0J9AwC/milclDroKpLi//b1n+RaIjXWF0EZ+j5evPWyNujXWUwmq6NYfXBAIQwABJIwpaAgEpicg480v0snvg3eU983gBqJ5EIAABI4RcBVA0gvtH86oTQr7IF8NOCN6tOoDWBFAfNxpEo5MSH3cgVIGJCD3sAaUI20jtLZVIF8SvD5/44iP7r6YjHRmTbS7vei+idYJ2jMPAQSQeWxJTyAQnYCMN1tpY/SzXE07lERnTvsgAAEIrIWAtwByK+B+dIZn+jI+4CTdGcfR4la9D+UkgXuTn7dwqkk4IoC0cA7KHIJAwHu4eFXEoKtAfhABRL9c5DpBAAEkyzUQQLIwkWgpAgHfrdgCaynjUw8EFiaAALIwcKqDAAQgsCIC3gKILtv33s7AFNwP+AVsa3d6JwGXT1tXErn8gEG/ElwIICXUPs6DAOLDkVIGJSDjYYvncA2NomBuCpSPtkqA8eeIp4gtdY6yE4YirVCq8evWeTlTpjVhys8mgACSjYqEEIBAJYFBBJBHEn9xjaNVYiM7BCAAAQhkEHAbuNML7t8ZdVqTmL7yGeWhaYVwJr1pizDHesMUhQDiY4pJOBKA9HEHShmUQMBtIItWgaQ5xZ2Y4ZPBTPGZvBNzkHUyWrLjVv6v98cxg7lFUXNf7nKJT90UlUAmCDgQQABxgEgREIBAFoFRYjkIIFnmJBEEIACBUAQ8BRB9OXvRoHemYII8NL0PYW/QJdciV7/n+CSBe1aA+NwWCCA+HCllYAIBn4Olq0BuxQze22q2tqxp1WrrxvQuHwGkygIIIFX4yOxFAAHEiyTlQAAClwgggFwixL9DAAIQgEApAU8BZCONcN/awKquy0Pz31IYA+bjAC4xGgKIj+dOwhEBxMcdKGVgAnIv63ZD3wfqQukqkCvpw1+B+pHTFLYuSpQQP3LcJTsNfpWNioTeBBBAvIlSHgQgcIrAIAJI0bwWq0MAAhCAQF8CngJIi5UXpoeLPDCfCM4/+iJdtHYOXEUAcXM4BBA3lBQEga4E5F6OKByUrgLZCEz3jysaGsg0b2nYjq5FI340wY8I0gQrhV4igAByiRD/DgEIeBEI+BHPsa7xwZ2XwSkHAhCAwIIEXASQhsKD6eEySQDXYv4vZIXMnSXDjGknsTtbYPk4p2nM8KmSUiAQj0DAL+iKhIGG84uWRlv9s1ns9qcA5twPfy/rPlfw7xIlRieAABLdQrQvEoHO76XDPyOE343Ys8W26p5usvozWD1hUhYEIACBpQh4CSDPpcE/N2i0KZg5yAPTC5OJjVelEcvpPNH0QtJ9wjoJR+4LL4+knKEJBH0eFp2PEVDMueQbRatdLhU6wr+LrTapnSOt2hkB7X4bi+6j0TpJe+MQQACJYwtaEp9A5/ep7u+TtRYKuor5sFurnefV2pf8EIAABHoS8BJAXkknvmnQEdNDPOAEvQGS90Xy4E0oOk80vWxs8nWvSvfLmYQjAkgL56DMIQnIPd1ia8oaFkX3Z1Ax5xyH17I682kNqBHzDminETFrm9/J70p8TO9vLgg0JxDw/epr8f9t845TAQQKCHR+n+r+PlmA7KMsA3z48hnPYA9LUwYEIACBZQl4CSCtgiymh/gge0Z6WZgHbyLZeaLpZU+Tr3tVul/OJByLAqwteFImBHoTSF/jR/sSvyhwJX25E56f92aaWf87eTH+NDPtNMkQQBYzJQLIYqipSAkggOAHEMgn0Pl9qvv7ZD6p0ykRQDwoUgYEIAABCBwSqBZAGu/PbTrke4CHpZcHrvLr0lPwOk80vWzafcI6CUcEEC+PpJzhCTR+PpfyKXp+BQzAXer/lyKC6DkYq7jSlhXa309W0eH+neRZ198Gq2lBwPG3SEhfjcHoaFcCnd+nur9PesDvzPBSF17K/O7mUiL+HQIQgAAE4hHwEEBanf+htEwT3BUJIGx/tXcvBZ8k5d713Sesk3AkKJTrcaRbBYGgKye+kJfHO4sBpB+6ouJvS57OaU0fcHRua3X1K5p/VbNyLOCZliX30saxTIqCwEcEEEBwCgjkE+j8PtX9fTKf1PmUgecVpviUFw/KgQAEIACBegIeAkir8z+0d6YHjDwoW23FVU/atwS2v9rj2Xmi6WXZ7hPWSTgigHh5JOVMQUDu65YfKZQyKvp6LuiWXqcYFK10KQXaMx9bX3Wjr1th6cV5IN1MsI6KEUDWYWd66UOg8/tU9/dJH4r3W+9dSVnRVpYWzV+9mFAOBCAAAQjUEfAQQFqKDqYtJORB+W8djiFyE+A9MFPniaaX03SfsE7CkfvDyyMpZwoC6QXyr2CdKTrDYLAx6q18ma8v71NfaWWOBihGOZ9lRnt0nz/MCJU+fSCAAII3QCCfQOe5ylTPg2AfWBTNXfM9h5QQgAAEINCaQJUAIg+lJ9LAP1o1UoIHpvatRABZ1bYaOb7VeaKZ08ScNN0nrJNwRADJ8TbSrIqA3NsaoH4crNNFz7KgW3qdQmve6iuYjS42J2Bg9GKbJcFb+d2dSKjz2tHOMSEok2N10hQTCHifm3YIKO44GSFQQKDz+1T398kCZGezCM9fJMH33uUWlMe4UwCNLBCAAAQiETAJDIcNb721hkUA6TzZWNKm0wdUrDAnsX33CeskHN0EkPTV0Y3VH4Ol37A/fDCLdGhO62d1YZeKVkgE7cspBM9mv//EHi1XARe61oNsKg7oVq3602srNtE2n7zSqpankkB/33g0YoEyus8hFugjVXQigADSCTzVDkmg8/vUlM+CJIKoP/QQQjhva8g7kUZDAAIQ+JhArQDS8vwPPdgxu32dJxtL+dYbQaJfJ3LtEZjE9t0nrJNw9BRAbsXNfhz8ZuvuV4Pzm6L5QbfBUrbfyjNtF5jOYp2C06Mchv5S+neT1bFBEyGAhDEcY30YU8zXEASQ+WxKj9oR6Pw+NeWzAAGknb9SMgQgAIE1EcgWGI5BabwVhSmQ2XmysZTP/CrBFD3QlgsBxN0HJrmHTOPGOYgBX/hLbD7li1AJiLXnCboNVtH9Kn3ZiD2/G8CmU3+0kFbJvQhsh5+kbb9cWvFx4TlwpWWkNJFXg7ANVmBHHL1pAedDbEUzulNN3P7O71NTz/s7zDumX8k78a1I1yAAAQh8RKBYAFngi1JTYCTg5LyFu5kOhW/RgIhldp5oeiHpPmGdhKNp3EAA8XJfyolOIPDWUV9IgPrOwk/6olsT/WbJ0zHtZzUB+I7tvlh1UFFN261iwLVw17Nv3K4UeFExJOoZIQRq3KxNQfsEAr5jIYDgomEJdH6f6v4+2dowwld3w9jIr9XZdnpOmM4zdTcS13lEazaUDwEIQAAC5wnUCCCtAxCmQGbAybm3772Th/Cn3oXOUF7niaYXwu4T1kk4msaNc8abZEzp7ldeNwjl1BFY4KOF0gYWrWxsvAK1tC/H8k0XqEvBB+3rH56gHMrSoIVeT1qJTqnvW6kjoggy9YojB/+giEICAedD042rhaYhW0ACnd+nVjPvTx8l3IoLfO7oBr9KWbet5hCO7aQoCEAAAhAoIFAjgOhXcC0PojIFMgNOzgvMcTbLS3kY33gXOkN5nSeaXgi7T1gn4WgaN84Zb5Ixpbtfed0glFNPIOgX+0Vb9wx0f053Dwr73ZZQLeeAVoe/X/WhmVp/sRl8BRIrha2eQ/qLBAKOtwggF61Ggl4EOr9PTTfnuGTHJIRosvtVG3JZtqvcfTixkXwb64rkS23j3yEAAQhAIBaBGgFkK135qmF3TA/w9EIe6WXcGw1bG5wg2nmi6WVnk797VbpfziQcEUAeOkd3v2rhq5RZRiDwNljm51vgFS2HxnktL9S7l/IywwXLlVbfaKs8v7qs7eWiY13gOWfRiqpa+OSfmwACyNz2pXe+BDq/Ty36LPQl51Oa8NcdM3SbrGv5XaXffuFb+T//yG/b+oMJnx5RCgQgAAEIeBGoEUD+9WrEiXJMD3B52OnDrKUg07i7F4v/gq8SjjPqPNG8aLjMBCZ/zyzTlGwSjgggD63e3a9MTkjipgTS9j3Rti3SPhdt3RN0RcuhDd/Ks1tfwKe4ggpPizNOARbdGzySCFR8L03hnHSiGQEEkGZoKXhCAp3fp5j3T+hTdAkCEIAABHwIFAkgCwVRTA/wyQWQouCQj4vEL6XzRNMLkMnfvSrdL2cSjgggD52ju1+18FXKLCeQvt6PFrTVDplF/rTtwYtyGovlNPdtsZYZK0IA+Q8YAojRcUg+NAEEkKHNR+MXJtD5fYp5/8L2pjoIQAACEBiHQKkAciNdbB10MD3AJxdA2NLgzD3VeaLpdbeb/N2r0v1yJuGIAPLQObr7VQtfpcxyAjNt3ZOC0H+X01gs57eyCuTVYrU1rAgBBAGkoXtRdFACCCBBDUOzQhLo/D7FvD+kV9AoCEAAAhCIQKBUAGl9ALqyMT3AJxdApgmetHD6zhNNry6Z/N2rUgSQ0yQDvvCXmL27X5U0mjztCIhf63kUv7WrobjkdyIS6L7Npkv6o8KC5cBLU/lOiae5D4X3c2HysxMXr2K68A28AmmaFUdeDkI5dQQCzoe+lufFtq5X5IZAGwKdn5NdnodtSFIqBCAAAQhAwJdAqQCik87W522YJrcy2dDDrD7xxROmtM9koq/94zpCAAHExy0m4cgKkIfuwIuQz+0xVSlyr7c+w6uUl1nsDxyE3mcwzUHownsjHfuu1MCN8nUL+Kct5bRbkbaVeyZzRrUTFwRcCCCAuGCkkJUQ6Hy/MO9fiZ/RTQhAAAIQsBMoFUCWCJ5YBZAl2mQnXJ+D8z8uMJwkcN99wjoJRwQQBJD6UXfyEgKvmjALBWkbrDsxWeQPIN5KQPpqBrdKAf9Iwf6ubFOgS037YyD7sm1qIGPM0JTOAd1jCE3viDPYgD6MQ6DzrhTd3yfHsRQthQAEIACBtREwCyDyUH8ikP5YAJRpchv4i9ZaVLzIIoDU+lBWfgSQh5gCvvBn2fEgES9CJdQmz9N5e4ZLdM0rHoOuSjjsp7lfl0D1+PeAcy030buEp/C4Svn+KsnfKA8fzjQCu9ZiA86HTO+Ia7Ub/e5DoPNzknl/H7NTKwQgAAEIDECgRABZav9w0+S282SjpanNW4K0bEzEsicJ3HefsE7C0S0YFvCFv+T26+5XJY0mT1sCC37IUNKRH2S1hJ4zln1Jf5aal2S36UhC05ympqJWeYM+I0KMccLmT+H+uBV7a7lyD5nn99Y6SL8eAgHnQ8OPp+vxnvX0NM1FtMM9z1kL8Uxcj9XpKQQgAAEIjETA/IK04CTYNLmdWACZ4qvRljdF0KCMtcvdJ6yTcEQAeeh53f3KeiOQfhkCcr9HPTer6Ov1wP3ZGdQs7CzjCfm1BBWaQoxxC86Ncw32pSYUHUSFGS4IVBEI6N+md8SqzpMZApkE0tZXmrr1OannWhTimZiJjGQQgAAEIACBRQmUCCDbhR7spsntpAJIUSBoUQ8KUNkkgfvuE9ZJOCKAPLwnu/tVgCGCJhwhIPf7K/nrb4LC+UICt3eWtg2wDdZL6dONpU/R0iKAnLZIwAAxAki0G2jg9gT0b9M74sDoafpABBBABjIWTYUABCAAgVUSKBFANCixxAGYpsntpALI8AGTJe6qSQL33QPVk3BEAEEAWWLYGb4Oud+fSyd+DtoR83gYNDi/j9dtbOpls4BBUEVh9pUW/AI+P7/VforopkInFwSqCAS8903viFWdJzMEMggEegaEeCZmICMJBCAAAQhAYHECJQLIvwu1MvsL0ECTDm80z+TldeNd6GzlTWL/7hPWSTi6BRkDvvCX3Lrd/aqk0eRpT0D8+4nU8kf7mopqeCvPvitLTunPp5L+b0uepdNKn8xzrqXbeK6+oGNimDEu2Ic4P6ktxeVuI/kQbRmTQMB7HwFkTFeastVp/nEnnfskQAfDPBMDsKAJEIAABCAAgQcETC/jSwZILYGCJdu1sP98KRzYv/kC9Ens333COglHBJCH90t3v1p4zKQ6AwG556OeA6K9MD//gm/rpX3K/rDDYMbFkgbdZuy1zJOeLgbhTEVp+5Oee7/vtw4BJIJTTNIGBJBJDEk3XAkk4UPL3MrvsWvh5YUx7y9nR04IQAACEJicgFUAuREeL5ZgggBy/+WeyT5L2CViHZME7rtPWCfhiADy8Cbt7lcRxwza9B+B4ILBr/II1G26si/pz2JzlOxGPUw49FfLwQL8O7Lm1UKFtruYTfj8Iom+v5hwmQS/azVyD10vU13fWvYCkbqyTS/t91X66eqw3ODkW0l7Jz8Vh3cfIOn/1+vPtX6UhADS17+pPR6B9M60SS1bYmvwXAjM+3NJkc5MIK0e13z6rNVnrP6pz1j9b8t9cD9HSc9Zfd5u956z+v+5JiIgfqP+odd1+lP//+6/cz4c2vmLZt/5yp3+t8zL9E8uI4H0DFM76G93H+u9rFfunFnT7mxzOG8OO2c2BdiXnABbgv+TBG4P3dYtkGu8H4ZLPon9u09YJ+Hodt8sOd41vOm6+1XDvlF0JQHx8cjngJgD2ykIGnkbrKHvx6ACiN4FIVbWBLufphZA0r2uK3+u088SeKkcOe9f9rbye7UWQSTgfGhoMbnWAcnfj0B6V7qRFnzXrxVnax56nhGU6WqblQSP3bM2J1DtwUo/RNBnrF67Zy2iiAfZBcsQ31G/2flOyznavr/ovIyz75Kd01x5N0/Wv9X/togbHh6jc2b9oOj+no5gH6sAog1fZPBDAHlk/vrVw0NHLGOSwH33CeskHBFAHt7E3f1qxDFlLW0e4J4v2QZLJ1lLT+5yXWbo53pgAeSZzBk3uUZolS7Y/TSVALIneKj5VLiNco/ri/fuZXszqyCCANJq1KDcqATSeK7N260q0z81eNQykOeBg3m/B8WVlpEEj93qaw1eRzjXRq3xOplEn7ca5EYQCeajeytx1X9uOo6V76RuXZGtc7K7YJiaNudgrqz37zdNKywrXO2zTfbpIlZZBZDFAgsIII9CvNCX+fWyuYIFHUo7333COglHBJCHHtjdr0pvCPItQ0Du+3+XqamoFrNgEOwr/MNOu41PRTQrMwUWQN7InHEXpKrsZXl24XMluf8qL8E15xstLQKXml4N8LX1YfeUu754TxWgWYsAEuh5OPSzouaeXyJv4GfZEt1vWoclfpPTkGi28u5fDoOWafaC1jdSjwauowt8Oxwv9Vk7y0cHnZ+xVTHH5EPqOzvhLIpopr6ifnLfrplFsz3hMurKxFPDmIohet0LVslOdy3HvPs6LBUsOTG0PGAmCdwemsL85avFljOlncT+3QPVk3B0e2nsPBnxukW7+5VXRyinDYFoL5cHvXwrc4ErS8/TJPAPS54F05r7s2DbLlYV2FcQQD62HgLIRY9ukgABpAnWjwr9Wp4NW++qlnzPvdB2t7msN6MZygv8LBseryV+k9PZaLby7l8Og5ZpEEBa0s0vu3PMAQEk31QhUyKA2MySLYAsHVSwPGAmCdw+sJyl/zaTz5d6Evt3D1RPwtHtpbHzZMTrRu3uV14doZw2BMTP9auLKAc3H+vkF/I8vLP0Xvqk6UN+xTbysz1aIOLAJ5oERC1+p2kDBVDvmz6qv6X5yK10YZFtd612zki/24JBx9fhvzwMOB9qcr8Hun/d5rIZvpqVJN2T11mJF0gkY5uOD0VX8GdZUZ+iZPJ+5kSzlXf/ethtT/SI+tW+BYtu93nbQhC3NKI2bedn7Gvhp9slma/0XNhIxpDvXKlDu1UG6if3c7JZrgnmysdMoat21FZ3rexkEUB00vO/Vg05LNfygJkkcLuPINzEdym7l9Qzif27B6on4eh273SejJTcCsfydPcrr45QThsC4uc3UvKLNqW7lPqDdcIqfdLJeNRlwMOu7owWiDjwriirQHYT9hAvg5a5tMvdWlFI+tBq93I6qvBxSOD9i7f+g3Usq8DpmjXgfAgBxNXClwuL5gM1Y1vwZ9llYwROUWOXY92KZivv/i1tynQfR9yqqBbF/blnct20DJzWNvJU/s7j6zth9qm1bwN8QHesS+onT6W/w54hI9yvpA+7uXLEMz6srnQs/fuPiFrYyiKA3ErrfvToUU4ZlgfMJIHbfSwvpf83OZxIc//F5bVwWEyca8S8e6B6Eo4IIA8dtLtfNbpfKNaJQAo6Rt0ySntpDmxLn/RLpt+cEHkX0yRw593IY+UFF5a0yT/p/9R8GVzLMd1PWoz5ZbK27mP5o38Vufclqr5jRF6J5mUe3SLreXS7HHa2c3DmGPsm46j0M8qZWG5zWS/HjeYDljjBEX/eyt/NIrJ6mdilnBq7nJh3hLKVd/9coGcUMsiX+hk9yUryq6TSL8iHCXIHGF+znql7czY9vHrUMVSD69fiH3q29VCX8FfxUufLkc5ZacnwrRSuoqY+B9wuBBA3lK4Fmb94da19sMImCdx3D1RPwtHtpTHAZMTjTuzuVx6doIy2BAIFfU519AvLF11pgv53W2rFpQ97TyKAXLY5AshlRvspEEBsvHqlDjgfygrWWHkFeha6zWWtDE6lj+YDNYHoaKsKvGwUoZwauxxrfzRbefdvKZshgCxFuqyeAONr1jMVAaTMvl65EEB8SFoEkK1UuZjSZ3nATBK43bdo1iDk4wLjlzKJ/bsHxSbh6PbSGGAy4nFzdvcrj05QRlsC0V4wj/T2mcwJNhYKgfv0q/Rlt/2ApUvd0w40Jg67zVh3Iy/YgDTn0K8I9VrL12w7wkN9oRrw3m/ynoQAcnoAiOYDljjBYa8Czw8WHIHbVFVjl2MtimYr7/61scKHUlPA+lb+Zg0rLPdxNvlyvJW9AoyvF99Nki9tE4PHrVgsWK753XLBtj2oKrEfedVNLTrXFf4WAUSXCS3m7JYHzCSB233H+GykZXu1Hl2bfxL7dw9UT8IRAeThDdXdr2rvb/K3JzDAl/3mA/rSVzI/t6dnrsFtjDLXXJkhwAtabg90efsV86hcXMunE1/S/YvXFpA5BK3bYd3oX4qvht6KIeC9jwCy8G0bzQcscYJDVNGC6gubsml1NXY51rBotvLuX0tjCLsnUr4GTUOcSdayr2fKHuI9OMD4ena74T3xY7FY8EL+8mxXj/VDu4Xap1v96328ld/aPhQ6hvi1zptr3+8sAsii+6JaHjCTBG53Ri46iGipmzBiPZPYv/sDehKObsHFAJMRj9utu195dIIy2hIILBYUPxfThDHi2SZuY1Rbr/i4dGEa+WyVwwZrcFn3+B1mD+il7bl0fRN+PeiBcHdIup4LsvEosEUZAedDCCAtDH2mzGg+YIkTHHYrWlB9YVM2ra7GLscaFs1W3v1rYQxhdpPKfdGi/AHL3B2Qrk0PeQB2kPH1C/HvuxP3oQppsx64vetyk3lFyf2SYnK7rMoe8eMDyOr3uywBJL20LLqftuUBM0ngdmfWYYMjJTe4R55J7N89UD0JR7f7J8hkpPYW6e5XtR0gf3sCg9z75omp9EuD3+EmjZb5TXvr59eAAJLPipQfE0AAOeoVCCBlN4v5eZBTjfjooh/7nWmT21w2p985aaLNiWueo9GC6jn8R0lTY5cTgdet/P1iW7Bf4uzdv0v1lfw7AshH1BBA8hwJAcT5sO087Efny9d7f4sA8hDRYgKIGuF/pUYsyWd5wMhAfyV1/FVST8A8L6XvNwHbFbZJgwTvLvHrHqiehKPbS2O0l71LDnTi37v7VWG7ybYggR4fORR07+L+tIdlSr9CfrFkmd8UcGqWZcBnRPUkuRnMlRWcVmRtpdvhBMlApgj7vA44H0IAWdhxo/lAzXMUAaSd89TY5VirotnKu3/elhBeGynzO+9yJypPzwbRVSChtp0MMr4+Ey7qPw+uIG1bwgX1gxRdOd7VNwb72GwJuxyro+r9LncFyLXUHFYAUSqBvtqpdYSwL0C1HWuVf8CgzDEU3e0+CUcEkIfe1d2vWt33lOtLIOpqib1ent2f9sSLsx42HvEckCbBO1+POF7agHOtEC80S9gmah2IHybLhPwIKmAApMkYGmh8c5vLmrzvTOJoPlATiI6uHB+KAAAgAElEQVQWVPeyUYRyauxyYh63lb9nBUiGcRE/MiD9lyTcvDDI+PrReYuTxIayHUMSmt81LYVfSst8+RKhB/9eLILkCiC3Ut2PpibVJzZNbgNNWmt7/q1MHvSrVa5MApMMzt0D1ZNwdHtpDDIZybwLTibr7le1HSD/MgQGCQicXJ594sVZD46LeA6IaX6zjAfk1SJ+ol9GjXYIor7s3mgPmV/l2dkrlfiLctcDz1n5kQ81nAgScD7UZAwN9C7pNpfNd7vzKaP5QE2gfZD5jpfpFi2nxi4n5nFb+XsEkAtWRPwwu3koESTI+Pr+HGJpz6eJqM75PzfTHTuDeccBj+4m5nfMl000i+bLCCAmxoskbjKpX6TlnSqZJHDfPVA9CUe3l8Ygk5Hau6q7X9V2gPzLEBB/1yDl98vUVlzL0eXZ50oLurLlBwkSKO/hrglesru82AxnaIcG8yVbFcSil7qqGs9kDjgfavKuhABy2gmi+UBNoB0BpNVIcf+RQVZsKbcF0Wzl3b9cDhfmuRv5d7a9ssMMI4IEGl9fJow7AWT2g89Pec2Xcq8vuhVWtLHOfjt1y/GD1mx5r856SIlBeuyjbZrcBpq0Vlk/4oO1qkMLZJ4kcN89UD0JRwSQh/dcd79aYAigCgcCgSbf53pjDgp2mr9cssiw92X6ov/FpQ4G/3ddNn2z9MtNcCZuzUvCh5a3lR8rP8rJ3gcixE9vyovwyRnw+WB6R8ylEOhd0m0um9v3S+mi+UDN+zKBpkvWLv/3GrscqzWarbz7V076v5wTfJRSi6A2/70Ikp61iwa89xsebXythTpB/kWfwWL/qFs2j2TKL3Pv41wBRF9ill5+aJrcBpq01jjK+6VnNYWsLe8kgfvuAbFJOLo9sCaZjHT3q7WNR6P2d5D73/yMDDqpHHYVgvC8Eh//a1Q/P2g346OzIcU/9KvBu1Qs4ocPX/PKN59qP5QScD5kekfM5RHoXdJtLpvb90vpovlATSA6WlD9EvuR/r3GLsf6Gc1W3v0rsW26F3dZl96ivqTJ0fPoweh6PRH7/tOjsdHG1x4MAtbZZJ5x2M/0XqXiG3PmOif4XbPLPXx9qZhcAaTHns8mpws0ab3E/Ny/h5vw1nRmqbyDBO4u4egeiJmEo9s9NMlkpLtfXXJ8/j0GgfTVdsTzMg4BmZYlB+2X2zi1tPcggCxNfKz6EECa2AsB5GOspnfEXKsEepcM94yINieuCURHC6rn+ucI6Wrscqx/0Wzl3b8SmyKAlFA7mwcBxB3pFAU2mWcggDTzDXcB5N9mTT1dsMnp5GHQQ6TxxvJaHqxPvQudvbxJAvfdA9WTcHR7aYz2sld4H3f3q8J2k60DgUDBn3O9N5+fIf3SL7oifVnjNk51cBPdcmG3TcBoh6Efw6XbYemly8/1y6FtD6az1DnJXDyiOUzvRN4dCDgfasIj0DMw3DMimg/UBKKjBdW979ee5dXY5Vi7o9nKu39WWwkPjRP9Zs1H+iwC3cbdaONrFq35Ey3iD2L7jaDkDB8/f7r40VDuCpARBBB9aV16my4/U/1XEsHKAqKTBO67234Sjm4Pq0kmI939quCWJksnAgGFgmMkzB8KRHuBlk69lZfoq05mrq5WeN6LBXL9XF1YvAL03IXnYp8u2yDEw5HfIvGLXyT19/k5SGkgoPuUX/Xyy4DzIQQQg/N4JI3mAzWB6IBzAg8ThSijxi7HOhDNVt79sxhNWOi8ka1yLNDsabtsURttfLVjmzbH19qzFh9HpftZi59lW+EoTnDxHfuiAJKM08MwpsBdtAdkoQeY+lxYx3TZJgncd7f9JBwRQB7e4d39aroBZ+IODfIcLTkH5FbMFmqf5J4v0bUuvIJJuwabb8VGGtDnyiDAV6kZkOqTuM1vrE0JGJxBALEasTJ9NB+oeYYOMteptFif7DV2OdbiaLby7p/FSsJiht1OLF3ulbbJ8+VcZ6KNr73AB6xXP4pSAeTGu21p5YcWy+oPb7iPHp1dBZIjgFxLm/7n366LJZoCd9EekBd7dzzB4gNuYTtDZZskcG/y9xYGmISjW4BgkslId79q4auU2YaA+PwrKfmbNqW7lmo9B6TXPOZkp3u+RHtZYiB/Ke2y7gt90+LLr9IGRcwnfrA7+DzSNnMRUXm0ybwFoEelAedDTd6XpJ89djw4ZiK3uayH/bWMaD5Q8wydJGbgZVrXcmrscqwh0Wzl3b9c+NHuv9x2D5pO536LHoiOfcN7ymdy77utDN+bN2vHI86d9R64O7DKlfz/z8Nb6r8GvhF7PTnV1pkEkI10cnQFrcmEfhBHLW7mJIH77oHqSTi6vTROMhnp7lfFNzYZFycwkM+bgoBpovn34kDPV/iFTM4OJ5fBmni+OZM8M3KY/5oS6aoQtxegnIpHSLMCISySGXR10v1L3ZLjR8BnQ5P3JQSQ064ezQdqAtHRguqRBpjattTY5Vjd0Wzl3b8c3sJAx/w/ctKSxo3AolthRRtf3SjOU9DZFQXWboq9byTPC2u+hul1bqmr3jfn5pfpfXrXDD2PSPsR9QiKk+/ZOQJILwOZAneTDBzDB0Qa3pgni54kCGPy9xacJ+GIAPLQObr7VQtfpcw2BAZ6jr6Ul1Cdm2Rf0rdoWwc0CeBlA3FKGC044dStU8WwIuSAzCTzhsZu41786/SCqi+fi1wBnw1Nxk8EkNPuFM0HagLRK3tuLTJG7CqpscuxhkazlXf/cowTjUFOmydJ0+Q5c8LPb+XvQ23VO4kNvbphPn/yXMXBPhx6I229Lv3AK70HbKSMaKtDToqYOQJIrxvSFOCINjErudt6PFRL2hktzyQv4N0D1ZNwRAB5eIN296to4wXtORvguJZ/7bHlpdUsb+V5eWXJJOObTs4irRJd7MXKwsmadmUv5gggBw4yybzB6va90yOAPHrUZPxEADk7P+gVDzjaqJp35pU9txYdr2rscqyh0Wzl3b8c40RjkNPmSdI0ec6c8PNQ4+sk9vPsBgLICZoIIJ5u9uiRKZCJAOILf6TSJnkB7x6onoSjadw45+czjCnSv+5+NdJYsva2DjYGmFZMSt+ei31/DmTjxV6sWvZ5MJ/xQqFbYrEdlkAQ+9/JH9G++vKyc/RyvtYGSkBu27qhAedDTcZPBJDTnhTNB2oC0QSU240YNXZBAPmYgPjqjfxtpK1y2jlPvJLdYgqXuhZtfL3U3jX+u+fYFmiuUbX6Y+cH0h89C3Ajv0jniJ48ByTyChDToDPBS/jZw1rWONDk9nkC22tXuweqJ+FoGjfO+dgkk5HufpV7H5OuPwHx+StpxV/9W5LVgm9lMqqHtmddAce3Ke7NgFyz/KEyEQJIAogAUulJddkRQOr4fZQ7UFDCbS7rhSjanLgmGIUA4uUVH5dTY5djrYpmK+/+XbIEAsglQk3/fbFxONr42pTqoIV73vuB5hoIICcePBpg6KHmmAadCV7CTf0ddOxo0uwJbK9cugfDJuHodh9NMhnp7ldNbnoKbUYg0KTsUh/NBxQG69s092a0AMUlx3H699Vvh0VgxsmTyotRH9QVIFflReTlDDgfYgVInuncUkXzgZpg1EqfWW6+cK6gGrsggHxMgI8MFnHbc5U8S8/ZTcuWRBtfW/Z14LJd5h3B4m1fypitZ2S6XNK3XVmPXQqsL+Qz6d8/h8XkrADZSqYup7tbHqLBnKnEXG6B25LKR84zge0Vf/dg2CQc3e6jSSYj3f1q5LFljW0PJhKcM4H5Xk8TsyiTsmnuzcFWDnnf1roaRK/VbYk1UGDmndhH32X00j8PX/au0r89kT/1YPHRtvR6Ju9LG2/H3i8v4HzIJRBxyCzQ88/8fGtpfy07mg9YYgRH7KzjQJfYRms79S6/xi7H2h5NrPLu3yl7pQ8M9J9H2f5KvyTfPWd3z9rDwKM+Y/W6Tr9PevtrRv2LfGgQbXzN4LKfZGf73dzq7iC/bo+0b3v95xHHX5f3tkC2dp9npPdBta/6QoT7++huEdMIIEo60MTVOG7cJzd/zVpSyYx5JgncuwyqNfadhKPbYB7oAVVj1u5+VdN48i5PYKCgpn75fHEOs09Q+raR/x/lIPSp7s1JxsuaG24VK0L2gjLKKnpgRm2iwpTe99mX9FFf1PXMoChjxaW2v229CiTg/Y0AcskrnP89mg9Yn/8Hc4Ff5P/vAnKepHZl9gz86Lh359kpS1lil2tL+ktpVyyA7GwYXZB/KTb8xfoVudhVg+L6wcGt/KL3Ud206YcG0cbXS/dl+ne1vc6xzONNsr/Os/TXc7zM7Op9MpcYk/S91+5Kh339QWynz0L3K5A/H33Xvhg86PngsU5uBhdApgqGuN9JZwqcJHDf3f6TcHR5OKm7BRq8a26n7n5V03jyLk+g5zO/oLempbvSt0gHob+WOY6+/E1zCV/94ifKCpteXH9PFd+UvBT2anRuvUkg3SWPHLSofvalOZH2dfeCGNm31xacQQDJvWmd0kWbE1tjBE4YzhaT5k+apufXzdVj3xKscuuINiddwu/ShwbRPzDQr/51nlO1fc5eIPzHXJ/olK7phwbRxtczjNXuej31mOMm+99Ked93squlWhcfCDSmNZlHKdBk178tcBulPfqunSOA3EmDer3kWIMbutRuFBXx0M5TTVgaOfHRYicJ3He3/yQcEUAe3iXd/WrJsYC66gkEmpjldMYU9As2xrmNVTmglkgjfK+knt3L8KhzMU9UP0lh98FzeVH8aA9az4qWKCt4UEa3udLrOvGuCsrs80wvcvpXasuoq0KajicBgzNNXtwDfUjX1J4l40U0H1giEG3lhABiJXY5fbQ56RJ+lz406BV7u2QU/epf5zQ3lxJa/n3vgwP9Oj7q/LHJc0c5RRtfT9jupbfdd/UEn1++x+Fx/weaZ3zhIWKdus+DjN1H51I5Asi/lgHMOa1poAkCuhQBgcpCcsGCWoW94AyQUnAH+dxeGgeZjFzCxrhyiRD//oDAYM9R09aRgb5IUeZuY1UUF0YA+cgSCCDLOScCiPPWM/umCzgfMr0f5rphoMBEuOdDNB/wCETl+kVuOgSQXFL56aLNSZfwOwQQBJD8O2TRlAggMgDUEg80z0AAOaPeIIDUenpe/qOHtORlXXcqBBAf+0/C0e2lMdrLXqGVEUAKwa0122B+b77fA71Ymts+gk/ufcX3vxHau0Abd4F53SpCv2wc9ooWiNoDqYyv9f8LY7eVH8cMJQw28vdRV4GYVs1bHDHgcwEBxGJAh7TRfGCJQLQVGwKIldjl9NGeO639Lvi7eLMA+M4TpP96js5WfhFXgTTbujba+HrkzlzC9jep3sjbv1XPPaIIIAuMZbdiz95b272Tfup5Qw+uiypWZyOZgnfSVl2ePsIecsee+NU31OVpxJwpgk8WcqGbfD23UEu6STi6BRUHmIzkmLe7X+U0kjRxCIzm99YJXKCXabexKo73fGiJcNYXmcgvMT2w6fkgQ54Nklb3/NUD2oU678WP1sLHfhuERZQDLA/RNAtQBHwuNHln6vzOu2/PcM+HaD5gffYvMXYhgPhTDjRnu+9ca78LLLI3e74cek0SQf7w9yaXEpt8NR9tfD0gped+6Dxrka1cg7OonnsEmmc0+2hG/Uf6qedc/uZy11UUcmzMnk0AuRU+vZWmUhNV31ClFY+eb5LAffdA9SQc3V4agz+Ac2/b7n6V21DSxSAwoN+bJnCB+uc2VsXwnI9bgQhy1DIasL+VCfnuYO2o5nvQrsAfGC2+elpY6NdkutIk2h7tR79083CwQOPmrjtN3pkCBSbCPR+i+UDrQHTJfYMAUkLtfJ41CSDp2RLh4OBDoywaAE/B05vUiGgf0vzQYv4WbXw9cADTe5bHKCA8dI712KMs5zKq4yqB5hnPxJc3znzeFxclrjiiAGJSmwd/2W4ymW/l1JHKjXKDVTKpHlAr61el9lrKGH3bEreXxuCTkVxzd/er3IaSLgaBAf3eNIELNE9oFqyM4Un/tSIQ70hYtC1DrQYRO95Jm6MF/JttR3HJWQLPl5oIQgGfC03emQIFJtzmspd8Offfo/kAAshJy00171+ZAHIjVo0W8FdHWzwAvvNusX+0FZdvZOzRbbpcr2jj617nTLFYLyiB51jV42ugeUbTObT0U++T7h97yf2q8c0HV/QVIKYJYOCbJWc8aDKZz6l49DSD232Hv3pArbXjJBxNY8Y5ZoEnIxZTd/crS2NJ25/AgH5v8vFI41zEAE4LD0QEOUlVV4NowEO31Qh7Pkh6iYm2HYWyuxJui2zJcMyCwmUjfx/tPJAmwYqAz4Um70yBAhNuc1mvMT2aD0R8fqZgvSL/yot7QTmmOVFB+YtmWZkAEi3Yr7bu6k9i/ytpg64GiHQmyBdp3nbndTNEG1/3+vWFjLVu/bTwinbvp7ZX3w+B5hnapSZzKYude6Q9K4AEeOkxTQDTIBlxj+Ic267SAXPAXEoTKaB1qa1n/r16QK2o+z7rJBxNY8Y5ZoEnIxZTd/crS2NJ25/AgOOA+Z6PMvmMGMBp5YHCXPeC1WuT/oz0Mtuq25ZyfxV/eG7JsFRasV3E8/W6P9uEi26FdSe/SL7cZGVZwPlQk3emKM8G8Snzc631eBDNByI+PxFA/L0wWhC0pd8FGn92huz+oUGKT9zKn5G2uP9B2yW+4PZ1e7TxVbqn255pH91Xu+SOEkE/nqr+yCTYfb749na59m+Z7pIAci2Vd90Sx/qgCeZUFtt91vNLNktDo6UdMGB3DGGEl/nu97uDb7m9NAacjJTg6e5XJY0mTz8CA46nb+XZeWUhJn3UoGX37Xys8xtLH6OmTR/WaPP0S8fuNgjGSbfFehptLig2i7YXc4igjPpO0HmCuzgQsJ/ufUz2/DfImOA2l/XqTzQfiPj8RADx8rYP5axFAEkfiXQ/MPjAgiHeIfc+NtDmRfjgQOdqKg5o3MTlija+SqfcRR4rqGT3aGfiVD+bA8aqVyeCzCiAhAhsWG/yiBM5ax96pR8wYHcMVfdJxiQcqx9MO+MEnIyU3GLd/aqk0eTpR2DQccD0AUGUF+o1PvcRQM7e2wggeUMfAsh5Tu7iQMD5kHsfEUDOO1U0H4j4/EQAyRvALamizNd2bW7ldwggp70CAcRyx7ilRQA5jrI6zoQA4uajxQWFF0CkZ6ZJbrQHZa5lWj1Qc+sfOd2gAbtD5N0D1ZNwrH4wIYCMPBrQ9loCg44D1nnCrXDqvpx+zc/99EK7ETt8U+uzk+XXL7FuxDd01UX3S+x0JY2ItrVs9/nS3jxhFXyiBb+t74a5N1KgwITbXDa375fSRfOBiM9PBJBLXmT/92hxnVZ+J/2MuNWk6eMiu3XzcwgfnS/qFencLTc+0cZX4fylwu49FxUu0VYgVz+bpU96dl2ElUz7N2Cod4/8kaEs5YwCiA6QkQbHLMu0eqBmVT54okEDdofUu7/QT8Kx+sG0F9i4lf/uHiStvD27+1Vl+8m+MIFBx4Ef5BmavRev9FHPWvh5YbQfVcdz/34LIbWFjrXRXgZ6uoeucLju/eKpAMQ+N/LHi54wjtT9hbC5i9ImYbQ7wD6KmOc2Dwo8HzKJ3rm+ggBymlS0AF3E5ycCSO6dlp9uRQJIpEDvS7WQ3GP6/A9xiR/szqL4I0SD/mvEt8Jo9/yvalag8VXnn2p7PeOs+xVQGKyeX0Ub0/aMrLa/TfbPfqfu7iQFDRhBADEF8AINICZzRJzImTrQMfGgAbtDYiY/b4F7Eo7VD6Yd21HHkgPf6O5XLXyVMtsRGHQcMPl5lD7y3P/Pj9Mqg43851ftPHu4ku9FkF2re4khwV4+ux/KecyLkkik/xRGKPIeWwLOhxBAFh7SovmAt4974EQA8aD4sIxowcIWfid91GBzpLMOvk1BUJfgvqdXBFsRYHr3OMch0Pjqfr5Jjf0DfoRTHWeSPun7TvSP9d9KG3U1+rbGflHzziiAPBXY0Q6Rumj/Fg/Ui5VOkiBKMKsSp9tDtLQdk3CsfjDt+AWajJSaVPN196uaxpN3eQKDjgOm+z4F3Ltv68Nz/6NAh64G0etWfqwIefTo/ku8dHVZERIs2NB9T+pzI3Kg1QPazK/1f7xeXgPOhxBAFp4eRPOBiM9PBBB/p1yJAHIt5P7nT6+oxHdRvv4/1vpg45Dp3ePC/EHnvRF2nfg1zR128/EiJ/LKFPCdtNrm0qcQuxBk2kgFsY2MCZvM9EMkG0EAMTlawBslyxEiTuSyGh4g0ag2P0DXPVA9CUfTeDHIZKTmLuvuVzWNJ+/yBAYdB97KM/TKQitCsJLn/nGLpa8hdfl19C+kLC5Xm7bLtlgR7pM9cF+kF/O7Wpgt8gsr/Vo2yjZYrmJRsKCTmg8BpIUTnykzmg9EfH4igPg75UoEkFshFyH4rQZ8KffWjb8lfUoUf9CtsMJsg+U1DgUaX39K8yz1ye5XNHsLkOo4U8A+5dhZV4Toe9lml1h8X88yGfKaTgBRKwR7YctyDK8BNKuyyRINGrA7tEL3QPUkHKsfTDvDBJqM1Nyx3f2qpvHkXZ7AqOOA9Rka4ct2a5uX94Y+NSKAHOWOAPLoEQJI/i2JAJLP6n3KQO+PbnPZAgxHs0SbE0d8fiKAeHnbh3IQQPyZXigRAcSA3GscCjS+IoCct3/1sxkBxHCDNUo6ggCiS7jPtvOQjTjWnfzd542YNSnW2scmjRi00FEDdge4uweqJ+FY/WBCABl0IKDZLgQGHge+lOeoHiKZdUV4qea5f95U6SVBvzjibJD/UOkXWE+W+uoq2FhgXuWVNRA4JhJekbY1cD3ENlBwZmcxVoA4+m5OUdF8IOLzEwEkx5NsaSLM1fZb3MLvpI+RVg8+kz5ubFZaNrXw0i/Po2yT6vJhRqDxNdz5L4E+TFBHd4kzjRirPnKX6zxTt8faLjsC1Nc2hAAi3fzM8sIX7WGZY6YWD9ScemdIE+wlvRQpAkgpuYf5XB5MWmSgyUgNme5+VdN48i5PYODx1BQQk35qYP375Ql/qJHn/nn6CCAf8UEA6XnDXqgbAWRR45jG+9yWBQq0uM1lc/t+KV20OXHE5ycCyCUvsv97tJhOC79DALH5BQKIjZcxNQLIeWAuz2YEEKNXOicfRQAxTXQjBDasdmrxQLW2YdT0Awfs9pF3D1RPwtHlwYQAMupoQLtrCQw8DpjG0AjBHJ77ed6afPJWUrMS5NGjN+I3ugd280u430glL5pXlFfBS+m3tifslQS7KHuT68GVuoL+2gNYhPHyoB+m98JcBgggp0lF84GIz08EkNw7LT/dSgSQOyESZeeSL+Te0vaEvYIJRi6CQaDx9es0d9hGcYBAz2VF4hJnGjFWfcYf3p8PYlmw0NO/RhFAfhCg+rVm1hXsK6ysNkecyGU1PECigQN2+/RMwbsW2Cfh6PJgUr6BJiM15u7uVzWNJ+/yBAYeB0y+Lv18KnR/W57whxp57tvoJ9/UTJuUM0rAwNaR+tS/iu/odktNr2DPQNN7QFMwZwoP9qJu3kL4VNeC+YI2EwFkYSeP5gMRn58IIP5OuRIB5F9/ckUlvpP76tOinAtmCjYWuZyZEahPCCDnfdklzhTsgxnPu/d++1V9T5OxZOtZsGdZowgg1sDGtUD6nyeo1mVFnMi17rNX+QMH7PYRmHzci91+OZNwdHkwKZdAk5Eac3f3q5rGk3d5AgOPA6Z7P0I/ee7X+Xf62OVWSomyF3Rdh2y5mwSAD+YEkfYlb95fG/7jqaMF66SVpi2ETzEIOB9q4g+BBCzT88zDdy+VEc0HIj4/EUAueZH936ONqZ5+l4KgCiXMykHpn8bQQl8R5u97gFzO2wo0viKAnPd+t2dzkOdFy3t9tzLkVbRVZaMIICZnE4dS9frvlhZtULbpANcG9Q9bZLAHYSnH7oHqSTiaxopzxgo0GSn1Kc3X3a9qGk/e5QkMPA6YtgaSfl4J3b+WJ/yhRs8X6Z796Fl3mu/dShu6nufSgcE7qVN9WL/y1wNB3a9IgadR7hVhthFDfOdujPICXYSCgPMhl34dYkUAOe1o0Xwg4pgQJKA11bw/0nMoPW/Pxs4sQ3Wab2uWKB/tLrK61MLoWNoI8/e9drlsNxlofEUAOe+gnnGm61RVlPu/9tY8l/+N/ONuNycVRJq8t+R24JIAovsMR1Cl3wqo+xe93EsGkjtJO9L2CE0m87m8Rk43cMBuH3v3CeskHD0fTLdioB9Hvjek7d39anB+q2v+yOOANSDSO9hlbe/qnNHQ4fRCvElZ1nJWiMuXh6cw974/9to1xLYc2t5AQYwdvm9lnNGVPFVXwH41eWcK5PNuc9kqw+9ljuYDEZ+fCCBe3vahnMkFkN1Wlj/7kysqcZh3xkBjtQZ29UOUqrPZAo2vCCDnbx33Z3O0Ma5o5LBl0g+4dF76i9w3f9qy+qS+qGJHGWCsE50BnanJZN7HTWKXMnLAbo9s90nHJBzdHkyBJiM1N2B3v6ppPHmXJzCy3xfME/QLlG7bJ1nbu7w3jFljepZtpPUjfQRTA7vJ/DHK/F/AuD3XayDn5BVm3c8WOminyxwg4HMBn89xSMc00Xwg4vMTAcTR4VJR0eI5nn6X7intaZSP7ZqMq/5ecf+xgQZOH7cou6TMWr8INL4igJx3APf5qNg+yoKDEtevzXO/gkouPTNE39sWuYYRQISGaVAONJDkGtLUv9xC15BuksC9y0tqjb0n4ej2YBpwDDlm/u5+VeOT5F2ewOB+b3qOdn6xNm3ZtbwnjFsjAoiP7RBA7BwRQOzMCnOYxvrcOvD506SizQ1qA465PmFJhwBioZWXtvM87aNGevodAkieDxxLhQBSzu5CTgSQ84Dc4ky7ahBA7kkggJzwu2cWZSjgS8ilkarJZP5SpTP8+ySB++6B6kk4uj2Yor3sFd6r3f2qsN1k69vLUUEAACAASURBVERgcL83PUc7v1i7jVWdXCV8tWJf3V7iVn7dVvksBMnVl4SbnqOnV5Sz9Fz719ImAedRLnOAgM8F01ifa3MEkNOkovmAZyA61z8upUMAuUTI/u+d52kfNdjT7xBA7P6wFzTeyn+H2e601i8Cja8IIOfdssl8NJD9y2/K+px6aLpet5aYf0m1I60AMU3ixZGuBEjXA06NBmkymTe2YcjkAV84Szia/Lukgkt5JuH4UgbNm0t9zfn3SR5G3f0qhzVp4hAY3O9/kPt/d8jaRajSV03b6/DsJpPoi51eWYIUzL/taOeliLvNIdNcQNsd5WDGYZ5jAedRLofaBnwuuPn7/g2KAHJ6uIrmA7UBxxYDMwKIP9XJBZBtIhYlkP+Z3FddDyfO9SDxCz1D4Jvc9Auk+0LY3ZXWE2h8RQA5b8Rm727RxrpSX3bKp2frPJd7ajdGOhX7XzEjCSBmhws0kc0x2jAveDmdWTJNwBfOku53tz8cH5ot0GSkxJ92ebr7VU3jybs8gcH93uTvnftqntMs7w3z1Jg+itEObeQXJdjgCdjNnxBA6swS7N3DxS86j5XHDIIAUuem5tzRfAAB5KQJTfMgsyMsnCFaUNDT74IIZu8t6tm31m4SbTyS/lY9kwL1BwHkvPO6zKmOVZE+2NrKv4U526b1fZxR/mtJo0LIXUba7CQjCSBvpfNX2T2ThNEemhfaPtWExWKn2rQE7msJ/pcfjg85BpqM1BiYcaWG3grzBvyqymKFlzJPuMnN0PkebzaJzu3/WtOlZ91G+j/bIelVL+A7f0AAqbszEEDq+GXmdvH1w7oC2S7c86Hz8/Ijt4gYrA0S0J5q3h8tluPpd0H85f295dm3zHG8OFm08Ug6UvVMCtQfBJDzXtn02ZxEkDtpwuxb91ru/XeSWLc0fiRjlL67VV85AogaIcpLomlpnjhRz+0trMaZasJi7XxNegL3NfQ+5IXjQ46BJiM1BmZcqaG3wrzRXjaNJjBNTDuPeaa2GjmQPIPAJGP8fk9fy8vB04yun02CAFJHMFAQXTviMs4EvFeqgk2nLBzIdi52q/Pk2HPiiMHaIAHtqeb90eaknn4XxF/e3+ieffMce46VNdszKVB/EEDOO2/zZ3MSQbZ7zWBFyAcYuiLkRsaqqq36cgQQNUCU7QJME15xIH0R/K31IOxUvss+vU5tGa6YQC8tpey6T1g7BwNLuR3mc+MYaDJSw8aNR00jyDsOgWgvm0Zypolp5zGPZ77RuC2Siw9cpXI36c8o893S7n4hLwZ3pZk1X0AB5JnXV181XHLzBpuPmsbEU30MOB8yvQ8OaDsXu+X2OyddNB+IGKwNEtCeat4fbU7q6XdB/OX97e/Zt5wxpSZNtPFI+lL1TArUHwSQ8465yLM5iSC7lmzlPxBBPthFzwdREeTP0jFkNAHE9FBPL7ajHIS+yA1V6ijR8wV74SzB1T0YFujhW8Jvl+dbGRD1YLTqaxIepjGzGhoFDE8g2sumEeg7uf8/zc3TWQDh3sw1VMN0CCAfw0UAqXO4YPNRl3eLgPOhqmDTKQsHsp2L3eo8+WHuaD4QMVgbJKA91dwi2pzU0++C+AsCiM9AWfVMCjS+IoAggPjcEe1KWZ0AYl7eLwOKLpEZYR+1cJPddn7rX3Kgl5bSznW3vzAcacu4U5yrJiD7hQaajJT6lOab6kWoBgR58wiMPpZaX0479pd7M88lF00l/qD7zN4OMm88xuYHuQf0WV58BRRA3J7rxVAMGTuOKcda6TK3DDgfauITgWznYjeD215MGs0HrM/6ix10SBAkoD3V3AIBxMExM4uIeE+danq08UjaWfVMCtQfBJDz98v/t3cvZk7cXAPH81UQUkFMBZAK4rcCSAUsFQAVZFNBoIKYCgIVxFSQUEFMBSEV5DtnoyHG2OM5uh5p/n4eZ8nuXKSfNDedkdTk2jxIG93CM9GixXRekG1sL5DeeoB8kIxuFrGEhaTC6NvgjyzrNFr2veTtYaN9d79bbzdIEaBNTqjH6RzAULPzXezJ8LTMHN2MRFSnT6sM9SCUAsG6ywQcNQAtS/DJUtYHuIb55diMKuHyK0md0HuxnXx77HKefC9JACStjjU8p5xLeJZ7S4f3Q0mNTZdK2FHZZSm3tJr8+dre6oD1Wp/TYqb+7MPfWg6lONS9hbdn05z1zknA7FN1zpm30sebt/OR5DfpmuQoPwRA5itvs2tzuDfXZxMvc3OXPsyvbT86CLIkAHIre//xWgoq/t06Ebq39F+k6unCU7G8F+3K2w3SokR/vpBp6JaI7V9dxdGD39W0Xlog5zHk6GYk2kNWHOpBKAWCdZcJDHAeMAVBG+b3qZyv9EaWjzMBAiD/bEOR/OakaJIaFmrnoeE55VxWszysO7wfKlInHJVdlnLLWfe91YGc9/u5nJw0aA913+/t+T5nvXNSXz5V/5x5y3VMXdqOt/ORpDPpmuQoPwRA5itvs2szAZAvCmZVARDTCSZUFi8PcbOHVE8XntIXNuv2HV04rEk/Xv6+1IFDygZi1xW/x7Lur7Hre1kv5zE0SJ0a6kHISz0bNR2h4ff3zvNnvUfQc26Lt2lM6ey8TLpLvhwLOpeMDiX1pLvEf/WV6UWh0/w57AHSVbDQUSO6Fm2Wh3WH90NFzp+Oyi5LueU8d3mrAznv93M5OWnQHuq+nwBIrtp5fTsejykCINfLLfMSBEDmQZtem8Ozya0k8Vnmcu91c3dBEE28nL8WT4reYw8Q84Xd0Q3ttcplenP12sbW9HdvN+aR9tkm8LbuX/x2nTb0HGc160VpkDplPl9a6w7LjyPQ0wsDM+qmhrGGD9emdI5Ty/rKidSPG0nxL32l+qukgIHDAEhX1zFnzxxZ7osc3g8VOX86Krss5ZbzvOWtDnhsrCUAkrPG/buthvdoZzOTs945qS+f8pkzb/lrwudb9HY+ktQlXZMc5YcAyHzldXFtlvqyCcnU9ruWQy6WPtSXbF+DIPq5M5HzmM7/PftZEgDRCSF/vrahin+PmQhdI0I9jOWcdPKsWAbudjVIw91rOWhvauOGaPJB9vt17X1n3p/53DC3f0c3IylMXTUcpWSUddMFpM6P0BPM1Pjb8OGaFx7Sq2yVLYQgiO5Le4T0cJ1MupcgAJJWrRw1omtGsjysO7wfKvK85KjsspRbWk3+fG1vdcBjY62TBu2h7vsb3qOdPXxy1jsn9eVTPnPmLee559y2vJ2PJI1J1yRH+SEAMl95PV6bt5LkW/muPRDyTotOzmPqMftZEgDRjXgaQuqDZGxzLWPHf5eTij609tBVyNRwYzEYfdkQCf2z83w2mQek0zdczxV11pt+RzcjKdU6q0lKQljXv8Aa63yrh+ueHjT919w6KZS6opOj7+XrPQhivk8+uWeeHh683Pt3dR1z1IiuxZrlYd3htSGpsenSGcNR2WUpt5xnRm91wOM11EmDdlfny2t1tNU92qV05ax3TurLp6zmzNu1ck39u7fzkeQn6ZrkKD8EQOYrp7tr85Tc8PLSjfx/j0P3pp4Sjte/eg3sMQCiGbwvJ+nDUqmO3mq9WmBL87zG5Rw9uKTwVw+CiZseSy3GwE9xOrdu1iHEHN2MpDhxTknRW9m6a6zzkuc3UsyPahd1Tw+atW287o8ASLOS6eo65uxeNMvDusNrQ1Jj06Wa7KjsspRbziPWWx3weA110qDd1fnyWh0lAHJNKN/fPR5TM+fqW/nbj/lyn7ylpGuSo/MrAZD5quDu2jwllwDIp4K7eg1cEgDRN968TYpqauiUCqGTWf6VfGorv4FXcvHRIcf4RAh4u0mKyIKukvTmpnWfji641qSfW94UGL22w0Fsrl4Erjnw9/UItAoGZBY21flGx3mT3n6ZnVe5uV5eqElpyAgPUVq+9AAx1nKx28gqnnojZ3muaHSenNNPamy6tGECIJfJvdWBlHOc8bBevDgBkMVUixf09myfs945qS/HZfGN5O/q+PmLC6/ggg6fV5LaIBydXwmAzNdbtwGQKdnhPlT/90a+2q7svdd6iTPF7PF4NQCiKXJ0QzgBmW/oJQ89zAPi/qAqUUNzbVPKuJehzq5l2dSAd21jMw96HoObsdnJ3qDo6GYk1kTXq1KXUhLIun4EvD1oRsqY7g8aHedc6yML18NqUmf0ocL7xOjRD7FHD09eGvK7OV5C8MhL4CjbPUCj8+Tc4U4ApPLJ0FsdyNkQnYvSSYP2UPf93u5Lc9a70G6h1c/LMO1Fzqu5jq/j7YxWLxydX6PvHUuUs8N26G7uR6fyCM8sOsenfqqPeFCqXlzZ7mw59RoAeS8XIG28XfzppHG86tv/i/E6WbCXNzMXchadIFestFeUBgVHGPpKSbNfkBzdjCysMmcXG+pBKAWCda8LSJ3XN796f1PEdC5odJyb0ni95FiitoDDtw9PCZ7qL+ReeRdr4+jlp26OFwIgsbXNvF6Rhjrq/OVyaHStvJignA3R5tp3YQUCILkk/9vOaA3dx0LhmNJfeRnKqch5NX+tuHs529WLzannI0fnVwIg8xW2m/vRc9kILzjdhL/pz1HaAc9l9+L5bGkA5OAQyNRNr5fG8dQTaImLTC/bDAe1lzcWU9n+lg1spT7oBT7rJwQ/9rLRB1k33HZj2Rv6Hd2MpMhmd0lJDOv6FnDU+JMCZbo5bXScv5Zz+3QDmpLXouuKzfSiiQbMm37ES69Zbj7hOqr3xl4Dhj8plrjdxqI5Oh9k7+EZa3JtPTHT4QZ+vrZcxb8/TQmCTelsdJ6cYyrSUOeozpuuYzXqk7c64PF5mQBI/ppIACS/6cwWu3lmdHSu1vYavddKuk92dH4lADJ/yLm7NqecIUL7+I1sY8SeIRfLamkARB88v08BLrCu6eY3PKz2MA/IfTmJ6kM1nwgBKWe1GyWaqRfV5zkeXo8eYrfyb53012ujTUSp361iOh8s2Ymjm5Elyb20TDc3symZZN10gdDY7W2+r5iMmW5OGx3nXRyXThpz7uqA08auG0ma16GwXgc3TWPUx9Mblh7L/xxqo/PJXPlmuTcaNV+ncI4a1UzXsagD3LiStzrg8Zzg5JrZxf3F0uo3eABEn8n142XYRNMQskvLMPdyzl54fRfutaayjMquo/MrAZD5EnR3bY6qcCcrhWNKnxemZ4ZR2lLPjqjTcwDEfIH39DA3U1mzPKzkOBh63IaU8SjzgBzz68X1NuUN2BAAvJXteBlnNGv1KvEg5OhmJMXKfJ5M2Rnr9isg9V1v3r08hKVAmm5OGx3nXRyXYqPBcv00fzOoxDk+pZJN64aXLvR/vT0sJD+UO2t46uLeOBwzzY+Xo7qdZTjVRufJuUO0SH0gAHKZ3Fsd8HhNIACS46r6+TacXYeyvowR7rs1w17uvU33z/lLe9kWnT2vvNVUy/lommNhWSZOlnJ0fiUAMl+CXRwjUZXwaCWpj1qf9fskdVuN1z8b1F0aALmVxHsZn3ByNFdARyeXubrQRcNI48p8cffhgP3Va/oS0/Ve1t/Jdy8X2kVDYx2dwPQkNlqvj4nzbeqNx7ly6eR8ca1KcT65JsTf7wSkvnsbuiW2ZEz3Bo0epIo03sWCXVovnAP1z83v/zw2doXjRu+PXRidlGOOAIinF0peSB3Q9Lj+eHvRKtdx4/B+qMg5lADI5cPLWx3IVbdznlAIgOTU/HdbgwdApmGTvIxQ0sVwk87ORcnDjR7dSza/15a0EACZP42ZnjHznxHrbjG8QD0FQzy93LMU4oPcK2xOF+45AGKOwjdq6FhaQNNyXXQ/tGaq5vKOHmBKZluHx9IgiE5afBoM0XHb9abK27B1pTyKNIw4u8GKtSMAEiu3svUGqe9aamdvdi4VZ6P7giKNd7mrLAGQ66KejE5SSwDkevFlX4IASHbSSxsscg519PzgrpHF2z0CAZCLx9pQ9/0EQKqdU3VHBEDs3ARA7GamNRxdlzXd7q7NJkzjwmsPgGjkx+Nb9eYbYClIbTD2/Cb8qg4s43G4aHEpYx22o8co5aL8sdAXAvflQeiQ28Xbw15k/oZ6EIo0YLUFAt4eMhck+eIiloaRRgGQbySNei/i+uOscT/LUD65wcVoE7b5Z+5tJ24vRwDkVtLg4Y1EpXgtx8xNoknx1R09qH/QzJ578y0GweH9kPn5b0m+HZWfu2dBb3XAcp1fUvY5lqEHSA7Fz7fh7d60RL1z1jZV5Jk6Z81w1s7zNFxrdyl5dHR+pQfIfEG6uzan1DvLuuF55/g+XEeO8NymPmXvu3CMfnphfGkPkK2s6GV8wuOyMveWcHbSPFfvuoi+Ww6Y2stKGevB6XVi0toco+/vvdyMao+X7B9HNyMpeSMAkqK3onW9vbmcQm95QG0RALGkL8Uhdd1wLdXNeLieFmnwTDWa1herg/zb2zwg5p7Sxx4tjo2Z8jD17MpVrpbtOPNKDoCd1IVb+X8vwTBNWpHzAQGQyzVebDwNiZd0brMc15ZlCYBYtJYtu5IAyF40vIza8FTuUXfLSqfNUs4CRlkCBo7aHLLkJ2fNcHRd1mytNgByrkzDc6LeH7p7/jlK7xe9tJYGQDayEW9vt2m+zI2fnTSOd/F2aM6TW+5tObs45s4e2/tPoMjwV7p5RzcjKeVNACRFb0XrOrvBTJK3BBgaNFqa71uSMBJWDja6BQ8vwLh+KBcrbTBwN1mg5Vg4rSrhbS9P9/73NY0lenwmHCafVhUvT/MoZRmWY8qcw/shAiA5Kq1hG2toiDZwnF2UAEiq4Jfrr6HeOQsuuu5tKVb60uPv+Wta3BZT7rGO9+joGksAZL4qEAA58ZG6q8Pu6/2vp5dkjlP5xQtBiwIgugXHjSOmYIHDB7pzh1mRG/u4U3ufa3ltjOhT03WqTce/JSeObkYsyT5dlgBIit5K1vX2QJHKbnkgaRAA6ebmOdwvaXF4aAR3fS7zer2wHAvnjjtnL5O80DRKnlxOhi5WnoZf/SFYaZqSPw7rd5HnJEfPuu6uE2toiE49UAiApAp+uf4a6p2zl3Ndj0Ti7FqUrWeqo3wRACEAEnUiD20Ju7Dyg6iNlFlJ503WZwcN1Nx9LAGQgyzvsXvLD5Ih0w2+FJCOAeapYE6Lu9hb7WXqlb+tjtag50/YRYqKvqXi6GYkBdt1o2FKxlg3n4DUda/zfEVl0tLo2yAAYh66Mwoh40pOGgXdNQgeE3s9hizHwrkq46zx6X14iCky7GXqIePkOJmykbW3jMP7IQIgqRXWuL6zcwFDYF0uv6Hu+9dQ7xy2WWQNoBtPNbOLO2vDy9YO4egaSwBkvsK6fhbJeazFbEvq8RRk2Dtsa//00rQlAKIZ8TI+4XGZmE8+UjiuxjE9U8HMeYqppKOv4+2maXTvBvkr8gA85cPRzUgK7VAPQikQrHtZYJC6/imDlkbfBgGQ7o5JL9dSS7nWPt4b1KNFWUw1c3puuC/5OiwCqLBQCH7pnn6tsLslu8j+Bq/DelDk/s9TECv12F1SUSzLiI0ec25ehPTmo5bhWqn/bNle0t09xlw99HL/MaWxVL2TfH6UfXiZUPi15lfyemM5R5RcNgSJdBduhr+StGR7YdnRNZYAyHxFJgCy4EAPgZC9LOqpw8Gn+0ZLAGQnmXA3vrGkydz9zGGk/bQqdTNG+IJjoNkiUs564fYweWszg4F3XPwYcXQzklKMQz0IpUCw7mUBqeuehm5JLirLA2qDhmtzr9VkkMQNeGmAsJRrYpbNqzeoR4vSmGrm9DpIAGS+9AmALDo6vlyIAMjsfcJB/koAZKZuEQCJPPCum7YMKH2WutRr6qWsEgCZrzsEQPIfWxe2SABknpoAyIKqOFIA5Fby63Vyk+/kgqTDWi3+OLvQfJHuUhfYxUCDLOjtjaVBWD1k46kcI7uSCXHa8GPNMgEQq9gKl5e67n1YSFOpWK6fDRqui7y5bAIyLkwA5DpYg3p0PVGyhOVYOLdBp9dBAiDzpU8AZNHR8eVCBEAuw3l7nko9t0VWkdnVCIDkV/Vy/zHlrFS9c9YuRQ+QZVWZHiDLnJKW8nRdlowQAFlQmiMFQDyPEW4+AUnBaOOpxx4tU7XqrpFkwfFQfRF6gVQnr7FDc6+vmEQ5bfixZoUAiFVshcs7u7lMLgHLA2rthmtL2pIhMm3A0f2S53Gpt8L9WybybJtJrW+1j4+FGXd1XQs96DTpjxamv/Rib6Xc9Zkt28fh/VCRZyRn18JvtAClLHVonOYfZzbJwd0SoARA8quuKACyFz03PV1CSX4aMz9/ydq2GO5DdSVPbXfmF7Av5drRNZYeIPNVkwDIwkM39NryNGRd1BBYLh/uQhmYK6MUiueAjmbLHNRZWB9Xt5i3t5ZWVwD5M1y894cm2dHNSIqgq4ailIywbhkBpw2cSZm1NPpWzn/2N7OToBauLEbPZdGfFy5ecjF3byVOmfX6soXlWJh5MP+nZKFGbPtvWWfjoWFYyn0jafkzIg8lV8l+j+TwfmgNARA3DVGVr5OLjo0c57ZFOzIsRADEgLVw0RUFQLzcZx2XjItnyPA2+SEkzMM8KXoPokHYacLnhbX58mKOrrFurjtH99ee7kHNbc4nzwk3yZUlbQN7qbe3aZtYvrajF+g00Z/OZ4vnANG1vL39ccJvjlJLfjxNOHVam15LBW19kCyv4Y6X9Hjj7pjLe9KiLzzWjDm6GbEm3d3Na0oGWLesgKPG7WwZtTSMVL4+VDt/ZcP8997vofzw8BZP9ofOXE5erxeWY+GShbcGqJBOLw0z+iDpbXjg+1Luh1x1Ozz/ecvnGgIgr9ReylIbRpt+PJ7fcpzbcqMSAMkt+mlieTc9I0rVO0f3WceF6OJlA4fnn+wv4zjKIwGQ+dNY9HOckzKOTn/M2d1Zh4PoAIjngIF5YlFnUanTevVBLrKbmMrGOl8KOH2Ap6jsAkUees8lw8mFyi70+RouGolSM8H65QSknr+UrT8rt4f6W7Y8oFYOgHT7YoOzF2CehobBXf3adX6PDu8n3wWjbaqR0yBp84aZo7dSPbyROhXzezn/acAy68fh/VCRe0Fn57n34RjOXp7WyiEu7uYJs1znrfmNXZ4ASKzc5fW8Pb+XrHeS14NIfJtfMWmLTZ8jxWQjqdfzj6frbPZ7UEfXWAIg84dLdADBSxmXPIedo3N0XxUdANlLxtxE4U+QzQ0LzqJS5+qMuVdL0iVu4JWdXkAHFi+SNfMxnpIKLxeqlDzIuk1vXBPTzuoVBLw9XObIsuXmrnIApNvjUZzeSNl4mePgQ2gY1AdjFx+HDRc5AyDq7G2YJy337HNdWCqTw6CXJv9ViR4DDu+H1hAAmapjtnHmLfV7WjY8P7k7/i3X+Zh8x6zjJABS5BwQ45FjHW/3qCXrneMXkpqdg5zde05VOvv8TI6usQRA5k9cKQEQL9MvfBeeoTSwWPzj6BweHQDx/KZo1NjaUihD9WopXos73oHTtxg7Fq2a9Opvezq6GUmB7rbBNSXTrLtcwNGbGcsTfWVJywNq5QBIkUa7bHAzG3J6/Ww+V5q4TG9nexgi7LgEsw7R4PEN8JBZc+/vHMeL4xeoijRUObwfKnIudXo9rPryz+nx4TTQxyTol09k0Q10Oc6NubfhqPHsLmuW+0urRbif8HYvodnQ3mhbybu2mVX5iMVN2NEvVXa4fCdFXrxwdI0lADJfF6LPr5WfN+dy8SKcy7Rdv/jH0Tk8OgByK0rexro9LjjzDbHXG7uQKRovMx+Wjg7CzDkbfnPVGzkc3YykFC7nkBS9wdd1dDOWVdrygFrZoEjjZFa8CxsjAHIehgBIjdo3u4/q9waaGgIgzcvd/Ly3JMUEQL5U8vqcbLnOLyn7HMuEZ0zdVMvRMqIb6HIY5N6Gt+f2kvWOAMh/tYcASO4jafH2CIDMU0WfXys/b87lggDI4sPh3xv+rfz4zbJO5WXN3T4dP8QoXfRBVtm9m91Jed+TxB7k62ksyW78GiXUfFznSCcBkByKbMOzgNNG7WQyywNqzfsaS7qSETJvQJw2sklvw6Boz8CH4qrX9Caf0Dio+37SJAGXd/qT/klsbnOky/G5QuuAvplapSu/WoZGqr3D+8gi83+EPGs98vQCXKkAiLu5LsLxe7/2eS6c83X33s77dyQer6dOAiBRI2LkuE6U2MaaAiDhXKv3M97mAdGkVeuJ5jgQpA5P5dyzy13XHbU5EACZL9zottnQBvlX7roTsb2q84s5OodH9wDZeL0RCoX/QU5KmkbTRwrG7TBYHm/wTLgOF67Z4OUw+70lqXq32wnI0c1ISpnRAyRFb/B1vb7ZmcpuuW5WvB4Ua5xM9Vq6vlh5fDBveY3Q4a88DlehRfqD/keOBZ27JfnjNAA25esuCBLyWzQQEh5g97KvB8mo+TdQbEg4h/dDpQIgWrYt396/VCuiG11iq5mTxvyLybdc52MNrOs5Mqs6xrvVybK8o8azu2SXrncOz7XHxVU8COL4BQN1KBZcdFTuBEDmT1BJ12Jnbc5VXqyQPHt5seTTPfL/WS5Cuqxk4h/rOpWXNw8x4bwRqMhNfuUycbc7x28zurNqmKCmb/c6uhlJKQICICl6g6/rtEE7Wd3ygFoxAFL8wTEZ7soGxMrrPHDVgyDOG8K1JO+HxppDrnohedZgyqNc28u8Hb1f0M825Dt7ICQ0zKiBx7dzNdvfyLmvyBjtDu+HijwbOa/jVe7nwvOR1qefMx+jWTdnuc5n3fHMxhwFQLLOAVXL79x+VhgA2YiDy15XoXyK1K1wL6670Gus11E6io1I4egaSwBk/oSXGgDZy+a9vGRRrD5PhOFZyUOvF03Sp/vGmACIp4I7V0XNhSmF81g29GvLC/zMvqvc8DrNe9FkOQ98Fc17BxuvPqzFqYmjm5GU4uL8kaI38LrObkpySpt6glYMhnd/LIqV5wfzN+We2wAAIABJREFUuyCIVqRSjcAnN/R6L+yxF4Am03QMLD34nN8rn2Yj2/F21CB8Kzvx2jBTNMDq8H6oVABEy9jTUF+n9fqpnN92S49Z63JSzjeyjrdJh89mgwDIbOlOAeGmQ0Ra69+55dcWAFGDTton9J7rRo7DpJcNwrPIc+fn3alqFntj3tE1lgDI/IkrNQDi6R6j+IvGzu4pPnWSiAmAeH0DcKquUQ9+UkBeh8FKOtBy3HyMvI1ObjJGLoJLeSv6kLcE1NHNyJLkXlomWyNQSiJY159AZ42ZFkDTNbPicV6kwc4Ck2NZb40RJ3n6EP5fH8r3OfJ7uo0QBNI3FL0GPzTJxRrDJf+HIxOvPSE+PQ/IP26tDcahQUZfjNIHVf14z6emsVjDjG684nly6WFb5Hzq7GH9koX5Rb8lqA7LeDbZTgMg0znDSxDtrTjpuazbj7d7jhr1LvSG8Dzn7nF90h4hL62BkJPrbA/X2KLHkqPzLwGQ+bOl6RnzzDPEVn7n6dhOys+1C0t4ZnBxfB+fu2MCIBqldd0tVtIXMwyW58BOsW7t1yruGv5OEMRdKTcPfjh94I8pKAIgMWorWEfOe56veSklYLqZq/jQUbSBMgXMsm4nDYSaJX0o18bvgyV/55YND+r6p17eUix2DQ3lPzF18aa4JFbfctuHROvPS2+s6oOpzuvidZivS1W5aMOM0/uhUgEQz/P6HJe/vn2t5yPt8TbV7ahTXWhsvZWVvQzLsSgfNRqiFyXkaKFwP6G/8RIA0bQUCZhZbWKXX2MAJJxzp+O6l+NSz0nH5yL99+mQjHp+1c82fL32qDxXXYtcc6YdVXwWuXYoEgCZFzI9Y57blJS1t+kkirw05ah99Z2Wg9wz6Hnn7hMTANGVPUWuztUt88VeCsnzTe8PUmhZJrK8dtZb698dHaRrLYIp38Uabqywjm5GrEk/Xp4ASIrewOtK/fYyKVluZdPNaaXjvNjEibnxrm2PAIirhq1LxVXsOkoA5NoR0uTvBEAysTt/FjzOJQEQeTDOVOzZNkMAJBvlpw0RAOkmMEkAJKH6V3oWWZJCAiDzSqZnzHObIgCypBpmXSY9AKLJcVhwp0of5L5oY6Vz3CBkDuhY887y3Yy5OWpR6Ruaj1PfZMuJ4+hmJCVbBEBS9AZdN7zR7mVSstzKppvTSse5KU25QXJvL3Rp1s266NZ8JX/6UL6T716XWzpMg+RR7yG3el2Sb289Aqr0Gg71oIc6kPsQ8LY9c693awYqnSctySr2Nm4Hz7inTtM57s2SHm9H5zbdjvYi8Tyc38U64bQHyF2vHPl4GymjyMTVlgM2dtkVB0D0/kM/3l86ji3aXtb7ovG0RMIdXWMJgMwXcPLznJS1vtTu7blCrxHP5bp62msrqro7e7H8lWZC8jZdn+09QHQDnTz0mB8IJF9eh/eKCuhE1diVr+S4DoxcMjpuuwY/kiZSyw3k6GYkJWsEQFL0Bl1X6rY26v46aPZMN6eVjvOhjkOnb7laqvPdA+3Mp5chJ85loXhvgGmnHfUGstSNnpat1qhZ6TxpsS8ZANlLQno9B0yTX1+6n9bRDnoaduZinXAaANmGBHtttNbnLfO8SKeFEO4h735deoSKtQZAjq6zPZ+PLOd0r8veD/X8UDKBjq6xBEDmC9r0jHluU47vne+uD6G+76z1PVwXdHht/Xh6OeqH02tVVPdRp5Gr03Iy95pw/lbs/SVv9lgrK8t/KSD1QG9gNTo7xEOC8zLWhigNfmSJOOfMq6ObkZRsDdXwmgLBuv8JSN0edf4PzaSpAbjScT7UMJbhXkmtD1wn3Z1Zqta1Tl6IcldIiQmaGrnvxlOv8WxQ6TxpYSkZALmVhHiaw8HispplnQZANqEA/nReENM5RJ91NVh27QU0fS7W843+1M/0fFz8GYMAyF1vVO/1yXl1j07eaznP3ESvbVjR0TWWAMh8ueUIgNyTXXgfhWGaO0+vDfsZEj0/6XVBv56CHlOSzw5BHRsAuZWter85jBpz23Fw54WchKeomuGUyqIxAqGBR28Me30LLCbbtdcpfuOckiFHNyMp2XBtnJIx1o0XkLo96vwfimKq85Uerod8gWGQc2T8geRvzeq9hQfvTeavhP9N0U/6H3km0GexKh+Hx3rJAIg+yHt9g79KefewE48BkMlNjhd9qWsNL9GZ7rdi6lWle7TFSWtR7wZ/aWmxfeUFtQF4U+sFTUfXWAIg8xUtOQCim5fy3smPJ5Xr9Bp3dzaIGRsA6eXm0PwmnOOHOdNbrWus4SXyLPVBh0XTh8w13MiWIDy3TZdDXp0m1NHNSEq5FH84SUkc69YXcN7TMQeIqc5XeLiOehkjB0TpbQxyjizNVHP7BEBqarfbFwGQr74iANKu/rnYc4uG6KUZJwCyVOr6chXu0a4n4miJFvWOAIipiHItTAAkl2TidqT+/5O4iZyrEwDJqVl+W1kDID103VHSqK5rjrv0V5nYsnxd7GsPUh82kuKdfOkNkl50psbJ9N3Fb2GQxr1uvONLijUtAlKvb2T5XyzrdLasqc5XeLjOcrPstQwc3y95JSuZrqfSOKP3KlU/FY6hqvlxvrP3UsZ3Q1/V/Di8HyoWAFHXFTVg16xGWffVoiF6aQZWdE403W8t9Ttezptli3oXXlzSntseh5iJKdYe1ql6P+XoGksPkPnameWZTsp7K7uhp2n5M8HZERiieoCEm8NDJydic9DA0UnotFpUPRmXr5N97SH0DtJhyLgBsRfdW1nludw46nmji4/j84DFr/jDiSUxLNteYAXdbk11vsLDtSk97WuILQWOe83aMtL/0tV7f0xk4SURbZyhp2z5evSd3EddG68/eyoc3g+VDoDsBJHhKbLXpHwbbNEQvTT1YfSAn5cu3/Fyxe9vKtyjmfhb1TsaTE3FlLpw9VFXHF1jCYDM154sARDdRTi36T95wTr1iD2//sWySgmA9HJzaA4ahIc5jxNORfVoKVOn1rvV8Ab1rQgQCLleDXSS81u5YdxfX9TXEo5uRlJgij+cpCSOdesLrODNVlPDmHiUng/FPBRn/VoRv0cCIPF2mdckAJIZ1OnmCID8WzCm87y1LFfwooCVxN3yrRqil0AQAFmitGwZAiD/OhEAWVZfMi1FAMRRu43U/eGGwArH9NQ2RgAk04F7spkiARCdG6GHtxuiInVyrOkE2I/KlEf0VocdSzxapOGKBEJm8V/LX3c9Bj6mXBEAaXhwsesiAlKndeiU34ts3M9GTQ1jFW6szb1Q/VAuS4nT+6VliR9nKfPLPjmzvqIGv5xs1m01e6HB4f2Q6Txvhaax0SpWf3nnAZA13GtpoRc/JxEA+e/YEgsdheJZ/aNtVXusOveHwzYHeoDMV/eoduVzmwz3GfonhsIqc4q5eJ+Y0gOkp4v7fevQO45vfod+m7RM/S+71VBXNCDoLWBWNuNfbl1vGnbyfWk93mondMn+HD7wL0n26TLFH05iEsU6bQQGqdPX8EwNY4UDIM3eyr+GlPPvYqjzwh3kyxBIOWGXbyvbA9nyXX65JIGwFL2r6zYtY4fXDtN5/qrumQUkz3pOo6d3DF6FdTwHQDT7K6k/xZ8xCIB8fjBV6LVc4eh1vYvi15ZzuXd0jSUAMl89s9+LeTvHuT46lydudtSk6ABIuLh76pI0R/JKbpS0gdr0cXrzMlugpgyycFYBqS8b2eBj+d7I90HWjfvemM7vob09tNfUMB9HNyMppsUfTlISx7p1BVby4GR64aFwAGQ112tx1Gvfr3VrNHsLAk2GRTrVD4Gw/cruf2pUwveyk63cY32ssbNz+3B4P1S8kUryrPfyv7QyZ7/zAh0EQG4lBz8OXo7FnzG8NQ62rndHL5xMVYsXT/IdZC+kfLWXTfWPo2ssAZD50i8RANnILplHL99Rd7UXV2oARB90ehi3LOotTKdd+hkGK98BUmxLR8EQbRTq4RixWOiJRYMd+t23fCi3JNq6rKObEWvSj5cv/nCSkjjWrScQzkke57bKimB5OK1g0uxhKivqwo2JJ8MzLLTKtNgL3U6rB/ZzeZA6oL3D9dlAPzTMpBW03mvpR4Mf1Sc+P066w/uh4gEQzb/TF+HSatUga1uu9S2yXOH+okW2TvdZ/BmDAMiXxRyus9Mf9HrLtTbtaNBhu/Ve6iZtM/FrO7rGEgCZL8bsAZBwr9HL1BLxlbzemlfvD1MDILeSl17ebjAPHXUUZfd2YTHnpV6dY0+nAqEebfUhNnx76x2iD+F6g3X3bf0gXquGOboZScly8YeTlMSxbj2BtbzNamkUERM9J5cce9XFm/n1atldg+HUE3DtQ0KWZq8+SefSDIXjShcveWwtTU6vy+l9l56ftFGmafBD0+DwfujqA26Ogl/LdTOHVe1tWK71tdM27U/qz07+/aTV/ivst/gzBgGQ+VIMwZDR5/YrWZVd9NR2dI0lADJf24oEQMJ9lsf5p0see7m3vfilsNQASE9DHkQ9LDq9eXFxss5da9e0vdBAoG9K6ncjXy+9RD5IWg7y3ctXH7r/kIcM/f/VfRzdjKTYF384SUkc69YTCA3TozdKm3p7Fg6ArLK3Zgj4a8XWa0hvwf56B2TanpoPibQk+TQeL1E6u8xd8MND4GNKncP7oSoBEM1/6AWi/2Q+kOgqnX/FTgIgG8n5yD1viz9jEAC5fuxwrb1udGEJN+1pjq6xBEDmq1PJAIjOp8izk/1wNvfgSg2AaEH9ZU9nszXuWxtznXZhvTq2WTNhdhwtEBqOpoCI3jTr8aX/rx/9d2pj0hTc0O0dwlfHlNZAx0dPD9vRiBlXdHQzkpKr4g8nKYlj3ToC4dzS07U6FsZ0YyouJV/iiHrpIjbj3tYLdY4b+fwF00XwY8o2DTPmCuAu+KE5cHg/VDMAsg2luNYeTXcvFnirAz0EQMKxM80p8Mx8NvC/QvFnDAIgyypBeKFH3yD3NmrJsgzUX8pN8MPZNZYAyHxdND1nWqt16NGlz04cx8vwoo7jpABIOGC18TS1YXZZFtOXip0M3WOXpKdy87dLJ2ELvQscDTcxZUV7bTSbLLN3z6OGm1v5dy9D/F1iL/5wMkp5j5yPFTVCmm5MCzforGr+j9PjhwBIsTMKAZBitC42TABkWTEQAFnmlGMpAiAJinItJACS5qeNgV5GSdAhCZPbzhI4Lq5KAMSsGtVwat7LwhUKP48sTMXdYgRA5rVMz5kWeF2WAIhV7Kuo4zj5JB4u7L281RA1JEXhYTLMJR1WWPXbpbForIfAUgFHNyNLk3xuOQIgKXqDrCt1eSdZGXkc6qmkTPW98DG+uvk/Tg8XgiDZTyAa/Hhs7cmcPRXGDa4oAGuU+WJxl8GP8FB+Kz89vRBSLQAylVJnz7updfF4/Tvr0DDjZq4Brw3RF66D+uuDfEd7q9d0zxVTKekBslyNxtPFVlGNpou3HrFg4ecRS4oIgMxrFQ2AhPstHf1lf5SM0a4blvo4t2xUxwbdYI4ASMkhJHIBHW8nqueEtwtwyND93h6ESxQo20SghICjm5GU7BV/OElJHOvWEZC6rD3C1nADZarvBQNDUS9b1KkNdfdCECSbd1c9P05zzdupV+uB6/J1eD/UIgCytvG5f7prKPi//7udaq+ne4leAiBHdlv592jDqJnuua6eBc8s4K39xXu9E6+NMOrIJfrpZYSWmKoRu05UO2Dszpau5+gaSwBkvtCKB0B09yGYOaVEj2fmIPuvXPRloedyLt4tPb5Ol8sRAOltHpD3AjbNq7DYzekbbKseYmNx4bEgAhECjm5GIlL/aZXiDycpiWPd8gJSj3t7SSEFxfRgU/DBmh6aR6UYgiDTMCBr6ImUUoeP13139D/a86ProS3DA930wELDzL+F+0r/I2X7PFelKbEdh/dD1QMg6hoaF3XoZ/2M+lLB3XlH6uT2tC4VfGnAXG29N0Sfy5DD48jsfrJC8WeMgvdpUXnvod6Fey7Nn953rf2eS+c/1eegu4+U33T+jir/Uis5OjcQAJkv5CoBkOMkhONZgyBuhgIsdRws2K6+LHSTehwnB0A0oVIwPc0DokmOGppC8nmQdT1F4D5IBdgsqCwsggACRgFHNyPGlH+2ePGHk5TEsW55AanHHuewKpVxU6NYwQdrXk64UMJiPjX0/lyqEgyy3SHP3TTMfKqd2iijD3H7Huqrw/sh07k+p/HRm5ladqMFQe56IqnXuYCrp5cBe2iIPlfvPAWRMhwXxa9TBe/TorLfW70LL0HtBjxXLSm/t+E66/7lEUfXWAIg8zWregBkSo6jOrLk2Mu5jPb4uHuJ7rhHasoOcgVANFG9zAOiXq8F8MYK5+nG7yjtUcEca95ZHoG1CQxyoSn+cLK2etFTfkNj4189pTkxrabrofiUGhrMlI7EPHe5+lFvAHoCfF6CXTWMp1S+o4YZ3cxoDclzNNrr47anHj0O74eaBUCOGiOmcbpHqbvayLCZq5ee7il6a4g+PiEMFAQp/oxBACTlKvvvuuG4XVNvkO7uoxxdYwmAzB9yzQIg4VjW+w49ltfSG+R1uF8+pJ8J/9tCrgBIj0Ns3JebJzNmwQaT2HKNCubE7oz1EFiLgKObkRTy4g8nKYlj3bICToP2xTJtbRARn38KJIb5Pwyog5xnDTmeXbS7hvHUjB/1CLmVbfX0IpU169NwZjpuscshOOYy5PA4bR4AOWqM2Ae7ngMhi+egkbow1d+mwWvr9d56wJZeXhx7e3n0HEnxZwwCIPlqolhutTFRviM2nmoAVz8vc70lnk/++pYcXWMJgMwXV9MAyJS08ALRXX2Xr6fRia5X9utL6LGso1foi0KH64vbl8gSAAk3gSUaEuw5Wr5G1EXb0QlqyunVN3aWk7AkAggcXVz0JvHHzkWiznOd55nkBwG5Xq1p+CvtGrv4nqbgm6y8lGA4Ah3eUxlSn31RAiDZSd1skABI3qIgAJLXkwBIXs+rWyMAcpXobgECIMuclixFAGSJUptlHN0LEwCZrwIEQMofIl0FQPbi0VNEOSpwEBpNDpJXT2/6PJV2n135+sgeEFiPgKObkRR0AiApeh2vK/V3I8n/s+MsWJNuuikND4K/WXeyYHmuxwuQjhcJdVXfYnpkXHWExbtuGM9ZAKEeTPeyPT1PXGLQsr3VP8o9uj4jdftxeD/kIgCiBRrqrf5TXzho2isisoJpPX28dEi2UBd0V01fELK88BDpUny1o7d49bznqV1had6LP2MQAFlaFMuXC/e/uoJen3q/1k7zA0xzBLif7+NcSTm6xhIAmT+UTM+ay4/K+CXDdeRGttDzM5S+hHF3DOu91NL7kVi1xW9LXtuB4OvElr1NahnVUOHoJDUVywepKJtrZcTfEUBguYDD43x54v9bsvjDSUyiWKe8QKfX5BQY001pwQDI/VJddlNwPK9LAOSudLocGilnvSIAklMz77Yc3g8RAMlXxARA8lmatkQA5DoXAZDrRtYlCIBYxcov7+gaSwBkvrhNz5rla87dSxg6FcWNfAmALATPGQDRSVl+X7hfL4tFBQ6c9gJx8zDgpXBJBwIpAo5uRlKyQQAkRa/jdaX+HiT5o40LOlciprpe6Ph+L8EPvRfiEyEw2EP5NYG73gG99wy4lsnYv0td0ONIX6x6EruNRutNXfd1HPLu5vq4ZFbofJlSRC6feRw6XTM2XTd1Y+HY1H82feYfoQfIVDihXaHHSarN9edahTz9OwEQq5ht+aP7rpvOrrc60fmtfIu/LW4TjVva0bWDAMh8EboLgBxdRzby7ykYor/23CP1raRvr8evJrT2i4PZAiDhpki7nfXWhXOUXiCvpfLoxYsPAghkEHB0M5KSm+IPJymJY90yAqGBomnjRJmczW71hVwDp+6zV3df6Ph+JWnQRls+iQKhN8BtuJnv7b5yLvev5Y9DNY4nFvXs6qEe6AOdHldeA7r6IKcffZAbojHmtFAKnS9Tqo7LAIhmKFx/9VrkdWgZDb7eXadSgnSSz6bP/CMFQKYD4ei610vgt/gzBgGQlNOkbd0QiNPrrX714+mNcg14TNfZXcq5y6ZSZ2lH11gCIPNF7jYAcua+7Z78bhu+08t5+rPmM5Xebxzkqy8E3b0U5OHFr9wBkJ3kq5eL9lRPot7YdNoLhKE36lyn2MsKBBzdjKRoF384SUkc65YRkLrb47U4FcPUIFboodqUhtQMr2H9kwdyTw/jS/mHbxxfCpG63NGb5/pApw00rRqYdazi/fSVh7kuxxy3lIfD+yH359rwZvVtcG5VV4+L+W6Mbamven+Q/JH83chGNskbityA5GOyjdyC39WOAr9TItXa09u8U2O0Dt149wZvqU/renaar5Hr3Wlej+6/tvI3bTitVQe1J+XUg1Lr1360gMcZazXWb+vPThNQ+438uUyH+4/WLtP+D7muoa0yFI5r3b0e0xokOR65YPq3/v74o8fj3L2u/m06ZnW9PzzfG+cOgOgDya+tCjRhv1E30g4fCGjsTKgErIrAsYDD4zumgDgnxKh1vI7T4HwNUdMLAAUCIH/Lzd7pDWONfK9mH0c37dPbiVvJvH699AzQRqF9KBD9OWRvAE8VLjQy6wPbJqRrenjT/4+pF8cNL1NZTg99+kA3/c4TQ/G0OLwfinpuKw51YQcheHcjf9ZvrbcvtS7rRxsQ9Y3pVdbdVmWee78hKKLXOz3HHZ/zYs5zc8nTN3aPP1O90Z/TG7zDB31zl98I2wv3YMdvkk9vmGv2rG+Wa0BW69EhfKfGU21gPm5IHYGOPCCAgCOB3AEQPRH+5Sh/S5MS1Z3JYUOT3uxuPEfclhYIyyHQWsDhA38MCQGQGLWO1wlvy/3ScRaikm4dDkOc/ona0eWV3koapob5zJtmc3MCoWFoahTSRbd6LyTf3A1DUzI00HGQ7z781N/rG4r6Oz4OBY56j+hzykcaWBwW0kqSFOqiXiv0PKWfqUExNTCiDddTw6GejzTwwWclAqFenb6EoY3JXJdWUgfIJgIIIIDAdYGsARDdXYG3Kq/nIs8SUW8TOWwkfdp716w8xclWEEgTcHhsx2SIAEiMWsfrdHwNTlE3D2VZIADCtTelBBPWJQCSgLeSVQmArKSgO8gmAZAOCqnDJBIA6bDQSDICCCCAQHWBEgGQ55KLn6vnJH2HUb1AQtDnID9LvWlozdkHCYBsrCuxPAIIfC5AAIQa0ZtAeABe2+TnWkym63cYNue3zOX7Db0vM4tm2FwIjhzfE+nb1pahyvSN6mm4D3oOZCgTNoEAAl8KHJ2r9Hx17TlOnzv1qx/e8qdCIYAAAggggAACCwRKBED0pu3PBfv2uEhsL5AbyYynIUd4E9Vj7SJNXQkQAOmquEisCEid3cmPJyvEMPV0Eqfc85WZAjArLB+yjAACCCCAAAIIIIAAAggggEAzgewBEM2JNC7oG3MPmuUqfsfRjRiS54Ps1ksvEPNwIPFkrInAmAIEQMYs11Fz5XBOqprUpqB/gWP7hfT+eFkzw+wLAQQQQAABBBBAAAEEEEAAAQSWCZQKgGhDwLNlSXC3VGwvkK3kJPeQGik4UflI2SHrIjCSQIFG0hY8pjfjWySQfeYRWOvk50HPdL0r0FPmPhON5qnHbAUBBBBAAAEEEEAAAQQQQACB3AKlAiA6xnKv45Cn9ALZS76/z11IkduLzkfk/lgNgaEECIAMVZzDZ8ZZL8Sq3hJ8MN3LZJ4onh6XVUubnSGAAAIIIIAAAggggAACCCBgEzA1Glg23XljjOlt0sml0MSqFvbTZaPykbJD1kVgFAECIKOU5Pj5cHjtqYn+QeIfG8sOxesfy/JXlmX4q4yYbAoBBBBAAAEEEEAAAQQQQACB3AIlAyA9D4MV3XuiwNAaKWUenY+UnbIuAiMIEAAZoRTXkYfMPRp6QzNd58JcKX9lzOR3EoDRec/4IIAAAggggAACCCCAAAIIIICAQ4GSAZCeh8HSoorqPSGNKxtZVxtDvnZS3lH5cJJ2koFAMwECIM3o2bFBIFxz/jSsMtqipnluMveWMfc+GQ2f/CCAAAIIIIAAAggggAACCCDgXaBYAEQz3vkwWNHjejtrODW9Heu9wpI+BGoJODuOY7NtahyO3QnrtRNw1uuwBcQP0gPjzdIdi9dzWfbnpctfWe6V7Fu3xwcBBBBAAAEEEEAAAQQQQAABBJwKlA6A9DwMlhbZU2nc2FnLLgyxob1AvrWuW2h5eoEUgmWz4woQABm3bEfJGb0/7krSNASVmOW8L7kv9wiHUeoT+UAAAQQQQAABBBBAAAEEEEBgRIHSAZDeh8GKHt5CGlkeS4X51UmloReIk4IgGf0IEADpp6zWmtJB6mhS8UkAwnQfk3G+lOheokkZZmUEEEAAAQQQQAABBBBAAAEEEDAJmBoOTFsOC3c+DJbm4oW0r+gbo+ZPxoYW877PrEAvkByKbGM1AoM0LjME1qA1NvQ0PEj2vMw31ULaHNwXt38yJTT63iDT/tkMAggggAACCCCAAAIIIIAAAggsEKgRAMk53MSCLGVf5G/Z4kaCIB+tW3Y2PEl0bxZrvlkegREEBgmAvJOy2I9QHkvyIOfp2yXLjbDMIPUztShMc3BkviYz/FVq6bE+AggggAACCCCAAAIIIIAAAhUEagRAeh8GS4sh+i1qZ41UUXOaVKiH7AIBdwLOjl13Ph4TZB0OyWMelqSJ3h+flEzXtIxDUzL81ZKKyjIIIIAAAggggAACCCCAAAIIOBAoHgDRPEqjg04I/sBBflOSEPW2p7MJ0T8IwMOY3iwpcKyLQI8CBED6K7UVBUBupXR+7K+EsqfYOgF6LjdT4CV7rtkgAggggAACCCCAAAIIIIAAAggsFqgVAHkuKfp5cap8LvhWGtd0YnPzJ+Nbp+Z9n1khujdLjp2zDQR6ESAA0ktJ/Ze2tM3WAAAgAElEQVTOFQVAdEjGNc/9cVfo1vKWY/qNrPYoQ83+hhcJMiiyCQQQQAABBBBAAAEEEEAAAQQqCNQKgGwkL39WyE/pXURPJJ6x4SU1j9FzmqTumPUR6EmAAEhPpfVvWq0N4v3l8K5H5Y2k+5ce0545zTEToB8kDd8mpiP6ZYjE/bI6AggggAACCCCAAAIIIIAAAghECFQJgGi6HAUAIpg+rRI97neYfFWHAvPw1u5raSjURjQ+CCBwQYAASH9VY/QACHN/fFYnTb0Zg91fGWr1D1LPtCcJHwQQQAABBBBAAAEEEEAAAQQQ6ECgZgBEG9xHeGv1hTR+vIwpW2mA8TQUmGns9Jj8sg4CPQsQAOmv9FYQALmVUmHuj3+rpikQkWkoyr+ljt3r78ggxQgggAACCCCAAAIIIIAAAgisV6BaAESJpQFihHHLk4aQEoO9UHzvoMqZhw9xkGaSgEA1AQIg1aiz7WjkAAi9P76oJqZ5ODIdz6+kjumLDHwQQAABBBBAAAEEEEAAAQQQQKATgdoBkJ24POnEZi6Z0UNIhaGwvMyH8lQac7RM+CCAwIlApgZTXCsKDB4AuRVKen/8W58+SFlvLFUr08sH9Jy0oLMsAggggAACCCCAAAIIIIAAAg4EagdAHkqef3eQ7xxJSJkQ3UtDVlJvlhyIbAMBrwIEQLyWzOV0jRoAoffHF2VufgkhQw/U6DnA+juSSDECCCCAAAIIIIAAAggggAAC4whUDYAomzRC6ETgDwYgTGoMceTAkB4DVEaykF+AAEh+09JbHDgAcit29P74rwKZei/KsZzj5QvTPkvXdbaPAAIIIIAAAggggAACCCCAAALLBFoEQDxNBL5M6fJSKROi52iQSU3/tD7DeuSSZDvDCBAA6a8oRwyAOBs20UuluC9lfViaGDG8kWV/Wbr8meXoLZmAx6oIIIAAAggggAACCCCAAAIItBRoEQC5Jxn+q2WmM+5bG0UeWhpijvftqIE1qTdLRk82hYAbAUfHpxsT7wkZNACyE/cR5s7KVX1i5v9INTQPuZUrs2wHAQQQQAABBBBAAAEEEEAAAQTSBKoHQDS50rCY2hiRluu8a7+VRrfHsZvMNDFr7O6P14vuzZJj52wDAW8CBEC8lcj19IwWAJE6uJVc/3Y956tawhyMyDDkJL0kV1XFyCwCCCCAAAIIIIAAAggggMBIAq0CIKM16vwgDW9vYipGGN5E50X5Omb9jOsk9WbJmA42hYALAQIgLorBlIgBAyB7AfjehDD+wqa5OMIE8im9TukhOX6dIocIIIAAAggggAACCCCAAAIDCzQJgKhnhjcyPRVL0vjgGcYnz2XxThoQt7k2xnYQ6FmAAEh/pTdSAETqn/Ys/LW/UiieYuv8H6mOpoBL8dyzAwQQQAABBBBAAAEEEEAAAQQQMAm0DIDcSEpTJiU1ZbTCwqlDYWkPkkcV0nltF9G9Wa5tmL8j0JMAAZCeSuvftI4SAAm9FrRn4Lf9lULRFMfM//FSUvQsMlVJLzdE7pPVEEAAAQQQQAABBBBAAAEEEEAgo0DLAIhOhn6Qb+uhnzJyfvU/aYDbx2zQUYMXDT4xBcg6wwkQAOmvSAcKgNyK/o/9lUDxFNee/+OV1KnnxXPFDhBAAAEEEEAAAQQQQAABBBBAoJhAswCI5kgaGFPezCyGkrDhD7LuQ2kw+RizDUcT3ib1ZonJO+sg4E2AAIi3ErmenhECII7mhboOXn8J03BUGeb/MA23VZ+DPSKAAAIIIIAAAggggAACCCCAwDWB1gGQjSTwz2uJ7Ozv5jdUj/PnqNHV1NDUWRmRXASuCjg6Fq+mlQX+FRgkAOJlOESP1eobywsGifOo8CKAxxpAmhBAAAEEEEAAAQQQQAABBBAwCjQNgGhapYFixMaepHk0xGQvNN8byzL34joUlvZmOeTeMNtDoAcBAiA9lNLnaew9AOKoF6DHwn8v5fvQkjDx3MnyTyzrHC0bPaRl5P5YDQEEEEAAAQQQQAABBBBAAAEECgh4CIBsJV+/Fchby00mzaMRhu04SAZaz4/yThqctHz4ILA6AQIg/RX5AAEQPe8z8fn5qveTlO+tpVbKMRzraZ5s3ZIulkUAAQQQQAABBBBAAAEEEEAAgXoCzQMgmtWERop6UvY9JQ2f4ehN4BfS6KRztfBBYFUCBED6K+6eAyDUt6v17Tsp3z+uLhUWCHOpxA6xyRCQS6FZDgEEEEAAAQQQQAABBBBAAAHnAl4CIDfi9Itzq5jkJTWiOGoQMzU8xUCxDgLeBBwdf95o3Kan1wBIYmO92/LImLC/pWzvWbYnps9l+Z8t64RlzfuK2AerIIAAAggggAACCCCAAAIIIIBAJQEXARDN66C9QJLn0XAyH8h7KaKtZfLZSvWX3SBQTIAASDHaYhvuOACyF5TW8z4VK5cMG34tZXtj2U7C/GLmobYs6WJZBBBAAAEEEEAAAQQQQAABBBCoK+ApAHIrWf+xbvar7M08cetxqsJ8IDrsR+tx4c0NUFV02QkChQQIgBSCLbjZHgMgCT0VCkq62/QPUrZvLKkS14+yvHUeraT5uyzpY1kEEEAAAQQQQAABBBBAAAEEEKgj4CkAosNbHCIaLOpIpe0l6Y1Sach5KLv/PS0JWdZOGtIrSwrYCAKVBAiAVILOuJveAiBh6CsNcFsb6jOqdbGpbyw9EMX1seTq14icEeiPQGMVBBBAAAEEEEAAAQQQQAABBDwLuAmAKJI0Wuhk2888gyWk7X/SgLOPXV9sbmTd1vOk6NuxOhTW4oloY/PLegi0FiAA0roE7PvvMACivRoe2XO6qjXeSrlqQGPxJ+Fe4r7s67B4RyyIAAIIIIAAAggggAACCCCAAALuBbwFQDYi9qd7tbgEJg+tIY06O9n1k7jdZ1uL+UCyUbIhzwIEQDyXzvm09RQASeil0F/BpKXY3PMwck4xen+klRNrI4AAAggggAACCCCAAAIIIOBSwFUARIWcNPKXKqx30kC3Tdm4k0nRaShKKUTW7UKAAEgXxfRZInsJgIS5nQ6SeIa+ul7NTL0yEoaMNO3nerJZAgEEEEAAAQQQQAABBBBAAAEEPAh4DIBsBGbUXiBa5qnzgehcKR4mRTe/leuhwpMGBJYKEABZKuVnuY4CIHtR+96PnNuUvJcy1TmwFn8iJ5U3D7O1OEEsiAACCCCAAAIIIIAAAggggAACTQXcBUBUY/BeIJrF1PlAtEFIG9Bavz38HfOBND1+2XlBAQIgBXELbbqHAEhkA30hMfebfSFlqnODLf6Ir74g8GDxCv8umHRNNu6LxRFAAAEEEEAAAQQQQAABBBBAoKKA1wDIRgxG7gWSYz4QnRT214p15dyukvPROP3sHoGLAgRA+qsc3gMgUqf02qYN9K2D170UrmlYquBrvXdIHpqyF0zSiQACCCCAAAIIIIAAAggggMAaBVwGQLQgpCHjjfx4NHChmIf2OLUQoxv53S+NjZLz0Tj97B6BswIEQPqrGB0EQGJ6J/RXEHlSbL62RPauofdHnvJiKwgggAACCCCAAAIIIIAAAgi4FPAcANmK2G8u1fIl6pU02D1P2ZyT4cKYFD2lEFnXpQABEJfFMpsozwEQ6pO5PtUY/oreH+ZiYQUEEEAAAQQQQAABBBBAAAEE+hJwGwBRRmkw2suP0SeKTZ5M3EkQJDkffR06pHZ0ARqs+ythrwEQqUtb0Rw9oJ+7wtQY/oreH7lLje0hgAACCCCAAAIIIIAAAggg4EzAewBkDY1GOo/GNmUycWlcuyfb0GCRdeLX3NWRxqTcomyvmQABkGb00Tv2GAAJ52cd+urb6Iytb8W3UpY6z9XiT8TwV/T+WKzLgggggAACCCCAAAIIIIAAAgj0K+A6AKKsK5gLRLP5Qb4PpcHnY2xVchIESQ7mxOaf9RDILUAAJLdo+e05DYCMPp9ViYI19yiU49U6vwoB+xIlxzYRQAABBBBAAAEEEEAAAQQQcCbQQwBkI2Z/OnMrkRzzhK+niZAGILXSRqCvSyRw4TaTgzkL98NiCBQVIABSlLfIxr0FQCJ6JRRx6XCj31heCAjXPst9Ar0/OqwUJBkBBBBAAAEEEEAAAQQQQACBGAH3ARDNlJM5LmJ8reskTyYuVg9lp3v5tgyCvJf967Be0T1arHAsj0BuAQIguUXLb89TACSci38vn+vh9mC+DkYEmkzziwwnTIYQQAABBBBAAAEEEEAAAQQQWJFALwGQjZSJ5e3OnovwhTTivUzJgJMgiLkRKyXPrItAbgECILlFy2/PSwCEeT+SyvoHKUcdNmzxxzj8FdemxbIsiAACCCCAAAIIIIAAAggggED/Al0EQJR5Rb1ANLvmBqDTqiheOoHsr42rKA1NjQuA3ccLEACJt2u1pqMACPN+xFWCD1KGG8uqET1t6P1hAWZZBBBAAAEEEEAAAQQQQAABBDoX6CkAoo0ia+kFkmUycWkYuhGzXxrX0Z+kQeu2cRrYPQJmATl+trKSfvl0IuDhXEPgLKmyvJIyfG7Zgnhrj8lnC9chKL8QisUQQAABBBBAAAEEEEAAAQQQGEWgmwCIgq+sYUmDIA+lMeiQUtmcBEGeSj52KflgXQQQQMC7QAia/eY9nY7TZ+6dIeZ6jfx2QZ70mrphbqoFUiyCAAIIIIAAAggggAACCCCAwEACvQVA7om9Nna0nOC7ZvFnmUycIEjNImNfCCCwRgE5z24k33+s6PqUu5jfSXBia9mocahHeiNacFkWAQQQQAABBBBAAAEEEEAAgUEEugqAqPnKeoFolt9Lo9DD1PrmJAiSPLdJqgPrI4AAArkFwqTne9nug9zbXtH2zD0FDXOD0ftjRRWJrCKAAAIIIIAAAggggAACCCBwLNBdACQEQQ7yc8mQF6OUdpZxyx0EQbLMbTJKoZIPBBAYQ0DOrUx6nlaU5gBFCDr9tXC35uDKwu2yGAIIIIAAAggggAACCCCAAAIIOBfoNQByI66tJ/euXbQEQWqLsz8EEEDgioBxEm48zwuYr2/irpOl/7wA9IP0otwsWI5FEEAAAQQQQAABBBBAAAEEEEBgQIEuAyBaDtL4sZcf3w9YJnNZeiENOS9T80xPkFRB1kcAAQTurkM34rC2YHyJoi85+fn/5Lqp9wt8EEAAAQQQQAABBBBAAAEEEEBghQI9B0C2Ul6/rbDMsgzl4aDhjuGwVlh5yTICowjIOVTnZvp9lPw0zEfM5OdLr//mbTd0YNcIIIAAAggggAACCCCAAAIIIFBAoNsAiFoYJkAtQNd0kwRBmvKzcwQQWLNACH7sxeDrNTtkyrv5ema49pt7lmTKE5tBAAEEEEAAAQQQQAABBBBAAAEnAr0HQDbi+MdKG6HMjUbn6hw9QZwciSQDAQS6EAiTb2vw40EXCfadSPP8HIbJz1/J0Fc6TwgfBBBAAAEEEEAAAQQQQAABBBBYsUDXARAtN2kMuZUfP660DLOMbR6CIDq3SKu3mRkOa6UVmGwj0JMAwY/spfWTBCn0Gr74s/Car9eUjWz74+INsyACCCCAAAIIIIAAAggggAACCAwp0H0AREtFGkQO8uPbIUtoPlPZAgcOhnTJlpcV1gOyjAACFQQMQy9VSM0Qu/jGGqRYeL3P0kNyCGEygQACCCCAAAIIIIAAAggggMDKBUYJgDyWcvx1pWWZLXDgJAjyXBrEdistS7KNAAJOBQh+ZC+Y13Kuv7FsdeGQjUx8bkFlWQQQQAABBBBAAAEEEEAAAQQGFxgiAKJlJA0je/nx/eDldSl7uYMgGoBoOb49b++utCKTbQQ8Ciwcdslj0j2nyTxB+cLr/HcSWNG5wfgggAACCCCAAAIIIIAAAggggAACX40UANlIef654jLNGQS5J457giArrk1kHQEE7gQW9jpAyyZg7qUReij+fmU3THxuKweWRgABBBBAAAEEEEAAAQQQQGB4gWECIKGh6lZ+rnVCdCXIHQTZyTYfNTwKzBPkNkwru0YAgcEECH4UK9D/SS+NvWXrC4YgY+JzCyjLIoAAAggggAACCCCAAAIIILASgaECICEIcpCfa5wQfaqy2YIgwVODIE8aHg/mceIbppVdI4DAIAIEP4oV5HsJfjy0bF3KYiPLX+vh+YNs941luyyLAAIIIIAAAggggAACCCCAAALjC4wYANlKsf02ftHN5lCDINkmE3fQEPhW8nMjjVsfV16uZB8BBCoIyDmP60g5Z/McTwvmYDEPqVUue2wZAQQQQAABBBBAAAEEEEAAAQQ8CQwXAFFcaSzRt0BbDt3kpYzNDU2XEi6mj+VvO/l+3Shz72W/W4IgjfTZLQIrEQhzTewbnutGlv4g5/CNJYNSHjon1WGmPDTg/1C2q8vwQQABBBBAAAEEEEAAAQQQQAABBD4TGDUAcq3BZE3VIGcQRIct0eBSqyHGsg7vtaZKQF4RQOC6AMGP60aJS5ivRwt6fzBXVGKhsDoCCCCAAAIIIIAAAggggAACIwsMGQDRAnMwbJOnemNudLqU+PA27l7+/qBRBjUIosNhMdZ7owJgtwiMKEDwo3ipmnt/hGv5QX5eCrqb5xMpnkt2gAACCCCAAAIIIIAAAggggAACrgSGDYCEhhNtqP/elXi7xGSdTFwaC3eSlZaTo7+QIMjLdpzsGQEERhEg+FGlJM2B+AUvMnwn14E/qqSenSCAAAIIIIAAAggggAACCCCAQJcCowdANlIq2jjSat4Kb5UidxDkRjL4S8NMZs1Pw3ywawQQaCRA8KMK/N8SqNChKU0fKZuDrHCp9wdDX5k0WRgBBBBAAAEEEEAAAQQQQACBdQoMHQDRIpUGlOfy4+d1Fu/ZXL+V3+oQUh9zmITGw5bzgjA5eo6CZBsIrFBAzl+PJds7+RIkL1v+5mDFld4fHyS5OvF5lutY2ayzdQQQQAABBBBAAAEEEEAAAQQQaCkwfABEcaUhZS8/GArrv5qWNWgQ5gXRIEgrYyZHb3kWYd8IdCiwYHilDnPlMslRwYorvT/+J8EPva7zQQABBBBAAAEEEEAAAQQQQAABBGYF1hIA2YgCQ2F9XhW0UepxzvHTpcHqVrb5Y8NjjnlBGuKzawR6ESD4UbWkcs/98UquW9qzkw8CCCCAAAIIIIAAAggggAACCCBwVWAVARBVYCiss3VBe05oEGR/taYsXECct7Ko9gZpNaRM1iG+FmabxRBAoBMBgh9VC+qDXF821j3O9P6I6k1i3T/LI4AAAggggAACCCCAAAIIIIDAOAKrCYBokTEU1sWKa35Dd+4QcDAklg7xpfOcaK8fPggggMCdgJybXsqPZ3BUEzBfW64EqBj6qlrRsSMEEEAAAQQQQAABBBBAAAEExhBYWwBkI8XGUFjn6+5rCRjc5KzWjYfE0t4tt5InbfDkgwACKxeQ89FOCJ6snKFm9nP3/jBPpF4zs+wLAQQQQAABBBBAAAEEEEAAAQR8CqwqAKJFwFBYsxXxnfxVh8T6mKu6ivdD2ZYOifVtrm0at8OQWEYwFkdgJIHQI20neXo0Ur46yEvO3h/v5bqk1xI+CCCAAAIIIIAAAggggAACCCCAgElgdQEQ1WEorNk6kn34qNAAqT0xWr19rePG65BYe9PRwcIIINC1QDj36HH/oOuM9Jf4d3K+3VqSHcpKe2ieC5Z/x5CGFk2WRQABBBBAAAEEEEAAAQQQQACBSWCtAZB7AnCQb6uJur3XQB0+SgMG2nMj20cauB7LxnYN3RlCJVtpsiEEfAs46H3mG6hs6sxzdcwMmfiCoQzLFhZbRwABBBBAAAEEEEAAAQQQQGBkgVUGQLRAQ2P8ryMXboa8ZQ8YOBiOJnsPlwzObAIBBDIKyHlmK5vTAC5B7oyuCzcV2/vjcKa8zNtamEYWQwABBBBAAAEEEEAAAQQQQACBlQisNgCi5cukuItqefZ5QYL9jfzUYbFaNFAyQfqiomchBPoTkPO6nlt+6S/lw6T4vvTYOFhyc6H3h56nNznnpLKkiWURQAABBBBAAAEEEEAAAQQQQGAMgbUHQHQorL18GR9+vj7rHBo6ObqOz57tI41eG9mYBkFaTU6swR0d6uuQLVNsCAEEmgnIOUXPJ8+aJYAdv5Lz6XMLQ7gO/HlmnR9yD8NoSRfLIoAAAggggAACCCCAAAIIIIDAGAKrDoBoEYZx4n8foziL56LIWOyN5wahN0jxasMOECgrEIbW0yGvvi+7J7Y+IxDVY+NCT0xzIIWSQQABBBBAAAEEEEAAAQQQQAABBM4JrD4AEoIg+sbqz1SRRQJvZSntNfFx0dILFwoNmPr29pOFq+ReTHuDPM/dyyV3ItkeAgh8LhCC2Dv5LT352lYOc4A8zNXy20my38t5+GHbrLB3BBBAAAEEEEAAAQQQQAABBBAYRYAASChJaYjRt4dbDcXUW30qMiSWIoQGMW3M/LYRSvaJ3xvlg90iMLxA495jw/saMvhBghYbw/J3i0r57eXHca8d7UWyJRBtlWR5BBBAAAEEEEAAAQQQQAABBBC4JEAAJMiEHgg6x0Wrhvcea2mxYMGFSXFrGWmAR3u5aOMcHwQQcCjAfB+uCuV/1vNlCF79epKLp7KdnauckRgEEEAAAQQQQAABBBBAAAEEEOhagADIUfExH0hUXS42kXiYHFcbw1qN6/9a9q3DYmUd7itKmZUQQOBOgPk+3FWEt3KOfGxJ1YUXDl7Ldm4s22FZBBBAAAEEEEAAAQQQQAABBBBA4JoAAZATIWmYYT6Qa7Xmy7/rsCXaY0KHEcv+CW8K6/wgLXrnMEl69hJlgwjECYQh8vQ883XcFlgrs4CeHx/Kuf9g2e6ZHn7vZX0d+opgswWSZRFAAAEEEEAAAQQQQAABBBBA4KoAAZAzRMwHcrXeXFqgyATp085Co5kGqFo0fjIsVnS1YEUE0gUaD4uXnoExt2AeBjH07NPhJqfzOPN+jFk3yBUCCCCAAAIIIIAAAggggAACLgQIgJwPgNyTXzMfSFwVLd0bRMtGe4M8iUte8lrFhvxKThkbQGBAAYa8cluo76XHxkNr6s68YPBDqd6D1rSxPAIIIIAAAggggAACCCCAAAIIjCdAAORCmYb5QPby5xa9DUaoaaV7g2jDmwZCWs0P8kr2fcuQLSNUVfLgVSAMf7fjPOyyhGImPt9KTn47ys0rOYdqrz4+CCCAAAIIIIAAAggggAACCCCAQBEBAiAzrNL4diN//qWI/Do2WrQ3iBKGOQE0EPKgAanmT/f9kkBIA312OaxA6PVxKxl8Nmwm+85YVOBCyvUg2Z7mcorqQdI3G6lHAAEEEEAAAQQQQAABBBBAAIHaAgRArohLg81OFmk13FLt+lBqf0V7g2iiQ7BKG0xbTJSu84NobxCtK3wQQCBBIPS+02OpRVAzIeWrWVUDvxtr0PdkDpeobaxGmIwigAACCCCAAAIIIIAAAggggEA2AQIgCyil4UbnA6ExboHVzCLa4PW8dJCAQEhaIbE2Ai0FmOi8pf7ifZvn7AgTn/95tIfv5Fqg11U+CCCAAAIIIIAAAggggAACCCCAQFEBAiALeMNwLAdZlPlAFnhdWaTKJOIEQtILii0gUEuAXh+1pJP381YCF4+tW5Hy3cs603xNT0sHwq3pY3kEEEAAAQQQQAABBBBAAAEEEBhXgADIwrINDXS/L1ycxa4L/CSLFJ87g0DI9YJgCQRaCtDro6W+ad9Rw1adzKX1WoIfN6a9sjACCCCAAAIIIIAAAggggAACCCCQIEAAxIDHpOgGrGWL6twZOizWm2WLxy9FICTejjURKCFAr48SqkW3GTP01T1J0UG+2nvynZzrt0VTyMYRQAABBBBAAAEEEEAAAQQQQACBEwECIMYqIY12L2WVZ8bVWHxeoMqwWJqEEAh5Lv9sMaeLBny0/uysEwhTgRAYRSAMKajH4I+j5GkF+Ygd+kqD24/kq+e+h5z3VlBTyCICCCCAAAIIIIAAAggggAACzgQIgEQUiDTgTY06EWuzyozAK/nbbY1GMinDre5LvtO49DULRoeSmQIhh5o7Zl8ItBQIx91O0vBty3Swb5NA7NBXOlfIr/LV9bdMem4yZ2EEEEAAAQQQQAABBBBAAAEEEMgkQAAkAjK8wbyXVVv0IohIcVeraGOZBkE0QFD8I2W50f3J90nxnZ3fwWv5tfYI0frEB4EhBcJxpse09gbg05dA6tBX5vX74iG1CCCAAAIIIIAAAggggAACCCDgWYAASGTphAa9P2R1HducT34BHTLlplZg4GhYnhvZb4u309/LfnVS+F1+SraIQDuBMMm5DnnFubJdMcTuOWrS8qNeki9qBbNjM8h6CCCAAAIIIIAAAggggAACCCAwtgABkITyDZP47mnYS0C8vqrOD6I9QtS5yifME3IjO2s1PNZO9q3BkEOVDLMTBAoIMNxVAdS6m4yat0PKfRr6Kip4UjeL7A0BBBBAAAEEEEAAAQQQQAABBEYXIACSWMKhsfyXxM2w+nWBt7LI85pBgdDLR99cv5Fvi7fXNfijw2PtrvOwBAI+BBjuykc5ZEjF/6yB59CT7iD7/kPW3WZIA5tAAAEEEEAAAQQQQAABBBBAAAEEkgQIgCTx/buyNPpoI/nPGTbFJq4L6JwZ2iNEG9mqfRz0CnkjmdVeITrsGh8E3AkcDSP3o7vEkSCrwE9yrrm1rhSGvtrIejrp+Ufr+iyPAAIIIIAAAggggAACCCCAAAII5BYgAJJJVBp+drKpVhNpZ8pFV5tpFQjRxr2b8G0xV4gOS6OTSb+pHQTqqnaQ2KoCIQh8Kztt0VOqal5XsLN3Mb03wtBXeh3cEPxYQS0hiwgggAACCCCAAAIIIIAAAgh0IkAAJGNBHU38mnGrbOqKQJNAiKYpzHFwI//UMe9bNPzqxOna4EgwhMOkiUDoGXUrO28RDGyS58F3+oPqBLIAABkzSURBVLfk76E1uBqGPdvruZBeaoPXELKHAAIIIIAAAggggAACCCCAQGcCBEAyFlgYAkYbgR5k3CybWibQLBCiyQtvP2sgpFUvIIIhy+oJS2UQYILzDIg+N/GDBDB0uD3TR+qDXvd0aEL9yQcBBBBAAAEEEEAAAQQQQAABBBBwI0AAJHNREATJDGrfnAZCms2VEcpfAyH6fWRPfpY1pmDInrexs3iykSAQAh+38r/fgzKcwCs5X+h8VqZPGP7so6y7M63IwggggAACCCCAAAIIIIAAAggggEAFAQIgBZDDcCA6WXWLYZEK5KjLTb6TVDd/I/moZ0irYbJ0zhB9o1uDIeY3u7sseRKdXYDAR3ZSbxt8L+eHh9ZESb3QdXTIrJ11XZZHAAEEEEAAAQQQQAABBBBAAAEEaggQACmkHBqG9rJ5giCFjBduVntDaI+Q5g10oRF56h3Sas6Et+Kh9ZLeIQsr0JoXI/CxitKPmvdDZbR+MOzVKuoImUQAAQQQQAABBBBAAAEEEECgWwECIAWLjiBIQVz7prUnhAZBNBjy0b563jVCLyENhmzl22qoLG343E9fhsvKW8Y9by1Mbn4jeWCoq54Lclna/0cQYxkUSyGAAAIIIIAAAggggAACCCCAQH8CBEAKl1l4g/q3wrth8zaBpvOEnEvqUe+Qrfz9gS072ZY+Doj8QaNoNtduNhQCH7eS4FY9lLqxGiShP8lxruXNBwEEEEAAAQQQQAABBBBAAAEEEBhSgABIhWINjYq/VNgVu7AJuBke6zjZYSL1rfxu+rYKiGiydC4Vnc/m7ksvEVsF62HpUN908mv9MmRfD4WWJ41v5XjWXmh8EEAAAQQQQAABBBBAAAEEEEAAgWEFCIBUKlqCIJWg43ajPR928tXhsQ5xmyi31lFARCcc3sq39bBEGhRRp+PASPNhxcqVgG3LobzueaxLxzkJQ/Rp0OOJLYcsPYCABn91/g6O2wEKkywggAACCCCAAAIIIIAAAggggMBlAQIgFWsHQZCK2PG7uusVIt83nhsHQ+P1VtKpQRH9tuwlotoaRJoCItqoupfvx5F7jIR5XDbB/174qQYHz/WH+T3iTw6DrKnHqgY/tK7yQQABBBBAAAEEEEAAAQQQQAABBIYWIABSuXgJglQGj9+dNhK+CQ3Z+tP9J8wjMgVENpLg1j1FJrMpOKKBkanRdR/+ePDYUyIEmDSooZ9t+Km2+ju11TkypnxpXjRg5rZBOQRrbiSd+mV+D/dHc9EE/iB1tYtzWlEFNo4AAggggAACCCCAAAIIIIAAAqsQIADSoJgJgjRAT9vlB23glu/OcyP3uSyGhvyN/G0KjGgDvpfAyGmS1flw9Mv9mTyd+92S0p16aBwve/o7NZqbA6O7+VCk/HWOhxv5PlqCxDLDC7yQc5j2cOODAAIIIIAAAggggAACCCCAAAIIrEKAAEijYiYI0gg+fbfaSD8NkXVI31ybLZwZvmkbUuI1OFILaurVoWWr373+9NhL5RJICHrdyN/1y6TmtWqO//28lnqsdYIPAggggAACCCCAAAIIIIAAAgggsBoBAiANi1oaKrUh/VnDJLDrNAGdL2QnXx3+6JC2KV9rh+G0NFHbkLKN/NSvfnoOkky9TL4YjkvKcO+rFJanJgS0pt4ereeDWZ5wlqwl8F7qt/Zw4oMAAggggAACCCCAAAIIIIAAAgisSoAASOPilobLW0nCj42Twe7TBTQYspdvd8NkpWT9KFCim5nmyJg2efr/0+9TAyhTL43jpB8HNPT3h/DVfw85GbvY6xBeU9Aj1TSlGrCubwE9N+mk53qM8EEAAQQQQAABBBBAAAEEEEAAAQRWJUAAxEFxMxyWg0LImwTtZbCXr/YMYbLhvLar3tpR0EMDH8zrserasCjzGizU4Mcfi5ZmIQQQQAABBBBAAAEEEEAAAQQQQGAwAQIgTgqUIIiTgiiTjLdHAZFDmV2w1VEFjoa30qAHPT1GLegy+fqO4EcZWLaKAAIIIIAAAggggAACCCCAAAJ9CBAAcVROBEEcFUa5pGjvEO0Vstcvw9KUg+55y0cTmW8lH8zp0XNhtkv7Uzm/7Nrtnj0jgAACCCCAAAIIIIAAAggggAAC7QUIgLQvg89SQBDEWYGUT840dwgBkfLWbvcQenlosEO/2tPja7eJJWE9CBD86KGUSCMCCCCAAAIIIIAAAggggAACCBQXIABSnNi+A4IgdrOB1jgOiPwhb3AfBsobWQkCYS4PDXZMX3p5UDtyCbyW88ZNro2xHQQQQAABBBBAAAEEEEAAAQQQQKBnAQIgTkuPIIjTgqmfrGlCdZ3EWAMi+/pJYI+pAqGHx0MCHqmSrH9FgOAHVQQBBBBAAAEEEEAAAQQQQAABBBA4EiAA4rg6SKPpVpKn80UwHI7jcmqQNO0lchcQ0S9BkQYlcGWX4djVgMcU9PjWXypJ0WACBD8GK1CygwACCCCAAAIIIIAAAggggAAC6QIEQNINi24hTIa8JwhSlHmEjWtQ5KABEflqfTkwfFadYj0Kdmxkjxrw+L7OntkLAp8E9PjfyjH/ERMEEEAAAQQQQAABBBBAAAEEEEAAgf8ECIB0UBsIgnRQSH6T+E6Spo2iU4+Rj/QYiSuscBxOQQ4NdOi/mbsjjpO18gkQ/MhnyZYQQAABBBBAAAEEEEAAAQQQQGAwAQIgnRRomDR5T4NrJwXmP5l/h6DIFBw5yP/rVwMkGixZ5ScEOe5J5jXAoT+34SeBjlXWCPeZJvjhvohIIAIIIIAAAggggAACCCCAAAIItBQgANJS37hvgiBGMBZPFdDeI/o5hK/+ez9ttLeeJGGoKk3+FODQf0+BDv3JXDupNYb1awoQ/Kipzb4QQAABBBBAAAEEEEAAAQQQQKBLAQIgnRVbCIK8lGQ/6SzpJHdsgQ+SvcNRFqeeJae5vvR7q85xEON43SmgMf2O+TissizfgwDBjx5KiTQigAACCCCAAAIIIIAAAggggEBzAQIgzYsgLgESCNnJmgRB4vhYCwEEEOhVgOBHryVHuhFAAAEEEEAAAQQQQAABBBBAoLoAAZDq5Pl2KEGQG9naL/m2yJYQQAABBBwLEPxwXDgkDQEEEEAAAQQQQAABBBBAAAEE/AkQAPFXJqYUhSCIDonF/AUmORZGAAEEuhIg+NFVcZFYBBBAAAEEEEAAAQQQQAABBBDwIEAAxEMpJKZBgiA678GeIEgiJKsjgAACPgUIfvgsF1KFAAIIIIAAAggggAACCCCAAALOBQiAOC+gpckLk6NrEOTB0nVYDgEEEEDAvQDBD/dFRAIRQAABBBBAAAEEEEAAAQQQQMCrAAEQryUTka4QBNnJqo8iVmcVBBBAAAFfAgQ/fJUHqUEAAQQQQAABBBBAAAEEEEAAgc4ECIB0VmBLkiuBkFtZ7scly7IMAggggIBLAYIfLouFRCGAAAIIIIAAAggggAACCCCAQE8CBEB6Ki1DWpkc3YDFoggggIAvgdeSnOf/93//99FXskgNAggggAACCCCAAAIIIIAAAggg0JcAAZC+ysuU2jA5+htZ6VvTiiyMAAIIINBK4LUEPm5a7Zz9IoAAAggggAACCCCAAAIIIIAAAiMJEAAZqTTP5CXMC6JBkO8HzyrZQwABBHoXIPjRewmSfgQQQAABBBBAAAEEEEAAAQQQcCVAAMRVcZRLjARCXsrWn5XbA1tGAAEEEEgQeCo9P3YJ67MqAggggAACCCCAAAIIIIAAAggggMCJAAGQFVUJ5gVZUWGTVQQQ6EmA4EdPpUVaEUAAAQQQQAABBBBAAAEEEECgGwECIN0UVZ6EhnlBdrK1B3m2yFYQQAABBCIF/pb1ttLz44/I9VkNAQQQQAABBBBAAAEEEEAAAQQQQGBGgADICqtHmBdEh8R6ssLsk2UEEEDAg8B7ScQNwQ8PRUEaEEAAAQQQQAABBBBAAAEEEEBgVAECIKOW7IJ8SSDkuSz284JFWQQBBBBAIJ+ABj+058fHfJtkSwgggAACCCCAAAIIIIAAAggggAACpwIEQFZeJ8KQWG+E4duVU5B9BBBAoIbAawl83NTYEftAAAEEEEAAAQQQQAABBBBAAAEE1i5AAGTtNUDyH4bE2sk/H8GBAAIIIFBM4IUEP3T4QT4IIIAAAggggAACCCCAAAIIIIAAAhUECIBUQO5lFxIIuZG0auPc172kmXQigAACHQjoZOc634f2tuODAAIIIIAAAggggAACCCCAAAIIIFBJgABIJehedhOGxNpJeh/0kmbSiQACCDgWYLJzx4VD0hBAAAEEEEAAAQQQQAABBBBAYGwBAiBjl2907iQQoj1BnkVvgBURQAABBN4Kgfb8YLJz6gICCCCAAAIIIIAAAggggAACCCDQQIAASAP0XnYpQZCtpFWHbGFIrF4KjXQigIAXgZ8k8HHrJTGkAwEEEEAAAQQQQAABBBBAAAEEEFijAAGQNZa6Ic9MkG7AYlEEEEDgq690vo/HEvzYg4EAAggggAACCCCAAAIIIIAAAggg0FaAAEhb/272LoGQx5LYnXzpDdJNqZFQBBCoLKDzfWjw41B5v+wOAQQQQAABBBBAAAEEEEAAAQQQQOCMAAEQqsVigdAbRIfE+n7xSiyIAAIIrEPglQQ+nq8jq+QSAQQQQAABBBBAAAEEEEAAAQQQ6EOAAEgf5eQqlRII0Ua+W/nSG8RVyZAYBBBoIKBDXulE5xoc5oMAAggggAACCCCAAAIIIIAAAggg4EiAAIijwugpKRIE2Uh6d/KlN0hPBUdaEUAgpwBDXuXUZFsIIIAAAggggAACCCCAAAIIIIBAZgECIJlB17Y5CYTcSJ5fypfeIGsrfPKLwLoFfpJeH7frJiD3CCCAAAIIIIAAAggggAACCCCAgG8BAiC+y6eL1IW5QXaS2EddJJhEIoAAAvECH2RVHfJqH78J1kQAAQQQQAABBBBAAAEEEEAAAQQQqCFAAKSG8kr2IYGQx5JV7Q3y7UqyTDYRQGBdAq8lu88l+PFxXdkmtwgggAACCCCAAAIIIIAAAggggECfAgRA+iw3t6kOvUFuJYHP3CaShCGAAAI2ASY6t3mxNAIIIIAAAggggAACCCCAAAIIIOBCgACIi2IYLxESCHkoudrJ98F4uSNHCCCwIoG3klcd8opeHysqdLKKAAIIIIAAAggggAACCCCAAAJjCBAAGaMc3eZCAiHPJXG38mWSdLelRMIQQOCMAL0+qBYIIIAAAggggAACCCCAAAIIIIBA5wIEQDovwB6SH4bF0rlBnvSQXtKIAAKrF6DXx+qrAAAIIIAAAggggAACCCCAAAIIIDCCAAGQEUqxkzxIIGQrSdVACMNidVJmJBOBlQnQ62NlBU52EUAAAQQQQAABBBBAAAEEEEBgbAECIGOXr8vcSSDkJgRCGBbLZQmRKARWKfBacv2cuT5WWfZkGgEEEEAAAQQQQAABBBBAAAEEBhUgADJowXrPVhgW61bS+cx7WkkfAggMLfBBcqeTnO+HziWZQwABBBBAAAEEEEAAAQQQQAABBFYoQABkhYXuKcsSCNlIenby/d5TukgLAgisQuAnCXzcriKnZBIBBBBAAAEEEEAAAQQQQAABBBBYoQABkBUWuscsMz+Ix1IhTQgMK/BOcqa9Pg7D5pCMIYAAAggggAACCCCAAAIIIIAAAgh8RQCESuBKIMwPciuJ+tZVwkgMAgiMIKDDXek8H29GyAx5QAABBBBAAAEEEEAAAQQQQAABBBCYFyAAQg1xKSCBEA2CPJcvE6W7LCEShUBXAn9Lal/ql0nOuyo3EosAAggggAACCCCAAAIIIIAAAggkCRAASeJj5ZICYaJ0DYIQCCkJzbYRGFvgtWTvluGuxi5kcocAAggggAACCCCAAAIIIIAAAgicEyAAQr1wLxACIbeS0GfuE0sCEUDAi4DO86GBj72XBJEOBBBAAAEEEEAAAQQQQAABBBBAAIG6AgRA6nqztwQBCYRstEFTvk8SNsOqCCAwtgDzfIxdvuQOAQQQQAABBBBAAAEEEEAAAQQQWCxAAGQxFQt6ESAQ4qUkSAcCrgR0ng/t8aFzffBBAAEEEEAAAQQQQAABBBBAAAEEEEDgKwIgVIJuBQiEdFt0JByBnAJMcJ5Tk20hgAACCCCAAAIIIIAAAggggAACAwkQABmoMNealRAI0YnSb+T79VodyDcCKxMg8LGyAie7CCCAAAIIIIAAAggggAACCCCAgFWAAIhVjOXdCoTJ0jUQol8CIW5LioQhkCzwSragw119TN4SG0AAAQQQQAABBBBAAAEEEEAAAQQQGFaAAMiwRbvejB0FQm5E4dv1SpBzBIYS0B4fb0Lg4zBUzsgMAggggAACCCCAAAIIIIAAAggggEARAQIgRVjZqBcBCYZoEORWvgRCvBQK6UDAJsBQVzYvlkYAAQQQQAABBBBAAAEEEEAAAQQQCAIEQKgKqxCQQMg2BEK+X0WGySQC/QsQ+Oi/DMkBAggggAACCCCAAAIIIIAAAggg0FSAAEhTfnZeWyBMmH4r+30sX+YJqV0A7A+B6wIfZJGX8t0xx8d1LJZAAAEEEEAAAQQQQAABBBBAAAEEELgsQACE2rFKgTBPyI1kXidMZ3isVdYCMu1M4L0GPiTosXOWLpKDAAIIIIAAAggggAACCCCAAAIIINCpAAGQTguOZOcTkGCI9gbRYMijfFtlSwggsFDgbQh87Bcuz2IIIIAAAggggAACCCCAAAIIIIAAAggsEiAAsoiJhdYgEIbH0kCIfukVsoZCJ4+tBHR+j518tcfHoVUi2C8CCCCAAAIIIIAAAggggAACCCCAwNgCBEDGLl9yFylAr5BIOFZDYF7gbpgr+b5hfg+qCgIIIIAAAggggAACCCCAAAIIIIBAaQECIKWF2X7XAqFXiA6RxVwhXZckiW8s8FoDHxL0+KNxOtg9AggggAACCCCAAAIIIIAAAggggMCKBAiArKiwyWqagARDHoZAiAZEvk7bGmsjMLwAvT2GL2IyiAACCCCAAAIIIIAAAggggAACCPgWIADiu3xInVMBCYbcSNI0EMLE6U7LiGQ1EdC5Pd7Il94eTfjZKQIIIIAAAggggAACCCCAAAIIIIDAsQABEOoDAgkCEgi5FwIhBEMSHFm1e4G3koOdDHGlwQ8+CCCAAAIIIIAAAggggAACCCCAAAIIuBAgAOKiGEjECAJH84XcSH4ejJAn8oDAjABDXFE9EEAAAQQQQAABBBBAAAEEEEAAAQRcCxAAcV08JK5XgaNgyFbywDBZvRYk6T4V0KDHTr5vpLfHAR4EEEAAAQQQQAABBBBAAAEEEEAAAQQ8CxAA8Vw6pG0IAYbJGqIY15wJgh5rLn3yjgACCCCAAAIIIIAAAggggAACCHQsQACk48Ij6X0KSEBE5wvR71a+3/aZC1I9uIDO6bGXLz09Bi9osocAAggggAACCCCAAAIIIIAAAgiMLEAAZOTSJW/uBSQY8jAEQzQgwrwh7kts2AT+rcGOo6DHx2FzSsYQQAABBBBAAAEEEEAAAQQQQAABBFYjQABkNUVNRr0LhKGytiEgoj/pHeK90PpOnw5ttdfAh8znoT/5IIAAAggggAACCCCAAAIIIIAAAgggMJQAAZChipPMjCQQJlLfSp6m4bK+Hil/5KW6wAfZowY69KtBD3p5VC8CdogAAggggAACCCCAAAIIIIAAAgggUFOAAEhNbfaFQIJAGC5rK5vQrw6dRQ+RBM8VrKrDWu2nrwQ8/lhBnskiAggggAACCCCAAAIIIIAAAggggAACnwQIgFAZEOhU4KiHyBQQYQ6RTssyU7KnHh4a6NgT8MikymYQQAABBBBAAAEEEEAAAQQQQAABBLoVIADSbdGRcAQ+FwhziGjPkCkgoj8ZNmvcivJOsnYX7NCfEvA4jJtVcoYAAggggAACCCCAAAIIIIAAAggggIBdgACI3Yw1EOhGIPQS0aDI8Zehs7opwU8J1QnLNdhB747+yo4UI4AAAggggAACCCCAAAIIIIAAAgg0EiAA0gie3SLQSuCop8gUFNlIWr5vlR72+5mAztsx9eo46L8ZyooaggACCCCAAAIIIIAAAggggAACCCCAQJwAAZA4N9ZCYDiB0FtEgyFb+d6TrwZI9P/pMZK/tHW+joN8Ndgx/dRgx8f8u2KLCCCAAAIIIIAAAggggAACCCCAAAIIrFOAAMg6y51cI2ASkOCIBkM0KLINP/X/9UPPkcuSOmyVBjQ0yKE/9/qTHh2mqsfCCCCAAAIIIIAAAggggAACCCCAAAIIRAsQAImmY0UEEJgEJECyDf+eAiVTDxL99Ua+I/UimYap0rwdwlf/vQ8G9OTg0EAAAQQQQAABBBBAAAEEEEAAAQQQQMCBAAEQB4VAEhBYi8DRMFtTlrcneZ8CKKck+vuvMzlNw0+dbm7qqTH9/iD/0O/dR3pu7DPtn80ggAACCCCAAAIIIIAAAggggAACCCCAQAUBAiAVkNkFAggggAACCCCAAAIIIIAAAggggAACCCCAAAII1BUgAFLXm70hgAACCCCAAAIIIIAAAggggAACCCCAAAIIIIBABYH/B9PNxWg6IohyAAAAAElFTkSuQmCC",
                                width: 80,
                                height: 30,
                                margin: [15, 5, 0, 5],
                              },
                            ],
                          },
                          {
                            text: " Renewable Resource Assessment Platform",
                            fontSize: 9,
                            margin: [0, 15, 0, 0],
                            bold: false,
                          },
                        ],
                      },
                    ],
                  ],
                },
                layout: "noBorders",
              },
            ];
          },
          footer: function (currentPage, pageCount, pageSize) {
            return [
              {
                style: "footer",
                table: {
                  widths: ["100%"],
                  heights: 20,
                  body: [
                    [
                      {
                        margin: [10, 5, 10, 5],
                        columns: [
                          {
                            text: [
                              "CONFIDENTIAL AND PROPRIETARY FOR INTERNAL USE ONLY",
                            ],
                            style: "",
                            width: "70%",
                          },
                          {
                            text: ["Page " + currentPage],
                            style: "rightalign",
                            width: "30%",
                          },
                        ],
                      },
                    ],
                  ],
                },
                layout: "noBorders",
              },
            ];
          },
          content: [
            {
              style: "toptable",
              table: {
                widths: "100%",
                body: [
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: ["Project"],
                          style: "toptableheading",
                          width: "25%",
                        },
                        {
                          text: [result.project_name],
                          style: "toptablevalue",
                        },
                      ],
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: ["Layout"],
                          style: "toptableheading",
                          width: "25%",
                        },
                        {
                          text: [eyCalc.site[0].site_name],
                          style: "toptablevalue",
                        },
                      ],
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: ["Date"],
                          style: "toptableheading",
                          width: "25%",
                        },
                        { text: [creationdate], style: "toptablevalue" },
                      ],
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: [
                            eyCalc.turbine_type[0].comments.length > 0
                              ? "Comments"
                              : "",
                          ],
                          style: "toptableheading",
                          width: "25%",
                        },
                        {
                          text: [eyCalc.turbine_type[0].comments],
                          style: "comments",
                        },
                      ],
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: turbineModel,
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: turbineRatedPower,
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: hubHeight,
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: numberOfTurbine,
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: ["Total Number of Turbines"],
                          style: "toptableheading",
                          width: "25%",
                        },
                        {
                          text: [eyCalc.site[0].turbines],
                          style: "toptablevalue",
                        },
                      ],
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: ["Plant Capacity"],
                          style: "toptableheading",
                          width: "25%",
                        },
                        {
                          text: [eyCalc.site[0].mw + " MW"],
                          style: "toptablevalue",
                        },
                      ],
                    },
                  ],
                  [
                    {
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: ["Site Air Density (kg/m³)"],
                          style: "toptableheading",
                          width: "25%",
                        },
                        {
                          text: [
                            checkValueOrBlank(
                              eyCalc.site[0].mean_air_density_at_turbines,
                              true
                            ),
                          ],
                          style: "toptablevalue",
                        },
                      ],
                    },
                  ],
                  [
                    {
                      style: result.hasGrdAdjustmentData ? "" : "noDisplay",
                      margin: [10, 5, 10, 5],
                      columns: [
                        {
                          text: [
                            result.hasGrdAdjustmentData
                              ? "Mast Adjusted Grid Created"
                              : "",
                          ],
                          style: "toptableheading",
                          width: "25%",
                        },
                        {
                          text: [
                            result.hasGrdAdjustmentData
                              ? "Name: Adjusted_" +
                                result.source_info.gridAdjustmentData.baseGrid +
                                "\n" +
                                "Base Grid Name: " +
                                result.source_info.gridAdjustmentData.baseGrid +
                                "\n" +
                                "Swithing Method: " +
                                result.source_info.gridAdjustmentData
                                  .switchMethod +
                                "\n" +
                                "Time Series Files: " +
                                (result.source_info.gridAdjustmentData.siteNames.toString()
                                  .length > 0
                                  ? result.source_info.gridAdjustmentData.siteNames.toString()
                                  : "NA") +
                                "\n" +
                                "Frequency Tabs: " +
                                (result.source_info.gridAdjustmentData.tabNames.toString()
                                  .length > 0
                                  ? result.source_info.gridAdjustmentData.tabNames.toString()
                                  : "NA") +
                                "\n"
                              : "",
                          ],
                          style: "toptablevalue",
                        },
                      ],
                    },
                  ],
                ],
              },
              layout: {
                fillColor: function (rowIndex, node, columnIndex) {
                  return rowIndex % 2 === 0 ? "#fafafa" : null;
                },
                hLineWidth: function (i, node) {
                  return 0.05;
                },
                vLineWidth: function (i, node) {
                  return 0.05;
                },
                hLineColor: function (i, node) {
                  return "#D1CDCD";
                },
                vLineColor: function (i, node) {
                  return "#D1CDCD";
                },
              },
            },
            {
              margin: [5, 10, 0, 15],
              table: {
                width: ["100%"],
                body: [
                  [
                    [
                      {
                        columns: [
                          { text: estimatesComment, style: "comments2" },
                        ],
                      },
                    ],
                  ],
                ],
              },
              layout: {
                hLineColor: function (i, node) {
                  return "#ffffff";
                },
                vLineColor: function (i, node) {
                  return "#ffffff";
                },
              },
            },
            {
              style: "midtable",
              layout: "noBorders",
              table: {
                widths: ["50%", "50%"],
                body: [
                  [
                    {
                      table: {
                        widths: "100%",
                        headerRows: 1,
                        body: [
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[0]],
                                  style: "midgreentableHeader",
                                  width: "100%",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[1]],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [wakeLossValueText],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[2]],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      lossesValues.availability[0][
                                        "Availability Total"
                                      ],
                                      true
                                    ) + "%",
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[3]],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      lossesValues.electrical[0][
                                        "Electrical Total"
                                      ],
                                      true
                                    ) + "%",
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[4]],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      lossesValues.turbinePerformance[0][
                                        "Turbine Performance Total"
                                      ],
                                      true
                                    ) + "%",
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[5]],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      lossesValues.environmental[0][
                                        "Environmental Total"
                                      ],
                                      true
                                    ) + "%",
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[6]],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      lossesValues.curtailments[0][
                                        "Curtailments Total"
                                      ],
                                      true
                                    ) + "%",
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: [lossAvbAccTableText[7]],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(totalLoss, true) + "%",
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                        ],
                      },
                      layout: {
                        fillColor: function (rowIndex, node, columnIndex) {
                          let colorval = "#c5e6e4";
                          if (rowIndex === 0) {
                            colorval = "#58b7b3";
                          } else {
                            if (rowIndex % 2 === 0) {
                              colorval = "#eef8f7";
                            }
                          }
                          return colorval;
                        },
                        hLineWidth: function (i, node) {
                          return i === 0 || i === node.table.body.length
                            ? 0.05
                            : 0;
                        },
                        vLineWidth: function (i, node) {
                          return i === 0 || i === node.table.widths.length
                            ? 0.05
                            : 0;
                        },
                        hLineColor: function (i, node) {
                          return "#D1CDCD";
                        },
                        vLineColor: function (i, node) {
                          return "#D1CDCD";
                        },
                      },
                    },
                    {
                      table: {
                        widths: "100%",
                        headerRows: 1,
                        body: [
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: ["Overall Wind Plant Summary"],
                                  style: "midgreentableHeader",
                                  width: "100%",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: ["Average Free Wind Speed (m/s)"],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      eyCalc.site[0]
                                        .mean_free_wind_speed_at_turbines,
                                      true
                                    ),
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: ["Gross Plant Production (GWh/yr)"],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      eyCalc.site[0].gross_energy,
                                      true
                                    ),
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: ["Net Plant Production (GWh/yr)"],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      eyCalc.site[0].net_energy,
                                      true
                                    ),
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                          [
                            {
                              margin: [10, 5, 10, 5],
                              columns: [
                                {
                                  text: ["Net Capacity Factor"],
                                  style: "tableheading",
                                  width: "50%",
                                },
                                {
                                  text: [
                                    checkValueOrBlank(
                                      eyCalc.site[0].capacity_factor,
                                      true
                                    ),
                                  ],
                                  style: "midtablevalue",
                                },
                              ],
                            },
                          ],
                        ],
                      },
                      layout: {
                        fillColor: function (rowIndex, node, columnIndex) {
                          let colorval = "#fad6b1";
                          if (rowIndex === 0) {
                            colorval = "#f18a21";
                          } else {
                            if (rowIndex % 2 === 0) {
                              colorval = "#fef3e9";
                            }
                          }
                          return colorval;
                        },
                        hLineWidth: function (i, node) {
                          return i === 0 || i === node.table.body.length
                            ? 0.05
                            : 0;
                        },
                        vLineWidth: function (i, node) {
                          return i === 0 || i === node.table.widths.length
                            ? 0.05
                            : 0;
                        },
                        hLineColor: function (i, node) {
                          return "#D1CDCD";
                        },
                        vLineColor: function (i, node) {
                          return "#D1CDCD";
                        },
                      },
                    },
                  ],
                ],
              },
            },
            {
              margin: [0, 0, 0, 15],
              table: {
                width: ["100%"],
                body: [
                  [
                    {
                      text:
                        'Note: 1. "' +
                        eyCalc.ect_test_parameters.wake_model +
                        '" Wake model used in calcuation.\n' +
                        neighboringLayoutNote +
                        "\n" +
                        note,
                      style: "tableheading2",
                    },
                  ],
                  [neighboringLayoutTable],
                ],
              },
              layout: {
                hLineColor: function (i, node) {
                  return "#ffffff";
                },
                vLineColor: function (i, node) {
                  return "#ffffff";
                },
              },
            },
            {
              style: "bottomtable",
              margin: [0, 15, 0, 0],
              table: {
                tableLayouts: "noBorders",
                body: rows,
              },
              layout: {
                fillColor: function (rowIndex, node, columnIndex) {
                  return "#f4f5f5";
                },
                paddingTop: function (rowIndex, node, columnIndex) {
                  return 5;
                },
                paddingBottom: function (rowIndex, node, columnIndex) {
                  return 5;
                },
                hLineWidth: function (i, node) {
                  return 0.5;
                },
                vLineWidth: function (i, node) {
                  return 0.5;
                },
                hLineColor: function (i, node) {
                  return "#dee2e6";
                },
                vLineColor: function (i, node) {
                  return "#dee2e6";
                },
              },
            },
          ],
          styles: {
            noDisplay: {
              display: "none",
            },
            redtable: {
              width: "100%",
              fillColor: "#ca0123",
              color: "white",
            },
            toptableheading: {
              fontSize: 10,
              color: "#00518b",
            },
            tableheading: {
              fontSize: 9,
              color: "#333333",
              bold: true,
            },
            tableheading2: {
              fontSize: 8,
            },
            tableheaderrow: {
              fillColor: "#949699",
              color: "#ffffff",
              fontSize: 9,
            },
            toptablevalue: {
              fontSize: 10,
              color: "#4f3a33",
            },
            surroundingTablevalue: {
              fontSize: 8,
              color: "#4f3a33",
            },
            tableHeader: {
              fontSize: 11,
              bold: true,
              color: "#ffffff",
            },

            toptable: {
              margin: [0, 5, 0, 15],
            },
            comments: {
              fontSize: 8,
              color: "#ff0000",
            },
            comments2: {
              fontSize: 11,
              color: "#ff0000",
            },
            midtable: {
              fontSize: 6,
              margin: [0, 5, 0, 2],
            },
            midtablevalue: {
              fontSize: 10,
              color: "#4f3a33",
              alignment: "right",
            },
            midgreentableHeader: {
              fontSize: 11,
              bold: true,
              color: "#ffffff",
              fillColor: "#58b7b3",
            },
            bottomtable1: {
              margin: [0, 15, 0, 0],
            },
            bottomtable: {
              width: "100%",
              fontSize: 7.5,
              margin: [0, 0, 0, 15],
            },
            footer: {
              width: "100%",
              fillColor: "#ca0123",
              color: "white",
              fontSize: 8,
              margin: [0, 35, -1, 0],
            },
            rightalign: {
              alignment: "right",
            },
            leftAlign: {
              alignment: "left",
            },
          },
        };
        /* -----------------------------  Main structure ends here  ------------------------  */

        // Addition to content section
        // If there is MAST
        if (metMast.length > 1) {
          dd.content.push({
            style: "bottomtable",
            margin: [0, 15, 0, 0],
            table: {
              tableLayouts: "noBorders",
              body: metMast,
            },
            layout: {
              fillColor: function (rowIndex, node, columnIndex) {
                return "#f4f5f5";
              },
              paddingTop: function (rowIndex, node, columnIndex) {
                return 5;
              },
              paddingBottom: function (rowIndex, node, columnIndex) {
                return 5;
              },
              hLineWidth: function (i, node) {
                return 0.5;
              },
              vLineWidth: function (i, node) {
                return 0.5;
              },
              hLineColor: function (i, node) {
                return "#dee2e6";
              },
              vLineColor: function (i, node) {
                return "#dee2e6";
              },
            },
          });
        }

        if (eyCalc.losses_parameters != undefined) {
          // Losses data being punched into the pdf file.
          angular.forEach(lossesValues, function (value, index) {
            let box = lossesData[index];
            angular.forEach(value[0], function (colValue, heading) {
              box.push([
                {
                  text: [heading],
                },
                {
                  text: [colValue + percentage],
                },
              ]);
            });

            dd.content.push({
              style: "bottomtable",
              id: "table-losses",
              margin: [0, 15, 0, 0],
              table: {
                widths: ["70%", "20%"],
                tableLayouts: "noBorders",
                body: box,
              },
              layout: {
                fillColor: function (rowIndex, node, columnIndex) {
                  return "#f4f5f5";
                },
                paddingTop: function (rowIndex, node, columnIndex) {
                  return 5;
                },
                paddingBottom: function (rowIndex, node, columnIndex) {
                  return 5;
                },
                hLineWidth: function (i, node) {
                  return 0.5;
                },
                vLineWidth: function (i, node) {
                  return 0.5;
                },
                hLineColor: function (i, node) {
                  return "#dee2e6";
                },
                vLineColor: function (i, node) {
                  return "#dee2e6";
                },
              },
            });
          });
        }

        if (!contentOnly) {
          // The name of the pdf....
          const fileName =
            result.project_name !== null
              ? result.project_name +
                "_" +
                result.key.split("_").slice(1).join("_")
              : result.key;

          // The final spit out
          pdfMake.createPdf(dd).download(fileName + ".pdf");
        } else {
          return dd;
        }
      };
      /* ------------------------- Export pdf  ------------------- */

      /* ------------------------- calculate number of turbines of each turbine type height ------------------- */
      // Calculate the number of turbines by height.
      // TODO -- need to see the difference between this function and calculateNumberOfTurbine....
      const countNumberOfHubHeight = function (turbines, turbineType) {
        const hubHeightArr = [];
        turbines
          .filter(
            (turbine) =>
              turbine.turbine_type
                .replace(/Unknown/gi, "")
                .trim()
                .toLowerCase() ===
              turbineType
                .replace(/Unknown/gi, "")
                .trim()
                .toLowerCase()
          )
          .forEach(function (element) {
            hubHeightArr.push(element.hub_height);
          });
        return hubHeightArr
          .filter(function (value, index) {
            return hubHeightArr.indexOf(value) === index;
          })
          .join(", ");
      };
      /* ------------------------- calculate number of turbines of each turbine type height ------------------- */

      /* ------------------------- calculate turbines ------------------- */
      // Calculate the total number of turbines and group them by type.
      const calculateNumberOfTurbine = function (turbines, turbineType) {
        // Filter function by type
        const filteredTurbineArry = turbines.filter(
          (turbine) =>
            turbine.turbine_type
              .replace(/Unknown/gi, "")
              .trim()
              .toLowerCase() ===
            turbineType
              .replace(/Unknown/gi, "")
              .trim()
              .toLowerCase()
        );
        // grouping by height
        const grouped = filteredTurbineArry.reduce((acc, obj) => {
          acc[obj.hub_height] = (acc[obj.hub_height] || 0) + 1;
          return acc;
        }, {});
        // temporary storage
        const tempArr = [];
        angular.forEach(grouped, function (ele) {
          tempArr.push(ele);
        });

        // return tempArr.join(','); different heights of the same type.
        const numberOfTurbineStr =
          tempArr.length > 1
            ? "(" + tempArr.join(",") + ") / " + filteredTurbineArry.length
            : filteredTurbineArry.length;
        return numberOfTurbineStr;
      };
      /* ------------------------- calculate turbines ------------------- */

      /* ------------------------- check value if null, blank or undedefined return 0.00 ------------------- */
      // this function is used at multiple locations.
      function checkValueOrBlank(value, returnAsString = false) {
        let returnVal = 0;
        if (!(value === null || value === undefined)) {
          value = Number.parseFloat(value);
          returnVal = Number.isFinite(value) ? value : 0;
        }
        return !returnAsString
          ? returnVal
          : returnVal == 0
          ? "0.00"
          : returnVal.toFixed(2);
      }
      /* ------------------------- Check value or blank ------------------- */
      // Calculate Total.
      function calculateTotal(valueArr, isReportTemplate = false) {
        let calculatedTotal = 0;
        if (valueArr.length) {
          let total = 1;
          valueArr.forEach(function (value) {
            if (!isReportTemplate) {
              value = 100 - value;
            }
            total *= value / 100;
          });
          calculatedTotal = total * 100;
        }
        if (!isReportTemplate) {
          return (100 - calculatedTotal).toFixed(2);
        } else {
          return calculatedTotal.toFixed(2);
        }
      }
      /* ------------------------- End calculate Total ------------------- */
      /* ------------------------- get wrb resolution  (grid resolution) ------------------- */
      // 200m or 50m resolution -- wrb or wrg -- relation name - GRID
      const getWrbResolution = function (resolution) {
        if (resolution === 0.2) {
          return "200m WRB";
        } else if (resolution === 0.05) {
          return "50m WRB";
        } else {
          return "external provided WRB";
        }
      };
      /* ------------------------- get wrb resolution ------------------- */

      /* ------------------------- get estimatesComment ------------------- */
      // Estimates comment - generated based on mast association and resolution.
      const getEstimatesComment = function (mastAssociation, resolution) {
        const wrbResolution = getWrbResolution(resolution);
        return mastAssociation === "N/A"
          ? "The estimates presented herein are based on wind flow simulation using " +
              wrbResolution +
              "; no on-site measurements were considered for this analysis. UL Solutions considers these estimates preliminary for the specified project area and recommends confirming with on-site measurements."
          : "The estimates presented herein are based on wind flow simulation using " +
              wrbResolution +
              "; on-site measurements were considered for this analysis. UL Solutions considers these estimates preliminary for the specified project.";
      };

      /* ------------------------- exposing functions of this service ------------------- */
      // the below named functions are exposed to be called from ey calculation modal js
      return {
        exportPdf: exportPdf,
        calculateNumberOfTurbine: calculateNumberOfTurbine,
        checkValueOrBlank: checkValueOrBlank,
        countNumberOfHubHeight: countNumberOfHubHeight,
        getEstimatesComment: getEstimatesComment,
        calculateTotal: calculateTotal,
      };
    },
  ]);
